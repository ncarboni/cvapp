import { Component } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent {
  submitForm(): void {
    // Ajoutez ici la logique pour traiter le formulaire (envoyer des données, etc.)
    console.log('Formulaire soumis !');
  }
}
