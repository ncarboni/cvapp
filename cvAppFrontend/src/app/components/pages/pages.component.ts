import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter, Inject,
  Input,
  OnChanges,
  OnInit,
  Output, PLATFORM_ID,
  SimpleChanges
} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {DOCUMENT, isPlatformBrowser} from "@angular/common";

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit, OnChanges {
  public productId: string = "";
  @Input() value: string = "";


  constructor(
    private route: ActivatedRoute,
    @Inject(DOCUMENT) private document: Document,
    @Inject(PLATFORM_ID) private platformId: Object,
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.productId = params['id'];
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      // Sélection de l'élément à faire défiler
      const page = document.getElementById(this.value);
      // Faire défiler l'élément jusqu'à ce qu'il soit visible à l'écran
      page?.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});
      page?.scrollIntoView(true);
    }
  }

}
