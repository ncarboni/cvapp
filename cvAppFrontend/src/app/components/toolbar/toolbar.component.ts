import {
  Component, ElementRef,
  EventEmitter,
  Inject,
  Input,
  OnChanges,
  OnInit,
  Output,
  PLATFORM_ID,
  SimpleChanges
} from '@angular/core';
import {DOCUMENT, isPlatformBrowser} from "@angular/common";

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent {
  @Output() clickItemMenuEvent: EventEmitter<string>;
  constructor() {
    this.clickItemMenuEvent = new EventEmitter<string>();
  }

  }
