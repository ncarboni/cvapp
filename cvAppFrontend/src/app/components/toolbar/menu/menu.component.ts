import {
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {MatMenuTrigger} from "@angular/material/menu";


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})

export class MenuComponent {
  @Output() clickItemMenu: EventEmitter<string>;

  constructor() {
    this.clickItemMenu = new EventEmitter<string>();
  }

  onClickItemMenu(page: string) {
    this.clickItemMenu.emit(page);
  }

  openMenu(matMenuTrigger: MatMenuTrigger, page: string) {
//    matMenuTrigger.openMenu()
    this.onClickItemMenu(page)
  }

}
