import { Component } from '@angular/core';
import {Observable, Observer} from "rxjs";

export interface ExampleTab {
  label: string;
  content: string;
}

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss'],
})
export class SkillsComponent {


}
