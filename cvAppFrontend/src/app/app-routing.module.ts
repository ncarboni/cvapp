import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {PersonnelComponent} from "./components/personnel/personnel.component";
import {SkillsComponent} from "./components/skills/skills.component";
import {ContactComponent} from "./components/contact/contact.component";
import {ExperienceComponent} from "./components/experience/experience.component";
import {RepositoryComponent} from "./components/repository/repository.component";
import {PagesComponent} from "./components/pages/pages.component";

const routes: Routes = [
  {path: ':id', component: PagesComponent },
  {path: 'home', component: HomeComponent },
  {path: 'personnel', component: PersonnelComponent },
  {path: 'skills', component: SkillsComponent },
  {path: 'contact', component: ContactComponent },
  {path: 'experience', component: ExperienceComponent },
  {path: 'repository', component: RepositoryComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


