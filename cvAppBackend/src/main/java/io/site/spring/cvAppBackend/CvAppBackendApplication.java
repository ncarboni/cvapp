package io.site.spring.cvAppBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class CvAppBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(CvAppBackendApplication.class, args);
	}

}
