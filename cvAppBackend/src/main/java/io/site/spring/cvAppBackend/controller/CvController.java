package io.site.spring.cvAppBackend.controller;

import model.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.CvService;

import java.util.List;

@RestController
public class CvController {

    private CvService cvService = new CvService();

    @Autowired
    @RequestMapping("/cars")
    public List<Car> getCard() {
        return cvService.getCars();
    }

    @RequestMapping("/car/{id}")
    public Car getCar(@PathVariable("id") long id) {
        return cvService.getCar(id);
    }
    @RequestMapping(method = RequestMethod.DELETE, value = "/car/{id}")
    public void deleteCar(@PathVariable("id") long id) {
        cvService.deleteCar(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/cars")
    public void addCar(@RequestBody Car car) {
        cvService.addCar(car);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "car/{id}")
    public void updateCar(@RequestBody Car car, @PathVariable("id") long id) {
        cvService.updateCar(car, id);
    }
}
