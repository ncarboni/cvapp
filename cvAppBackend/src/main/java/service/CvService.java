package service;

import model.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.CvRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class CvService {
    public CvRepository cvRepository;

    @Autowired
    public List<Car> getCars() {
        List<Car> cars = new ArrayList<>();
        cvRepository.findAll().forEach(car -> {
            cars.add(car);
        });
        return cars;
    }


    public Car getCar(long id) {
        return cvRepository.findById(id).orElse(null);
    }

    public void deleteCar(long id) {
        cvRepository.deleteById(id);
    }

    public void addCar(Car car) {
        cvRepository.save(car);
    }

    public void updateCar(Car car, long id) {
        cvRepository.save(car);
    }
}
