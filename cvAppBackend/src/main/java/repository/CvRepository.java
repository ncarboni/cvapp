package repository;

import model.Car;
import org.springframework.data.repository.CrudRepository;

public interface CvRepository extends CrudRepository<Car, Long> {}
