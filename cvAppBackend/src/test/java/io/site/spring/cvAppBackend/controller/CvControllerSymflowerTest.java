package io.site.spring.cvAppBackend.controller;

import java.lang.reflect.Field;
import model.Car;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class CvControllerSymflowerTest {
	@Test
	public void addCar1() throws IllegalAccessException, NoSuchFieldException {
		CvController c = new CvController();
		final Field fieldCvService = CvController.class.getDeclaredField("cvService");
		fieldCvService.setAccessible(true);
		fieldCvService.set(c, null);
		Car car = new Car(0L, null, null, 0, null);
		// assertThrows(java.lang.NullPointerException.class, () -> {
		c.addCar(car);
		// });
	}

	@Test
	public void deleteCar2() throws IllegalAccessException, NoSuchFieldException {
		CvController c = new CvController();
		final Field fieldCvService = CvController.class.getDeclaredField("cvService");
		fieldCvService.setAccessible(true);
		fieldCvService.set(c, null);
		long id = 0L;
		// assertThrows(java.lang.NullPointerException.class, () -> {
		c.deleteCar(id);
		// });
	}

	@Test
	public void getCar3() throws IllegalAccessException, NoSuchFieldException {
		CvController c = new CvController();
		final Field fieldCvService = CvController.class.getDeclaredField("cvService");
		fieldCvService.setAccessible(true);
		fieldCvService.set(c, null);
		long id = 0L;
		// assertThrows(java.lang.NullPointerException.class, () -> {
		c.getCar(id);
		// });
	}

	@Test
	public void getCard4() throws IllegalAccessException, NoSuchFieldException {
		CvController c = new CvController();
		final Field fieldCvService = CvController.class.getDeclaredField("cvService");
		fieldCvService.setAccessible(true);
		fieldCvService.set(c, null);
		// assertThrows(java.lang.NullPointerException.class, () -> {
		c.getCard();
		// });
	}

	@Test
	public void updateCar5() throws IllegalAccessException, NoSuchFieldException {
		CvController c = new CvController();
		final Field fieldCvService = CvController.class.getDeclaredField("cvService");
		fieldCvService.setAccessible(true);
		fieldCvService.set(c, null);
		Car car = new Car(0L, null, null, 0, null);
		long id = 0L;
		// assertThrows(java.lang.NullPointerException.class, () -> {
		c.updateCar(car, id);
		// });
	}
}
