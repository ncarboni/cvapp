package model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class CarSymflowerTest {
	@Test
	public void Car1() {
		long id = 0L;
		String model2 = "";
		String brand = "";
		int year = 0;
		Car.Color color = Car.Color.BLUE;
		Car expected = new Car(0L, "", "", 0, Car.Color.BLUE);
		Car actual = new Car(id, model2, brand, year, color);

		assertTrue(EqualsBuilder.reflectionEquals(expected, actual, false, null, true));
	}

	@Test
	public void Car2() {
		Car expected = new Car();
		Car actual = new Car();

		assertTrue(EqualsBuilder.reflectionEquals(expected, actual, false, null, true));
	}

	@Test
	public void getBrand3() {
		Car c = new Car(0L, null, "", 0, null);
		String expected = "";
		String actual = c.getBrand();

		assertEquals(expected, actual);
	}

	@Test
	public void getColor4() {
		Car c = new Car(0L, null, null, 0, Car.Color.BLUE);
		Car.Color expected = Car.Color.BLUE;
		Car.Color actual = c.getColor();

		assertEquals(expected, actual);
	}

	@Test
	public void getId5() {
		Car c = new Car(0L, null, null, 0, null);
		long expected = 0L;
		long actual = c.getId();

		assertEquals(expected, actual);
	}

	@Test
	public void getModel6() {
		Car c = new Car(0L, "", null, 0, null);
		String expected = "";
		String actual = c.getModel();

		assertEquals(expected, actual);
	}

	@Test
	public void getYear7() {
		Car c = new Car(0L, null, null, 0, null);
		int expected = 0;
		int actual = c.getYear();

		assertEquals(expected, actual);
	}

	@Test
	public void setBrand8() {
		Car c = new Car(0L, null, null, 0, null);
		String brand = "";
		c.setBrand(brand);

		Car cExpected = new Car(0L, null, "", 0, null);

		assertTrue(EqualsBuilder.reflectionEquals(cExpected, c, false, null, true));
	}

	@Test
	public void setColor9() {
		Car c = new Car(0L, null, null, 0, null);
		Car.Color color = Car.Color.BLUE;
		c.setColor(color);

		Car cExpected = new Car(0L, null, null, 0, Car.Color.BLUE);

		assertTrue(EqualsBuilder.reflectionEquals(cExpected, c, false, null, true));
	}

	@Test
	public void setId10() {
		Car c = new Car(0L, null, null, 0, null);
		long id = 0L;
		c.setId(id);

		Car cExpected = new Car(0L, null, null, 0, null);

		assertTrue(EqualsBuilder.reflectionEquals(cExpected, c, false, null, true));
	}

	@Test
	public void setModel11() {
		Car c = new Car(0L, null, null, 0, null);
		String model2 = "";
		c.setModel(model2);

		Car cExpected = new Car(0L, "", null, 0, null);

		assertTrue(EqualsBuilder.reflectionEquals(cExpected, c, false, null, true));
	}

	@Test
	public void setYear12() {
		Car c = new Car(0L, null, null, 0, null);
		int year = 0;
		c.setYear(year);

		Car cExpected = new Car(0L, null, null, 0, null);

		assertTrue(EqualsBuilder.reflectionEquals(cExpected, c, false, null, true));
	}
}
