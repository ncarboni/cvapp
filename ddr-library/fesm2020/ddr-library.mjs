import * as i0 from '@angular/core';
import { Injectable, EventEmitter, Directive, Input, Output, HostListener, NgModule, Component, forwardRef, ViewEncapsulation, Pipe, TemplateRef, ContentChild, ViewChildren, ViewChild, ContentChildren } from '@angular/core';
import * as i1 from '@angular/common';
import { CommonModule } from '@angular/common';
import { Subject } from 'rxjs';
import * as i2 from '@angular/forms';
import { NG_VALUE_ACCESSOR, FormsModule } from '@angular/forms';
import * as i1$1 from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { trigger, transition, style, animate, state, group } from '@angular/animations';
import { has, get, cloneDeep, isEqual } from 'lodash-es';
import * as i3 from 'ngx-pagination';
import { NgxPaginationModule } from 'ngx-pagination';

class DdrToastConstants {
}
DdrToastConstants.TYPE_INFO = 'info';
DdrToastConstants.TYPE_WARNING = 'warning';
DdrToastConstants.TYPE_ERROR = 'danger';
DdrToastConstants.TYPE_SUCCESS = 'success';
DdrToastConstants.TIMEOUT = 5000;
class DdrBlockListConstants {
}
DdrBlockListConstants.PAGINATION_DEFAULT = 10;
class DdrResolutionConstants {
}
DdrResolutionConstants.MOBILE = 'mobile';
DdrResolutionConstants.TABLET = 'tablet';
DdrResolutionConstants.WEB = 'web';
DdrResolutionConstants.MOBILE_MIN = 0;
DdrResolutionConstants.MOBILE_MAX = 568;
DdrResolutionConstants.TABLET_MIN = 568;
DdrResolutionConstants.TABLET_MAX = 992;
DdrResolutionConstants.WEB_MIN = 992;
DdrResolutionConstants.WEB_MAX = 9999;
class DdrAccordionConstants {
}
DdrAccordionConstants.OPEN = 'open';
DdrAccordionConstants.CLOSE = 'close';
class DdrModalTypeConstants {
}
DdrModalTypeConstants.CONFIRM = 'confirm';
DdrModalTypeConstants.INFO = 'info';
DdrModalTypeConstants.NO_BUTTONS = 'no-buttons';
class DdrThemeConstants {
}
DdrThemeConstants.THEME_DEFAULT = 'theme-default';
DdrThemeConstants.THEME_DARK = 'theme-dark';

class DdrConstantsService {
    constructor() {
        this.DdrToastConstants = DdrToastConstants;
        this.DdrBlockListConstants = DdrBlockListConstants;
        this.DdrResolutionConstants = DdrResolutionConstants;
        this.DdrAccordionConstants = DdrAccordionConstants;
        this.DdrModalTypeConstants = DdrModalTypeConstants;
        this.DdrThemeConstants = DdrThemeConstants;
    }
}
DdrConstantsService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConstantsService, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
DdrConstantsService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConstantsService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConstantsService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });

class DdrThemeService {
    constructor(constans) {
        this.constans = constans;
        this.themesAvailables = [
            this.constans.DdrThemeConstants.THEME_DEFAULT,
            this.constans.DdrThemeConstants.THEME_DARK
        ];
        this._theme = this.constans.DdrThemeConstants.THEME_DEFAULT;
    }
    get theme() {
        return this._theme;
    }
    changeTheme(theme) {
        if (this.themesAvailables.find(t => t === this._theme)) {
            this._theme = theme;
        }
        else {
            console.error("Theme " + theme + " not exists");
        }
    }
}
DdrThemeService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrThemeService, deps: [{ token: DdrConstantsService }], target: i0.ɵɵFactoryTarget.Injectable });
DdrThemeService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrThemeService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrThemeService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return [{ type: DdrConstantsService }]; } });

class DdrClickOutsideDirective {
    constructor(elementRef) {
        this.elementRef = elementRef;
        this.clickOutsideEnabled = true;
        this.avoidFirstTime = false;
        this.clickOutside = new EventEmitter();
        this.firstTime = true;
    }
    onDocumentClick(event) {
        if (this.clickOutsideEnabled) {
            const target = event.target;
            if (target && !this.elementRef.nativeElement.contains(target)) {
                if (!this.avoidFirstTime || (this.avoidFirstTime && !this.firstTime)) {
                    if (this.clickOutsideDelay) {
                        setTimeout(() => {
                            this.clickOutside.emit(event);
                        }, this.clickOutsideDelay);
                    }
                    else {
                        this.clickOutside.emit(event);
                    }
                }
                else {
                    this.firstTime = false;
                }
            }
        }
    }
}
DdrClickOutsideDirective.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrClickOutsideDirective, deps: [{ token: i0.ElementRef }], target: i0.ɵɵFactoryTarget.Directive });
DdrClickOutsideDirective.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "13.0.2", type: DdrClickOutsideDirective, selector: "[ddrClickOutside]", inputs: { clickOutsideEnabled: "clickOutsideEnabled", avoidFirstTime: "avoidFirstTime", clickOutsideDelay: "clickOutsideDelay" }, outputs: { clickOutside: "clickOutside" }, host: { listeners: { "document:click": "onDocumentClick($event)" } }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrClickOutsideDirective, decorators: [{
            type: Directive,
            args: [{
                    selector: '[ddrClickOutside]'
                }]
        }], ctorParameters: function () { return [{ type: i0.ElementRef }]; }, propDecorators: { clickOutsideEnabled: [{
                type: Input
            }], avoidFirstTime: [{
                type: Input
            }], clickOutsideDelay: [{
                type: Input
            }], clickOutside: [{
                type: Output
            }], onDocumentClick: [{
                type: HostListener,
                args: ['document:click', ['$event']]
            }] } });

class DdrClickOutsideModule {
}
DdrClickOutsideModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrClickOutsideModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrClickOutsideModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrClickOutsideModule, declarations: [DdrClickOutsideDirective], imports: [CommonModule], exports: [DdrClickOutsideDirective] });
DdrClickOutsideModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrClickOutsideModule, imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrClickOutsideModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        DdrClickOutsideDirective
                    ],
                    exports: [
                        DdrClickOutsideDirective
                    ]
                }]
        }] });

class DdrModalService {
    constructor() {
        this.modals = [];
    }
    add(modal) {
        if (!this.modals.find(m => m.id === modal.id)) {
            this.modals.push(modal);
        }
    }
    remove(id) {
        this.modals = this.modals.filter(m => m.id != id);
    }
    open(id) {
        const modal = this.modals.find(x => x.id == id);
        if (modal) {
            modal.show = true;
        }
        else {
            console.error("Modal not exists");
        }
    }
    close(id) {
        const modal = this.modals.find(x => x.id == id);
        if (modal) {
            modal.show = false;
        }
        else {
            console.error("Modal not exists");
        }
    }
}
DdrModalService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrModalService, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
DdrModalService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrModalService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrModalService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });

class DdrModalComponent {
    constructor(constants, ddrModal) {
        this.constants = constants;
        this.ddrModal = ddrModal;
        this.type = this.constants.DdrModalTypeConstants.INFO;
        this.forceClose = false;
        this.close = new EventEmitter();
        this.accept = new EventEmitter();
        this.show = false;
    }
    ngOnInit() {
        if (!this.id) {
            console.error("Modal must have id");
            return;
        }
        this.ddrModal.add(this);
    }
    onConfirm($event) {
        this.accept.emit($event);
        this.ddrModal.close(this.id);
    }
    onClose($event) {
        this.close.emit($event);
        this.ddrModal.close(this.id);
    }
    onclickOutside($event) {
        this.close.emit($event);
        this.ddrModal.close(this.id);
    }
}
DdrModalComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrModalComponent, deps: [{ token: DdrConstantsService }, { token: DdrModalService }], target: i0.ɵɵFactoryTarget.Component });
DdrModalComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrModalComponent, selector: "ddr-modal", inputs: { id: "id", type: "type", labelConfirm: "labelConfirm", labelClose: "labelClose", forceClose: "forceClose" }, outputs: { close: "close", accept: "accept" }, ngImport: i0, template: "<div class=\"ddr-modal\" *ngIf=\"show\">\n  <div class=\"ddr-modal__content\">\n\n    <div class=\"card\" ddrClickOutside \n      [avoidFirstTime]=\"true\" \n      [clickOutsideEnabled]=\"!forceClose\" \n      (clickOutside)=\"onclickOutside($event)\">\n      <div class=\"card-title ddr-modal__content--title\">\n        <ng-content select=\"[modal-title]\"></ng-content>\n      </div>\n\n      <div class=\"card-body\">\n        <ng-content select=\"[modal-content]\"></ng-content>\n      </div>\n  \n      <div class=\"ddr-modal__content--card-buttons\" *ngIf=\"type !== constants.DdrModalTypeConstants.NO_BUTTONS\">\n  \n        <button *ngIf=\"type === constants.DdrModalTypeConstants.CONFIRM || type === constants.DdrModalTypeConstants.INFO\"\n          (click)=\"onConfirm($event)\"\n          class=\"btn btn-primary ddr-modal__content--card-buttons--button\">\n          <i class=\"fa fa-check\">{{labelConfirm}}</i>\n        </button>\n  \n        <button *ngIf=\"type === constants.DdrModalTypeConstants.CONFIRM\"\n          (click)=\"onClose($event)\"\n          class=\"btn btn-danger ddr-modal__content--card-buttons--button\">\n          <i class=\"fa fa-times\">{{labelClose}}</i>\n        </button>\n  \n      </div>\n    </div>\n   \n  </div>\n</div>\n\n<div class=\"ddr-overlay\" *ngIf=\"show\"></div>", styles: [".ddr-modal{position:fixed;z-index:9999;width:50%;left:25vw;top:40vh;border-radius:.25em}.ddr-modal .card{box-shadow:5px 5px 5px 1px #000000bf}.ddr-modal__content--title{font-weight:600;text-decoration:underline;text-align:center;padding-top:20px}.ddr-modal__content--card-buttons{margin-left:5px;padding-bottom:20px}.ddr-modal__content--card-buttons--button{margin-left:20px}\n"], directives: [{ type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: DdrClickOutsideDirective, selector: "[ddrClickOutside]", inputs: ["clickOutsideEnabled", "avoidFirstTime", "clickOutsideDelay"], outputs: ["clickOutside"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrModalComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-modal', template: "<div class=\"ddr-modal\" *ngIf=\"show\">\n  <div class=\"ddr-modal__content\">\n\n    <div class=\"card\" ddrClickOutside \n      [avoidFirstTime]=\"true\" \n      [clickOutsideEnabled]=\"!forceClose\" \n      (clickOutside)=\"onclickOutside($event)\">\n      <div class=\"card-title ddr-modal__content--title\">\n        <ng-content select=\"[modal-title]\"></ng-content>\n      </div>\n\n      <div class=\"card-body\">\n        <ng-content select=\"[modal-content]\"></ng-content>\n      </div>\n  \n      <div class=\"ddr-modal__content--card-buttons\" *ngIf=\"type !== constants.DdrModalTypeConstants.NO_BUTTONS\">\n  \n        <button *ngIf=\"type === constants.DdrModalTypeConstants.CONFIRM || type === constants.DdrModalTypeConstants.INFO\"\n          (click)=\"onConfirm($event)\"\n          class=\"btn btn-primary ddr-modal__content--card-buttons--button\">\n          <i class=\"fa fa-check\">{{labelConfirm}}</i>\n        </button>\n  \n        <button *ngIf=\"type === constants.DdrModalTypeConstants.CONFIRM\"\n          (click)=\"onClose($event)\"\n          class=\"btn btn-danger ddr-modal__content--card-buttons--button\">\n          <i class=\"fa fa-times\">{{labelClose}}</i>\n        </button>\n  \n      </div>\n    </div>\n   \n  </div>\n</div>\n\n<div class=\"ddr-overlay\" *ngIf=\"show\"></div>", styles: [".ddr-modal{position:fixed;z-index:9999;width:50%;left:25vw;top:40vh;border-radius:.25em}.ddr-modal .card{box-shadow:5px 5px 5px 1px #000000bf}.ddr-modal__content--title{font-weight:600;text-decoration:underline;text-align:center;padding-top:20px}.ddr-modal__content--card-buttons{margin-left:5px;padding-bottom:20px}.ddr-modal__content--card-buttons--button{margin-left:20px}\n"] }]
        }], ctorParameters: function () { return [{ type: DdrConstantsService }, { type: DdrModalService }]; }, propDecorators: { id: [{
                type: Input
            }], type: [{
                type: Input
            }], labelConfirm: [{
                type: Input
            }], labelClose: [{
                type: Input
            }], forceClose: [{
                type: Input
            }], close: [{
                type: Output
            }], accept: [{
                type: Output
            }] } });

class DdrModalModule {
}
DdrModalModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrModalModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrModalModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrModalModule, declarations: [DdrModalComponent], imports: [CommonModule,
        DdrClickOutsideModule], exports: [DdrModalComponent] });
DdrModalModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrModalModule, providers: [
        DdrModalService
    ], imports: [[
            CommonModule,
            DdrClickOutsideModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrModalModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule,
                        DdrClickOutsideModule
                    ],
                    declarations: [
                        DdrModalComponent
                    ],
                    exports: [
                        DdrModalComponent
                    ],
                    providers: [
                        DdrModalService
                    ]
                }]
        }] });

class DdrNgModelBase {
    constructor() {
        this._innerValue = null;
        this._firstTime = true;
        this._firstValue = new Subject();
    }
    get value() {
        return this._innerValue;
    }
    set value(value) {
        this._innerValue = value;
        if (this.onChange) {
            this.onChange(value);
        }
    }
    get firstValue() {
        return this._firstValue;
    }
    set firstValue(value) {
        this._firstValue = value;
    }
    writeValue(value) {
        if (value !== this._innerValue) {
            this.value = value;
            if (this._firstTime) {
                this._firstValue.next(this.value);
                this._firstTime = false;
            }
        }
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) { }
}
DdrNgModelBase.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrNgModelBase, deps: [], target: i0.ɵɵFactoryTarget.Component });
DdrNgModelBase.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrNgModelBase, selector: "ng-component", ngImport: i0, template: '', isInline: true });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrNgModelBase, decorators: [{
            type: Component,
            args: [{
                    template: ''
                }]
        }], ctorParameters: function () { return []; } });

class DdrToggleComponent extends DdrNgModelBase {
    constructor() {
        super();
        this.toggled = new EventEmitter();
    }
    onClick() {
        this.value = !this.value;
        this.toggled.emit(this.value);
    }
}
DdrToggleComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToggleComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
DdrToggleComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrToggleComponent, selector: "ddr-toggle", inputs: { label: "label" }, outputs: { toggled: "toggled" }, providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DdrToggleComponent),
            multi: true
        }
    ], usesInheritance: true, ngImport: i0, template: "<div class=\"ddr-toggle\">\n  <div class=\"ddr-toggle__label\" *ngIf=\"label\">\n    <label>{{label}}</label>\n  </div>\n  <button class=\"ddr-toggle__btn\"\n    [ngClass]=\"{'ddr-toggle__btn--on': value, 'ddr-toggle__btn--off': !value}\"\n    (click)=\"onClick()\">\n\n  </button>\n</div> ", styles: [".ddr-toggle{margin-top:5px;margin-bottom:5px;display:inline-block}.ddr-toggle__label{text-transform:uppercase}.ddr-toggle__btn{display:inline-block;width:60px;height:30px;position:relative;cursor:pointer;border-radius:20px;padding:2px;border:none}.ddr-toggle__btn:after{content:\"\";display:block;position:relative;width:50%;height:100%;border-radius:20px;transition:all .3s cubic-bezier(.175,.885,.32,1.275),padding .3s ease,margin .3s ease}.ddr-toggle__btn--on{background:#28a745}.ddr-toggle__btn--on:after{left:50%}.ddr-toggle__btn--off{background:#afafafaf}.ddr-toggle__btn--off:after{left:0}\n"], directives: [{ type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }], encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToggleComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-toggle', encapsulation: ViewEncapsulation.None, providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef(() => DdrToggleComponent),
                            multi: true
                        }
                    ], template: "<div class=\"ddr-toggle\">\n  <div class=\"ddr-toggle__label\" *ngIf=\"label\">\n    <label>{{label}}</label>\n  </div>\n  <button class=\"ddr-toggle__btn\"\n    [ngClass]=\"{'ddr-toggle__btn--on': value, 'ddr-toggle__btn--off': !value}\"\n    (click)=\"onClick()\">\n\n  </button>\n</div> ", styles: [".ddr-toggle{margin-top:5px;margin-bottom:5px;display:inline-block}.ddr-toggle__label{text-transform:uppercase}.ddr-toggle__btn{display:inline-block;width:60px;height:30px;position:relative;cursor:pointer;border-radius:20px;padding:2px;border:none}.ddr-toggle__btn:after{content:\"\";display:block;position:relative;width:50%;height:100%;border-radius:20px;transition:all .3s cubic-bezier(.175,.885,.32,1.275),padding .3s ease,margin .3s ease}.ddr-toggle__btn--on{background:#28a745}.ddr-toggle__btn--on:after{left:50%}.ddr-toggle__btn--off{background:#afafafaf}.ddr-toggle__btn--off:after{left:0}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { label: [{
                type: Input
            }], toggled: [{
                type: Output
            }] } });

class DdrNgModelBaseModule {
}
DdrNgModelBaseModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrNgModelBaseModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrNgModelBaseModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrNgModelBaseModule, declarations: [DdrNgModelBase], imports: [CommonModule], exports: [DdrNgModelBase] });
DdrNgModelBaseModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrNgModelBaseModule, imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrNgModelBaseModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        DdrNgModelBase
                    ],
                    exports: [
                        DdrNgModelBase
                    ]
                }]
        }] });

class DdrToggleModule {
}
DdrToggleModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToggleModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrToggleModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToggleModule, declarations: [DdrToggleComponent], imports: [CommonModule,
        DdrNgModelBaseModule], exports: [DdrToggleComponent] });
DdrToggleModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToggleModule, imports: [[
            CommonModule,
            DdrNgModelBaseModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToggleModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule,
                        DdrNgModelBaseModule
                    ],
                    declarations: [
                        DdrToggleComponent
                    ],
                    exports: [
                        DdrToggleComponent
                    ]
                }]
        }] });

class DdrTranslateService {
    constructor(http) {
        this.http = http;
    }
    getData(path) {
        return new Promise((resolve, reject) => {
            let language;
            if (navigator.language.includes('-')) {
                language = navigator.language.split('-')[0];
            }
            else {
                language = navigator.language;
            }
            this.http.get(path + language + '.json').subscribe(data => {
                this._data = data;
                resolve(true);
            }, error => {
                console.error("Error to get translations: ", error);
                reject(true);
            });
        });
    }
    getTranslate(word) {
        return this._data[word];
    }
}
DdrTranslateService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTranslateService, deps: [{ token: i1$1.HttpClient }], target: i0.ɵɵFactoryTarget.Injectable });
DdrTranslateService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTranslateService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTranslateService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return [{ type: i1$1.HttpClient }]; } });

class DdrTranslatePipe {
    constructor(translateService) {
        this.translateService = translateService;
    }
    transform(value) {
        return this.translateService.getTranslate(value) ? this.translateService.getTranslate(value) : value;
    }
}
DdrTranslatePipe.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTranslatePipe, deps: [{ token: DdrTranslateService }], target: i0.ɵɵFactoryTarget.Pipe });
DdrTranslatePipe.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTranslatePipe, name: "ddrTranslate" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTranslatePipe, decorators: [{
            type: Pipe,
            args: [{
                    name: 'ddrTranslate'
                }]
        }], ctorParameters: function () { return [{ type: DdrTranslateService }]; } });

class DdrTranslateModule {
}
DdrTranslateModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTranslateModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrTranslateModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTranslateModule, declarations: [DdrTranslatePipe], imports: [CommonModule,
        HttpClientModule], exports: [DdrTranslatePipe] });
DdrTranslateModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTranslateModule, providers: [
        DdrTranslateService
    ], imports: [[
            CommonModule,
            HttpClientModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTranslateModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule,
                        HttpClientModule
                    ],
                    declarations: [
                        DdrTranslatePipe
                    ],
                    exports: [
                        DdrTranslatePipe
                    ],
                    providers: [
                        DdrTranslateService
                    ]
                }]
        }] });

class DdrResolutionService {
    constructor(constants) {
        this.constants = constants;
    }
    get size() {
        return this._size;
    }
    set size(value) {
        this._size = value;
    }
    setSize(width) {
        if (width >= this.constants.DdrResolutionConstants.MOBILE_MIN &&
            width < this.constants.DdrResolutionConstants.MOBILE_MAX) {
            this._size = this.constants.DdrResolutionConstants.MOBILE;
        }
        else if (width >= this.constants.DdrResolutionConstants.TABLET_MIN &&
            width < this.constants.DdrResolutionConstants.TABLET_MAX) {
            this._size = this.constants.DdrResolutionConstants.TABLET;
        }
        else if (width >= this.constants.DdrResolutionConstants.WEB_MIN &&
            width < this.constants.DdrResolutionConstants.WEB_MAX) {
            this._size = this.constants.DdrResolutionConstants.WEB;
        }
    }
}
DdrResolutionService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionService, deps: [{ token: DdrConstantsService }], target: i0.ɵɵFactoryTarget.Injectable });
DdrResolutionService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return [{ type: DdrConstantsService }]; } });

class DdrResolutionDirective {
    constructor(resolution) {
        this.resolution = resolution;
    }
    ngOnInit() {
        this.sendSize(window.innerWidth);
    }
    onResize(event) {
        this.sendSize(event.currentTarget.innerWidth);
    }
    sendSize(width) {
        this.resolution.setSize(width);
    }
}
DdrResolutionDirective.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionDirective, deps: [{ token: DdrResolutionService }], target: i0.ɵɵFactoryTarget.Directive });
DdrResolutionDirective.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "13.0.2", type: DdrResolutionDirective, selector: "[ddrResolution]", host: { listeners: { "window:resize": "onResize($event)" } }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionDirective, decorators: [{
            type: Directive,
            args: [{
                    selector: '[ddrResolution]'
                }]
        }], ctorParameters: function () { return [{ type: DdrResolutionService }]; }, propDecorators: { onResize: [{
                type: HostListener,
                args: ['window:resize', ['$event']]
            }] } });

class DdrResolutionModule {
}
DdrResolutionModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrResolutionModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionModule, declarations: [DdrResolutionDirective], imports: [CommonModule], exports: [DdrResolutionDirective] });
DdrResolutionModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionModule, providers: [
        DdrResolutionService
    ], imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        DdrResolutionDirective
                    ],
                    exports: [
                        DdrResolutionDirective
                    ],
                    providers: [
                        DdrResolutionService
                    ]
                }]
        }] });

class DdrDetailComponent {
    constructor() {
        this.showOverlay = true;
        this.showTitle = true;
        this.clickOutsideEnabled = true;
        this.close = new EventEmitter();
        this.showDetail = true;
    }
    ngOnInit() {
    }
    closeDetail() {
        this.showDetail = false;
        setTimeout(() => {
            this.close.emit(true);
        }, 600);
    }
}
DdrDetailComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDetailComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
DdrDetailComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrDetailComponent, selector: "ddr-detail", inputs: { showOverlay: "showOverlay", showTitle: "showTitle", clickOutsideEnabled: "clickOutsideEnabled" }, outputs: { close: "close" }, ngImport: i0, template: "<div class=\"ddr-detail\" *ngIf=\"showDetail\" \n  [@slide] \n  ddrClickOutside \n  [avoidFirstTime]=\"true\"\n  [clickOutsideEnabled]=\"clickOutsideEnabled\" \n  (clickOutside)=\"closeDetail()\">\n\n  <div class=\"row ddr-detail__title\" *ngIf=\"showTitle\">\n    <div class=\"col-lg-10 col-md-9 col-10\">\n      <ng-content select=\"[detail-title]\"></ng-content>\n    </div>\n    <div class=\"col-lg-2 col-md-3 col-2 p-3 text-right\">\n      <i class=\"fa fa-times ddr-detail__close\" (click)=\"closeDetail()\"></i>\n    </div>\n  </div>\n\n  <div class=\"row ddr-detail__content\">\n    <div class=\"col-12\">\n      <ng-content select=\"[detail-content]\"></ng-content>\n    </div>\n  </div>\n\n</div>\n\n<div class=\"ddr-overlay\" *ngIf=\"showDetail && showOverlay\"></div>", styles: [".ddr-detail{position:fixed;top:0;right:0;padding:15px;z-index:9999;height:100%}.ddr-detail__close{font-size:26px;cursor:pointer;margin-right:5px}@media (min-width: 992px){.ddr-detail{width:40%}}@media (max-width: 992px){.ddr-detail{width:100%}}\n"], directives: [{ type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: DdrClickOutsideDirective, selector: "[ddrClickOutside]", inputs: ["clickOutsideEnabled", "avoidFirstTime", "clickOutsideDelay"], outputs: ["clickOutside"] }], animations: [
        trigger('slide', [
            transition(':enter', [
                style({ transform: 'translateX(100%)' }),
                animate('600ms ease-in', style({ transform: 'translateX(0%)' }))
            ]),
            transition(':leave', [
                animate('600ms ease-in', style({ transform: 'translateX(100%)' }))
            ])
        ])
    ], encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDetailComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-detail', encapsulation: ViewEncapsulation.None, animations: [
                        trigger('slide', [
                            transition(':enter', [
                                style({ transform: 'translateX(100%)' }),
                                animate('600ms ease-in', style({ transform: 'translateX(0%)' }))
                            ]),
                            transition(':leave', [
                                animate('600ms ease-in', style({ transform: 'translateX(100%)' }))
                            ])
                        ])
                    ], template: "<div class=\"ddr-detail\" *ngIf=\"showDetail\" \n  [@slide] \n  ddrClickOutside \n  [avoidFirstTime]=\"true\"\n  [clickOutsideEnabled]=\"clickOutsideEnabled\" \n  (clickOutside)=\"closeDetail()\">\n\n  <div class=\"row ddr-detail__title\" *ngIf=\"showTitle\">\n    <div class=\"col-lg-10 col-md-9 col-10\">\n      <ng-content select=\"[detail-title]\"></ng-content>\n    </div>\n    <div class=\"col-lg-2 col-md-3 col-2 p-3 text-right\">\n      <i class=\"fa fa-times ddr-detail__close\" (click)=\"closeDetail()\"></i>\n    </div>\n  </div>\n\n  <div class=\"row ddr-detail__content\">\n    <div class=\"col-12\">\n      <ng-content select=\"[detail-content]\"></ng-content>\n    </div>\n  </div>\n\n</div>\n\n<div class=\"ddr-overlay\" *ngIf=\"showDetail && showOverlay\"></div>", styles: [".ddr-detail{position:fixed;top:0;right:0;padding:15px;z-index:9999;height:100%}.ddr-detail__close{font-size:26px;cursor:pointer;margin-right:5px}@media (min-width: 992px){.ddr-detail{width:40%}}@media (max-width: 992px){.ddr-detail{width:100%}}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { showOverlay: [{
                type: Input
            }], showTitle: [{
                type: Input
            }], clickOutsideEnabled: [{
                type: Input
            }], close: [{
                type: Output
            }] } });

class DdrDetailModule {
}
DdrDetailModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDetailModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrDetailModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDetailModule, declarations: [DdrDetailComponent], imports: [CommonModule,
        DdrClickOutsideModule], exports: [DdrDetailComponent] });
DdrDetailModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDetailModule, imports: [[
            CommonModule,
            DdrClickOutsideModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDetailModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule,
                        DdrClickOutsideModule
                    ],
                    declarations: [
                        DdrDetailComponent
                    ],
                    exports: [
                        DdrDetailComponent
                    ]
                }]
        }] });

class DdrConfigService {
    constructor(http) {
        this.http = http;
    }
    getDataFromJSON(pathJSON) {
        return new Promise((resolve, reject) => {
            this.http.get(pathJSON).subscribe(data => {
                this._data = data;
                resolve(true);
            }, error => {
                console.error("DDR-CONFIG: " + error);
                reject(true);
            });
        });
    }
    getData(path) {
        if (has(this._data, path)) {
            return get(this._data, path);
        }
        else {
            console.error("Not exists path: " + path);
            return null;
        }
    }
    getAllData() {
        return cloneDeep(this._data);
    }
}
DdrConfigService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigService, deps: [{ token: i1$1.HttpClient }], target: i0.ɵɵFactoryTarget.Injectable });
DdrConfigService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return [{ type: i1$1.HttpClient }]; } });

class DdrConfigPipe {
    constructor(ddrConfig) {
        this.ddrConfig = ddrConfig;
    }
    transform(path) {
        return this.ddrConfig.getData(path);
    }
}
DdrConfigPipe.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigPipe, deps: [{ token: DdrConfigService }], target: i0.ɵɵFactoryTarget.Pipe });
DdrConfigPipe.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigPipe, name: "ddrConfig" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigPipe, decorators: [{
            type: Pipe,
            args: [{
                    name: 'ddrConfig'
                }]
        }], ctorParameters: function () { return [{ type: DdrConfigService }]; } });

class DdrConfigModule {
}
DdrConfigModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrConfigModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigModule, declarations: [DdrConfigPipe], imports: [HttpClientModule], exports: [DdrConfigPipe] });
DdrConfigModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigModule, providers: [
        DdrConfigService
    ], imports: [[
            HttpClientModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        HttpClientModule
                    ],
                    declarations: [
                        DdrConfigPipe
                    ],
                    exports: [
                        DdrConfigPipe
                    ],
                    providers: [
                        DdrConfigService
                    ]
                }]
        }] });

class DdrLoadIframeDirective {
    constructor(el) {
        this.el = el;
        this.loadIframe = new EventEmitter();
    }
    onLoad() {
        if (!this.el.nativeElement.contentDocument ||
            this.el.nativeElement.contentDocument.body.children.length > 0) {
            this.loadIframe.emit(true);
        }
    }
}
DdrLoadIframeDirective.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLoadIframeDirective, deps: [{ token: i0.ElementRef }], target: i0.ɵɵFactoryTarget.Directive });
DdrLoadIframeDirective.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "13.0.2", type: DdrLoadIframeDirective, selector: "[ddrLoadIframe]", outputs: { loadIframe: "loadIframe" }, host: { listeners: { "load": "onLoad()" } }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLoadIframeDirective, decorators: [{
            type: Directive,
            args: [{
                    selector: '[ddrLoadIframe]'
                }]
        }], ctorParameters: function () { return [{ type: i0.ElementRef }]; }, propDecorators: { loadIframe: [{
                type: Output
            }], onLoad: [{
                type: HostListener,
                args: ['load']
            }] } });

class DdrLoadIframeModule {
}
DdrLoadIframeModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLoadIframeModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrLoadIframeModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLoadIframeModule, declarations: [DdrLoadIframeDirective], imports: [CommonModule], exports: [DdrLoadIframeDirective] });
DdrLoadIframeModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLoadIframeModule, imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLoadIframeModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        DdrLoadIframeDirective
                    ],
                    exports: [
                        DdrLoadIframeDirective
                    ]
                }]
        }] });

class DdrSpinnerService {
    constructor() { }
    get show() {
        return this._show;
    }
    showSpinner() {
        this._show = true;
    }
    hideSpinner() {
        this._show = false;
    }
}
DdrSpinnerService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrSpinnerService, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
DdrSpinnerService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrSpinnerService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrSpinnerService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });

class DdrSpinnerComponent {
    constructor(ddrSpinner) {
        this.ddrSpinner = ddrSpinner;
        this.embedded = false;
        this.pathImg = 'resources/img/spinner.gif';
    }
    ngOnInit() {
    }
}
DdrSpinnerComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrSpinnerComponent, deps: [{ token: DdrSpinnerService }], target: i0.ɵɵFactoryTarget.Component });
DdrSpinnerComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrSpinnerComponent, selector: "ddr-spinner", inputs: { embedded: "embedded", pathImg: "pathImg" }, ngImport: i0, template: "<div class=\"ddr-spinner\" \n  *ngIf=\"ddrSpinner.show\"\n  [ngClass]=\n  \"{\n    'ddr-spinner--overlay': !embedded, \n    'ddr-spinner--overlay-embedded': embedded\n  }\">\n  <div class=\"ddr-spinner__spinner\">\n    <img [src]=\"pathImg\">\n  </div>\n</div>", styles: [".ddr-spinner--overlay{z-index:999;position:fixed;top:0;left:0;width:100%;height:100%;background-color:#fff9;cursor:default}.ddr-spinner--overlay .ddr-spinner__spinner{position:fixed}.ddr-spinner--overlay-embedded{z-index:999;position:absolute;top:0;left:0;width:100%;height:100%;background-color:#fff9;cursor:default}.ddr-spinner--overlay-embedded .ddr-spinner__spinner{position:absolute}.ddr-spinner__spinner{top:50%;left:50%;transform:translate(-50%,-50%);-webkit-transform:translate(-50%,-50%)}\n"], directives: [{ type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }], encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrSpinnerComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-spinner', encapsulation: ViewEncapsulation.None, template: "<div class=\"ddr-spinner\" \n  *ngIf=\"ddrSpinner.show\"\n  [ngClass]=\n  \"{\n    'ddr-spinner--overlay': !embedded, \n    'ddr-spinner--overlay-embedded': embedded\n  }\">\n  <div class=\"ddr-spinner__spinner\">\n    <img [src]=\"pathImg\">\n  </div>\n</div>", styles: [".ddr-spinner--overlay{z-index:999;position:fixed;top:0;left:0;width:100%;height:100%;background-color:#fff9;cursor:default}.ddr-spinner--overlay .ddr-spinner__spinner{position:fixed}.ddr-spinner--overlay-embedded{z-index:999;position:absolute;top:0;left:0;width:100%;height:100%;background-color:#fff9;cursor:default}.ddr-spinner--overlay-embedded .ddr-spinner__spinner{position:absolute}.ddr-spinner__spinner{top:50%;left:50%;transform:translate(-50%,-50%);-webkit-transform:translate(-50%,-50%)}\n"] }]
        }], ctorParameters: function () { return [{ type: DdrSpinnerService }]; }, propDecorators: { embedded: [{
                type: Input
            }], pathImg: [{
                type: Input
            }] } });

class DdrSpinnerModule {
}
DdrSpinnerModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrSpinnerModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrSpinnerModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrSpinnerModule, declarations: [DdrSpinnerComponent], imports: [CommonModule], exports: [DdrSpinnerComponent] });
DdrSpinnerModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrSpinnerModule, providers: [
        DdrSpinnerService
    ], imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrSpinnerModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        DdrSpinnerComponent
                    ],
                    exports: [
                        DdrSpinnerComponent
                    ],
                    providers: [
                        DdrSpinnerService
                    ]
                }]
        }] });

class DdrJoinPipe {
    transform(value, separator = ',') {
        if (!value) {
            return '';
        }
        return value.join(separator);
    }
}
DdrJoinPipe.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrJoinPipe, deps: [], target: i0.ɵɵFactoryTarget.Pipe });
DdrJoinPipe.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrJoinPipe, name: "join" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrJoinPipe, decorators: [{
            type: Pipe,
            args: [{
                    name: 'join'
                }]
        }] });

class DdrJoinPipeModule {
}
DdrJoinPipeModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrJoinPipeModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrJoinPipeModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrJoinPipeModule, declarations: [DdrJoinPipe], imports: [CommonModule], exports: [DdrJoinPipe] });
DdrJoinPipeModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrJoinPipeModule, imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrJoinPipeModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        DdrJoinPipe
                    ],
                    exports: [
                        DdrJoinPipe
                    ]
                }]
        }] });

class DdrToast {
    constructor(title, message, type) {
        this.title = title;
        this.message = message;
        this.type = type;
    }
}

class DdrToastService {
    constructor(constans) {
        this.constans = constans;
        this._toasts = [];
        this._timeout = this.constans.DdrToastConstants.TIMEOUT;
    }
    get toasts() {
        return this._toasts;
    }
    get timeout() {
        return this._timeout;
    }
    set timeout(value) {
        this._timeout = value;
    }
    addInfoMessage(title, message) {
        this.addMessage(title, message, this.constans.DdrToastConstants.TYPE_INFO);
    }
    addWarningMessage(title, message) {
        this.addMessage(title, message, this.constans.DdrToastConstants.TYPE_WARNING);
    }
    addErrorMessage(title, message) {
        this.addMessage(title, message, this.constans.DdrToastConstants.TYPE_ERROR);
    }
    addSuccessMessage(title, message) {
        this.addMessage(title, message, this.constans.DdrToastConstants.TYPE_SUCCESS);
    }
    addMessage(title, message, type) {
        let toast = new DdrToast(title, message, type);
        this._toasts.push(toast);
        setTimeout(() => {
            this.closeToast(toast);
        }, this._timeout);
    }
    closeToast(toast) {
        let index = this._toasts.findIndex(t => t === toast);
        if (index !== -1) {
            this._toasts.splice(index, 1);
        }
    }
}
DdrToastService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastService, deps: [{ token: DdrConstantsService }], target: i0.ɵɵFactoryTarget.Injectable });
DdrToastService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return [{ type: DdrConstantsService }]; } });

class DdrToastComponent {
    constructor(toastService) {
        this.toastService = toastService;
    }
    ngOnInit() {
        if (this.timeout) {
            this.toastService.timeout = this.timeout;
        }
    }
    closeToast(toast) {
        this.toastService.closeToast(toast);
    }
}
DdrToastComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastComponent, deps: [{ token: DdrToastService }], target: i0.ɵɵFactoryTarget.Component });
DdrToastComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrToastComponent, selector: "ddr-toast", inputs: { timeout: "timeout" }, ngImport: i0, template: "<div class=\"ddr-toast\">\n\n  <div [@overlayAnimation]=\"{value: 'visible'}\" class=\"ddr-toast__container\"\n    [ngClass]=\"'text-light bg-' + toast.type + ' border-' + toast.type\"\n    *ngFor=\"let toast of toastService.toasts\">\n\n    <div class=\"ddr-toast__container--title\">\n      <span class=\"ddr-toast__container--title--message\">{{toast.title}}</span>\n      <i class=\"fa fa-times ddr-toast__container--title--close-toast\" (click)=\"closeToast(toast)\"></i>\n    </div>\n\n    <div class=\"ddr-toast__container--content\">\n      <span>{{toast.message}}</span>\n    </div>\n\n  </div>\n\n</div>", styles: [".ddr-toast{position:absolute;top:10px;right:10px;z-index:99999}.ddr-toast__container{width:300px;padding:5px;margin-bottom:10px;border-radius:4px;border:1px solid #c8c8c8}.ddr-toast__container--content{padding:5px 0;font-size:14px}.ddr-toast__container--title{padding-bottom:5px;border-bottom:1px solid #FFF}.ddr-toast__container--title--message{width:270px;display:inline-block;font-size:18px}.ddr-toast__container--title--close-toast{cursor:pointer;position:absolute;font-size:20px}\n"], directives: [{ type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }], animations: [
        trigger('overlayAnimation', [
            state('void', style({
                transform: 'translateY(5%)',
                opacity: 0
            })),
            state('visible', style({
                transform: 'translateY(0)',
                opacity: 1
            })),
            transition('void => visible', animate('225ms ease-out')),
            transition('visible => void', animate('195ms ease-in'))
        ])
    ], encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-toast', encapsulation: ViewEncapsulation.None, animations: [
                        trigger('overlayAnimation', [
                            state('void', style({
                                transform: 'translateY(5%)',
                                opacity: 0
                            })),
                            state('visible', style({
                                transform: 'translateY(0)',
                                opacity: 1
                            })),
                            transition('void => visible', animate('225ms ease-out')),
                            transition('visible => void', animate('195ms ease-in'))
                        ])
                    ], template: "<div class=\"ddr-toast\">\n\n  <div [@overlayAnimation]=\"{value: 'visible'}\" class=\"ddr-toast__container\"\n    [ngClass]=\"'text-light bg-' + toast.type + ' border-' + toast.type\"\n    *ngFor=\"let toast of toastService.toasts\">\n\n    <div class=\"ddr-toast__container--title\">\n      <span class=\"ddr-toast__container--title--message\">{{toast.title}}</span>\n      <i class=\"fa fa-times ddr-toast__container--title--close-toast\" (click)=\"closeToast(toast)\"></i>\n    </div>\n\n    <div class=\"ddr-toast__container--content\">\n      <span>{{toast.message}}</span>\n    </div>\n\n  </div>\n\n</div>", styles: [".ddr-toast{position:absolute;top:10px;right:10px;z-index:99999}.ddr-toast__container{width:300px;padding:5px;margin-bottom:10px;border-radius:4px;border:1px solid #c8c8c8}.ddr-toast__container--content{padding:5px 0;font-size:14px}.ddr-toast__container--title{padding-bottom:5px;border-bottom:1px solid #FFF}.ddr-toast__container--title--message{width:270px;display:inline-block;font-size:18px}.ddr-toast__container--title--close-toast{cursor:pointer;position:absolute;font-size:20px}\n"] }]
        }], ctorParameters: function () { return [{ type: DdrToastService }]; }, propDecorators: { timeout: [{
                type: Input
            }] } });

class DdrToastModule {
}
DdrToastModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrToastModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastModule, declarations: [DdrToastComponent], imports: [CommonModule], exports: [DdrToastComponent] });
DdrToastModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastModule, providers: [
        DdrToastService
    ], imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        DdrToastComponent
                    ],
                    exports: [
                        DdrToastComponent
                    ],
                    providers: [
                        DdrToastService
                    ]
                }]
        }] });

class DdrDropdownComponent extends DdrNgModelBase {
    constructor() {
        super();
        this.options = [];
        this.labelNoResults = 'No results';
        this.showItems = false;
        this.selectItem = new EventEmitter();
    }
    ngOnInit() {
        this.optionsShow = this.options.slice(0);
        this.firstValue.subscribe((v) => {
            this.preload(v);
        });
    }
    preload(v) {
        let optionFound = this.options.find(option => isEqual(option.value, v));
        if (optionFound) {
            this.valueShow = optionFound.label;
            this.onSelectItem(optionFound);
        }
    }
    showPanelOptions() {
        this.showItems = !this.showItems;
    }
    filter(searchWord) {
        this.optionsShow = this.options.filter(option => option.label.toLowerCase().includes(searchWord.toLowerCase()));
    }
    onSelectItem(item) {
        this.showItems = false;
        console.log(item);
        this.valueShow = item.label;
        this.value = item.value;
        this.selectItem.emit(item);
    }
    hidePanelItems() {
        this.showItems = false;
    }
}
DdrDropdownComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDropdownComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
DdrDropdownComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrDropdownComponent, selector: "ddr-dropdown", inputs: { options: "options", labelNoResults: "labelNoResults" }, outputs: { selectItem: "selectItem" }, providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DdrDropdownComponent),
            multi: true
        }
    ], queries: [{ propertyName: "template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0, template: "<div class=\"ddr-dropdown\" ddrClickOutside (clickOutside)=\"hidePanelItems()\">\n  \n  <div class=\"input-group\" (click)=\"showPanelOptions()\">\n    <input type=\"text\" name=\"value\" [(ngModel)]=\"valueShow\" class=\"form-control\" readonly>\n    <div class=\"input-group-append\">\n      <button>\n        <i class=\"fa fa-caret-down\"></i>\n      </button>\n    </div>\n  </div>\n\n  <div class=\"ddr-dropdown__panel-items\" [@overlayAnimation]=\"{value: 'visible'}\" *ngIf=\"showItems\">\n\n    <div class=\"ddr-dropdown__panel-items--search input-group\">\n      <input type=\"text\" name=\"value\" class=\"form-control\" #search (keyup)=\"filter(search.value)\">\n      <div class=\"input-group-append\">\n        <button>\n          <i class=\"fa fa-search\"></i>\n        </button>\n      </div>\n    </div>\n    <div class=\"ddr-dropdown__panel-items--no-results\" *ngIf=\"optionsShow.length == 0\">\n      <span>{{labelNoResults}}</span>\n    </div>\n    <ul *ngIf=\"optionsShow.length > 0\">\n      <li *ngFor=\"let item of optionsShow\" (click)=\"onSelectItem(item)\">\n        \n        <ng-container *ngTemplateOutlet=\"(template ? itemTemplate : defaultItemTemplate); context: { item }\">\n        </ng-container>\n\n        <ng-template #defaultItemTemplate let-item=\"item\">\n          <span>{{item?.label}}</span>\n        </ng-template>\n        \n        <ng-template #itemTemplate \n          [ngTemplateOutlet]=\"template\"\n          [ngTemplateOutletContext]=\"{item: item}\"  \n        ></ng-template>\n\n      </li>\n    </ul>\n\n  </div>\n\n</div>", styles: [".ddr-dropdown{min-width:100px;position:relative}.ddr-dropdown .form-control[readonly]{cursor:pointer}.ddr-dropdown button{height:100%;width:32px;background-color:transparent;border-color:transparent;position:absolute;right:0px;z-index:999}.ddr-dropdown input{outline:0px;border-radius:0}.ddr-dropdown input:focus{outline:0px!important;-webkit-appearance:none;box-shadow:none!important}.ddr-dropdown__panel-items{position:absolute;width:100%;z-index:9999}.ddr-dropdown__panel-items--no-results{padding:5px;border:1px solid #c8c8c8;font-size:11px;font-style:italic}.ddr-dropdown__panel-items ul{margin:0;padding:0;max-height:200px;overflow:auto;border:1px solid #c8c8c8;border-top:0}.ddr-dropdown__panel-items ul li{list-style:none;cursor:pointer;padding:5px}.ddr-dropdown .input-group-append{margin-left:0}\n"], directives: [{ type: DdrClickOutsideDirective, selector: "[ddrClickOutside]", inputs: ["clickOutsideEnabled", "avoidFirstTime", "clickOutsideDelay"], outputs: ["clickOutside"] }, { type: i2.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { type: i2.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i2.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i1.NgTemplateOutlet, selector: "[ngTemplateOutlet]", inputs: ["ngTemplateOutletContext", "ngTemplateOutlet"] }], animations: [
        trigger('overlayAnimation', [
            state('void', style({
                transform: 'translateY(5%)',
                opacity: 0
            })),
            state('visible', style({
                transform: 'translateY(0)',
                opacity: 1
            })),
            transition('void => visible', animate('225ms ease-out')),
            transition('void => visible', animate('195ms ease-in')),
        ])
    ], encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDropdownComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-dropdown', encapsulation: ViewEncapsulation.None, animations: [
                        trigger('overlayAnimation', [
                            state('void', style({
                                transform: 'translateY(5%)',
                                opacity: 0
                            })),
                            state('visible', style({
                                transform: 'translateY(0)',
                                opacity: 1
                            })),
                            transition('void => visible', animate('225ms ease-out')),
                            transition('void => visible', animate('195ms ease-in')),
                        ])
                    ], providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef(() => DdrDropdownComponent),
                            multi: true
                        }
                    ], template: "<div class=\"ddr-dropdown\" ddrClickOutside (clickOutside)=\"hidePanelItems()\">\n  \n  <div class=\"input-group\" (click)=\"showPanelOptions()\">\n    <input type=\"text\" name=\"value\" [(ngModel)]=\"valueShow\" class=\"form-control\" readonly>\n    <div class=\"input-group-append\">\n      <button>\n        <i class=\"fa fa-caret-down\"></i>\n      </button>\n    </div>\n  </div>\n\n  <div class=\"ddr-dropdown__panel-items\" [@overlayAnimation]=\"{value: 'visible'}\" *ngIf=\"showItems\">\n\n    <div class=\"ddr-dropdown__panel-items--search input-group\">\n      <input type=\"text\" name=\"value\" class=\"form-control\" #search (keyup)=\"filter(search.value)\">\n      <div class=\"input-group-append\">\n        <button>\n          <i class=\"fa fa-search\"></i>\n        </button>\n      </div>\n    </div>\n    <div class=\"ddr-dropdown__panel-items--no-results\" *ngIf=\"optionsShow.length == 0\">\n      <span>{{labelNoResults}}</span>\n    </div>\n    <ul *ngIf=\"optionsShow.length > 0\">\n      <li *ngFor=\"let item of optionsShow\" (click)=\"onSelectItem(item)\">\n        \n        <ng-container *ngTemplateOutlet=\"(template ? itemTemplate : defaultItemTemplate); context: { item }\">\n        </ng-container>\n\n        <ng-template #defaultItemTemplate let-item=\"item\">\n          <span>{{item?.label}}</span>\n        </ng-template>\n        \n        <ng-template #itemTemplate \n          [ngTemplateOutlet]=\"template\"\n          [ngTemplateOutletContext]=\"{item: item}\"  \n        ></ng-template>\n\n      </li>\n    </ul>\n\n  </div>\n\n</div>", styles: [".ddr-dropdown{min-width:100px;position:relative}.ddr-dropdown .form-control[readonly]{cursor:pointer}.ddr-dropdown button{height:100%;width:32px;background-color:transparent;border-color:transparent;position:absolute;right:0px;z-index:999}.ddr-dropdown input{outline:0px;border-radius:0}.ddr-dropdown input:focus{outline:0px!important;-webkit-appearance:none;box-shadow:none!important}.ddr-dropdown__panel-items{position:absolute;width:100%;z-index:9999}.ddr-dropdown__panel-items--no-results{padding:5px;border:1px solid #c8c8c8;font-size:11px;font-style:italic}.ddr-dropdown__panel-items ul{margin:0;padding:0;max-height:200px;overflow:auto;border:1px solid #c8c8c8;border-top:0}.ddr-dropdown__panel-items ul li{list-style:none;cursor:pointer;padding:5px}.ddr-dropdown .input-group-append{margin-left:0}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { options: [{
                type: Input
            }], labelNoResults: [{
                type: Input
            }], selectItem: [{
                type: Output
            }], template: [{
                type: ContentChild,
                args: [TemplateRef, { static: false }]
            }] } });

class DdrDropdownModule {
}
DdrDropdownModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDropdownModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrDropdownModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDropdownModule, declarations: [DdrDropdownComponent], imports: [CommonModule,
        FormsModule,
        DdrClickOutsideModule,
        DdrNgModelBaseModule], exports: [DdrDropdownComponent] });
DdrDropdownModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDropdownModule, imports: [[
            CommonModule,
            FormsModule,
            DdrClickOutsideModule,
            DdrNgModelBaseModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDropdownModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule,
                        FormsModule,
                        DdrClickOutsideModule,
                        DdrNgModelBaseModule
                    ],
                    declarations: [
                        DdrDropdownComponent
                    ],
                    exports: [
                        DdrDropdownComponent
                    ]
                }]
        }] });

class DdrBlockListItemComponent {
    constructor() {
        this.showHeader = true;
        this.showInfoAdditional = true;
        this.showActions = true;
        this.showBorder = true;
        this.actionSelected = new EventEmitter();
        this.closeOptions = new EventEmitter();
        this.showOptions = false;
    }
    ngOnInit() {
        this.showActions = this.showActions && this.blockItem.actions && this.blockItem.actions.length > 0;
    }
    openActions($event) {
        $event.stopPropagation();
        this.showOptions = true;
        this.closeOptions.emit(this.id);
    }
    sendAction($event, action, index) {
        $event.stopPropagation();
        this.showOptions = false;
        let actionSended = cloneDeep(action);
        actionSended.item = this.blockItem.item;
        this.actionSelected.emit(actionSended);
    }
    hideOptions() {
        this.showOptions = false;
    }
}
DdrBlockListItemComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrBlockListItemComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
DdrBlockListItemComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrBlockListItemComponent, selector: "ddr-block-list-item", inputs: { blockItem: "blockItem", id: "id", showHeader: "showHeader", showInfoAdditional: "showInfoAdditional", showActions: "showActions", showBorder: "showBorder", templateHeader: "templateHeader", templateInfoAdditional: "templateInfoAdditional", templateData: "templateData" }, outputs: { actionSelected: "actionSelected", closeOptions: "closeOptions" }, ngImport: i0, template: "<div class=\"ddr-block-list-item row\" [attr.id]=\"id\"\n  [ngStyle]=\"{'border-color': showBorder ? blockItem.borderColor:'', 'border-left-width.px': showBorder ? 10 : 1}\">\n\n  <div class=\"col-md-2 col-12 ddr-block-list-item__info-additional\" *ngIf=\"showInfoAdditional\">\n    <ng-template *ngTemplateOutlet=\"templateInfoAdditional; context: {item: blockItem.item}\"></ng-template>\n  </div>\n  \n  <div class=\"col-12 ddr-block-list-item__data\" [ngClass]=\"{'col-md-10': showInfoAdditional}\">\n    <div class=\"row\" *ngIf=\"showHeader\">\n      <div class=\"col-12\">\n        <ng-template *ngTemplateOutlet=\"templateHeader; context: {item: blockItem.item}\"></ng-template>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <ng-template *ngTemplateOutlet=\"templateData; context: {item: blockItem.item}\"></ng-template>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"ddr-block-list-item__actions\" *ngIf=\"showActions\">\n      <i class=\"fa fa-ellipsis-v\" (click)=\"openActions($event)\"></i>\n      <ul class=\"ddr-block-list-item__actions--options-panel\" ddrClickOutside (clickOutside)=\"hideOptions()\" *ngIf=\"showOptions\">\n        <li *ngFor=\"let action of blockItem.actions; let index = index\" (click)=\"sendAction($event, action, index)\">\n          <div class=\"row no-gutters\">\n            <div class=\"col-11\">\n              <span>{{action.label}}</span>\n            </div>\n            <div class=\"col-1\">\n              <i [class]=\"action.icon\" *ngIf=\"action.icon\"></i>\n            </div>\n          </div>\n        </li>\n      </ul>\n  </div>\n\n</div>", styles: [".ddr-block-list-item{min-width:200px;border-radius:4px;padding:10px;margin:10px 0;cursor:pointer;position:relative;box-shadow:2px 2px 5px 1px #000000bf}.ddr-block-list-item__info-additional{margin:auto;font-size:14px}.ddr-block-list-item__actions{position:absolute;top:10px;right:5px;text-align:center}.ddr-block-list-item__actions i{cursor:pointer;font-size:20px;width:25px}.ddr-block-list-item__actions--options-panel{position:absolute;top:25px;right:0;padding:0;margin:0;list-style-type:none;min-width:200px;z-index:999;border-radius:4px}.ddr-block-list-item__actions--options-panel li{padding:5px;text-align:left}.ddr-block-list-item__actions--options-panel li:hover{cursor:pointer}.ddr-block-list-item__actions--options-panel li:first-child{border-top-left-radius:4px;border-top-right-radius:4px}.ddr-block-list-item__actions--options-panel li:last-child{border-bottom-left-radius:4px;border-bottom-right-radius:4px}\n"], directives: [{ type: i1.NgStyle, selector: "[ngStyle]", inputs: ["ngStyle"] }, { type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1.NgTemplateOutlet, selector: "[ngTemplateOutlet]", inputs: ["ngTemplateOutletContext", "ngTemplateOutlet"] }, { type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: DdrClickOutsideDirective, selector: "[ddrClickOutside]", inputs: ["clickOutsideEnabled", "avoidFirstTime", "clickOutsideDelay"], outputs: ["clickOutside"] }, { type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }], encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrBlockListItemComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-block-list-item', encapsulation: ViewEncapsulation.None, template: "<div class=\"ddr-block-list-item row\" [attr.id]=\"id\"\n  [ngStyle]=\"{'border-color': showBorder ? blockItem.borderColor:'', 'border-left-width.px': showBorder ? 10 : 1}\">\n\n  <div class=\"col-md-2 col-12 ddr-block-list-item__info-additional\" *ngIf=\"showInfoAdditional\">\n    <ng-template *ngTemplateOutlet=\"templateInfoAdditional; context: {item: blockItem.item}\"></ng-template>\n  </div>\n  \n  <div class=\"col-12 ddr-block-list-item__data\" [ngClass]=\"{'col-md-10': showInfoAdditional}\">\n    <div class=\"row\" *ngIf=\"showHeader\">\n      <div class=\"col-12\">\n        <ng-template *ngTemplateOutlet=\"templateHeader; context: {item: blockItem.item}\"></ng-template>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <ng-template *ngTemplateOutlet=\"templateData; context: {item: blockItem.item}\"></ng-template>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"ddr-block-list-item__actions\" *ngIf=\"showActions\">\n      <i class=\"fa fa-ellipsis-v\" (click)=\"openActions($event)\"></i>\n      <ul class=\"ddr-block-list-item__actions--options-panel\" ddrClickOutside (clickOutside)=\"hideOptions()\" *ngIf=\"showOptions\">\n        <li *ngFor=\"let action of blockItem.actions; let index = index\" (click)=\"sendAction($event, action, index)\">\n          <div class=\"row no-gutters\">\n            <div class=\"col-11\">\n              <span>{{action.label}}</span>\n            </div>\n            <div class=\"col-1\">\n              <i [class]=\"action.icon\" *ngIf=\"action.icon\"></i>\n            </div>\n          </div>\n        </li>\n      </ul>\n  </div>\n\n</div>", styles: [".ddr-block-list-item{min-width:200px;border-radius:4px;padding:10px;margin:10px 0;cursor:pointer;position:relative;box-shadow:2px 2px 5px 1px #000000bf}.ddr-block-list-item__info-additional{margin:auto;font-size:14px}.ddr-block-list-item__actions{position:absolute;top:10px;right:5px;text-align:center}.ddr-block-list-item__actions i{cursor:pointer;font-size:20px;width:25px}.ddr-block-list-item__actions--options-panel{position:absolute;top:25px;right:0;padding:0;margin:0;list-style-type:none;min-width:200px;z-index:999;border-radius:4px}.ddr-block-list-item__actions--options-panel li{padding:5px;text-align:left}.ddr-block-list-item__actions--options-panel li:hover{cursor:pointer}.ddr-block-list-item__actions--options-panel li:first-child{border-top-left-radius:4px;border-top-right-radius:4px}.ddr-block-list-item__actions--options-panel li:last-child{border-bottom-left-radius:4px;border-bottom-right-radius:4px}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { blockItem: [{
                type: Input
            }], id: [{
                type: Input
            }], showHeader: [{
                type: Input
            }], showInfoAdditional: [{
                type: Input
            }], showActions: [{
                type: Input
            }], showBorder: [{
                type: Input
            }], templateHeader: [{
                type: Input
            }], templateInfoAdditional: [{
                type: Input
            }], templateData: [{
                type: Input
            }], actionSelected: [{
                type: Output
            }], closeOptions: [{
                type: Output
            }] } });

class DdrBlockListDataItemComponent {
    constructor() { }
    ngOnInit() {
    }
}
DdrBlockListDataItemComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrBlockListDataItemComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
DdrBlockListDataItemComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrBlockListDataItemComponent, selector: "ddr-block-list-data-item", inputs: { label: "label", value: "value" }, ngImport: i0, template: "<div class=\"ddr-block-list-data\">\n    <span class=\"d-block ddr-block-list-data__label\">{{label}}</span>\n    <span class=\"d-block ddr-block-list-data__value\">{{value}}</span>\n</div>", styles: [".ddr-block-list-data{display:inline-block;margin-right:10px}.ddr-block-list-data__label{color:#c8c8c8;font-size:14px}.ddr-block-list-data__value{font-size:12px}\n"], encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrBlockListDataItemComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-block-list-data-item', encapsulation: ViewEncapsulation.None, template: "<div class=\"ddr-block-list-data\">\n    <span class=\"d-block ddr-block-list-data__label\">{{label}}</span>\n    <span class=\"d-block ddr-block-list-data__value\">{{value}}</span>\n</div>", styles: [".ddr-block-list-data{display:inline-block;margin-right:10px}.ddr-block-list-data__label{color:#c8c8c8;font-size:14px}.ddr-block-list-data__value{font-size:12px}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { label: [{
                type: Input
            }], value: [{
                type: Input
            }] } });

class DdrSelectedItem {
}

class DdrBlockListComponent {
    constructor(constants) {
        this.constants = constants;
        this.labelNoResults = 'No results';
        this.showHeader = true;
        this.showInfoAdditional = true;
        this.showActions = true;
        this.showBorder = true;
        this.pagination = this.constants.DdrBlockListConstants.PAGINATION_DEFAULT;
        this.previousLabel = 'Previuos';
        this.nextLabel = 'Next';
        this.page = 1;
        this.itemSelected = new EventEmitter();
        this.actionSelected = new EventEmitter();
    }
    ngOnInit() {
        if (this.pagination < 0) {
            this.pagination = this.constants.DdrBlockListConstants.PAGINATION_DEFAULT;
        }
    }
    ngOnChanges(changes) {
        if (changes) {
            if (changes['blockItems'] && !changes['blockItems'].firstChange) {
                if (((this.page - 1) * this.pagination) == changes['blockItems'].currentValue.length) {
                    this.page = this.page - 1;
                }
            }
        }
    }
    sendAction($event) {
        this.actionSelected.emit($event);
    }
    selectItem(blockItem, index) {
        let selectedItem = new DdrSelectedItem();
        selectedItem.item = blockItem.item;
        selectedItem.index = ((this.page - 1) * this.pagination) + index;
        this.itemSelected.emit(selectedItem);
    }
    closeAllOptionsExcept(id) {
        this.blocks.forEach(block => {
            if (block.id !== id) {
                block.showOptions = false;
            }
        });
    }
}
DdrBlockListComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrBlockListComponent, deps: [{ token: DdrConstantsService }], target: i0.ɵɵFactoryTarget.Component });
DdrBlockListComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrBlockListComponent, selector: "ddr-block-list", inputs: { blockItems: "blockItems", labelNoResults: "labelNoResults", showHeader: "showHeader", showInfoAdditional: "showInfoAdditional", showActions: "showActions", showBorder: "showBorder", pagination: "pagination", previousLabel: "previousLabel", nextLabel: "nextLabel" }, outputs: { itemSelected: "itemSelected", actionSelected: "actionSelected" }, queries: [{ propertyName: "templateHeader", first: true, predicate: ["templateHeader"], descendants: true }, { propertyName: "templateInfoAdditional", first: true, predicate: ["templateInfoAdditional"], descendants: true }, { propertyName: "templateData", first: true, predicate: ["templateData"], descendants: true }], viewQueries: [{ propertyName: "blocks", predicate: DdrBlockListItemComponent, descendants: true }], usesOnChanges: true, ngImport: i0, template: "\n<div class=\"ddr-block-list row\">\n  <div class=\"col-12\">\n\n    <div class=\"row\" *ngIf=\"blockItems?.length == 0\">\n      <div class=\"col-12 text-center\">\n        <span>{{labelNoResults}}</span>\n      </div>\n    </div>\n\n    <div class=\"row\" *ngIf=\"blockItems?.length > 0\">\n      <div class=\"col-12\">\n        \n        <ddr-block-list-item *ngFor=\"let blockItem of blockItems | paginate: { itemsPerPage: pagination, currentPage: page }; let index = index\"\n          [blockItem]=\"blockItem\"\n          [id]=\"'block-item-' + index\"\n          [showHeader]=\"showHeader\"\n          [showInfoAdditional]=\"showInfoAdditional\"\n          [showActions]=\"showActions\"\n          [showBorder]=\"showBorder\"\n          [templateHeader]=\"templateHeader\"\n          [templateInfoAdditional]=\"templateInfoAdditional\"\n          [templateData]=\"templateData\"\n          (actionSelected)=\"sendAction($event)\"\n          (closeOptions)=\"closeAllOptionsExcept($event)\"\n          (click)=\"selectItem(blockItem, index)\">\n        </ddr-block-list-item>\n\n      </div>\n    </div>\n    \n    <!--Pagination-->\n    <div class=\"row\">\n      <div class=\"col-12 p-4 text-center\">\n        <pagination-controls \n          [autoHide]=\"true\"\n          [previousLabel]=\"previousLabel\"\n          [nextLabel]=\"nextLabel\"\n          (pageChange)=\"page = $event\">\n        </pagination-controls>\n      </div>\n    </div>\n\n  </div>\n</div>", styles: [""], components: [{ type: DdrBlockListItemComponent, selector: "ddr-block-list-item", inputs: ["blockItem", "id", "showHeader", "showInfoAdditional", "showActions", "showBorder", "templateHeader", "templateInfoAdditional", "templateData"], outputs: ["actionSelected", "closeOptions"] }, { type: i3.PaginationControlsComponent, selector: "pagination-controls", inputs: ["maxSize", "previousLabel", "nextLabel", "screenReaderPaginationLabel", "screenReaderPageLabel", "screenReaderCurrentLabel", "directionLinks", "autoHide", "responsive", "id"], outputs: ["pageChange", "pageBoundsCorrection"] }], directives: [{ type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }], pipes: { "paginate": i3.PaginatePipe }, encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrBlockListComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-block-list', encapsulation: ViewEncapsulation.None, template: "\n<div class=\"ddr-block-list row\">\n  <div class=\"col-12\">\n\n    <div class=\"row\" *ngIf=\"blockItems?.length == 0\">\n      <div class=\"col-12 text-center\">\n        <span>{{labelNoResults}}</span>\n      </div>\n    </div>\n\n    <div class=\"row\" *ngIf=\"blockItems?.length > 0\">\n      <div class=\"col-12\">\n        \n        <ddr-block-list-item *ngFor=\"let blockItem of blockItems | paginate: { itemsPerPage: pagination, currentPage: page }; let index = index\"\n          [blockItem]=\"blockItem\"\n          [id]=\"'block-item-' + index\"\n          [showHeader]=\"showHeader\"\n          [showInfoAdditional]=\"showInfoAdditional\"\n          [showActions]=\"showActions\"\n          [showBorder]=\"showBorder\"\n          [templateHeader]=\"templateHeader\"\n          [templateInfoAdditional]=\"templateInfoAdditional\"\n          [templateData]=\"templateData\"\n          (actionSelected)=\"sendAction($event)\"\n          (closeOptions)=\"closeAllOptionsExcept($event)\"\n          (click)=\"selectItem(blockItem, index)\">\n        </ddr-block-list-item>\n\n      </div>\n    </div>\n    \n    <!--Pagination-->\n    <div class=\"row\">\n      <div class=\"col-12 p-4 text-center\">\n        <pagination-controls \n          [autoHide]=\"true\"\n          [previousLabel]=\"previousLabel\"\n          [nextLabel]=\"nextLabel\"\n          (pageChange)=\"page = $event\">\n        </pagination-controls>\n      </div>\n    </div>\n\n  </div>\n</div>", styles: [""] }]
        }], ctorParameters: function () { return [{ type: DdrConstantsService }]; }, propDecorators: { blockItems: [{
                type: Input
            }], labelNoResults: [{
                type: Input
            }], showHeader: [{
                type: Input
            }], showInfoAdditional: [{
                type: Input
            }], showActions: [{
                type: Input
            }], showBorder: [{
                type: Input
            }], pagination: [{
                type: Input
            }], previousLabel: [{
                type: Input
            }], nextLabel: [{
                type: Input
            }], itemSelected: [{
                type: Output
            }], actionSelected: [{
                type: Output
            }], templateHeader: [{
                type: ContentChild,
                args: ["templateHeader", { static: false }]
            }], templateInfoAdditional: [{
                type: ContentChild,
                args: ["templateInfoAdditional", { static: false }]
            }], templateData: [{
                type: ContentChild,
                args: ["templateData", { static: false }]
            }], blocks: [{
                type: ViewChildren,
                args: [DdrBlockListItemComponent]
            }] } });

class DdrBlockListModule {
}
DdrBlockListModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrBlockListModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrBlockListModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrBlockListModule, declarations: [DdrBlockListItemComponent,
        DdrBlockListDataItemComponent,
        DdrBlockListComponent], imports: [CommonModule,
        DdrClickOutsideModule,
        NgxPaginationModule], exports: [DdrBlockListComponent,
        DdrBlockListDataItemComponent] });
DdrBlockListModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrBlockListModule, imports: [[
            CommonModule,
            DdrClickOutsideModule,
            NgxPaginationModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrBlockListModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule,
                        DdrClickOutsideModule,
                        NgxPaginationModule
                    ],
                    declarations: [
                        DdrBlockListItemComponent,
                        DdrBlockListDataItemComponent,
                        DdrBlockListComponent
                    ],
                    exports: [
                        DdrBlockListComponent,
                        DdrBlockListDataItemComponent
                    ]
                }]
        }] });

class DdrTabItemComponent {
    constructor() {
        this.selected = false;
    }
}
DdrTabItemComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTabItemComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
DdrTabItemComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrTabItemComponent, selector: "ddr-tab-item", inputs: { title: "title" }, viewQueries: [{ propertyName: "contentTemplate", first: true, predicate: ["contentTemplate"], descendants: true }], ngImport: i0, template: "<ng-template #contentTemplate>\n  <ng-content></ng-content>\n</ng-template>", styles: [""], encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTabItemComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-tab-item', encapsulation: ViewEncapsulation.None, template: "<ng-template #contentTemplate>\n  <ng-content></ng-content>\n</ng-template>", styles: [""] }]
        }], ctorParameters: function () { return []; }, propDecorators: { title: [{
                type: Input
            }], contentTemplate: [{
                type: ViewChild,
                args: ['contentTemplate', { static: false }]
            }] } });

class DdrTabsComponent {
    constructor() { }
    ngAfterViewInit() {
        if (this.tabsItems.toArray().length > 0) {
            this.open(this.tabsItems.toArray()[0]);
        }
    }
    open(tab) {
        this.tabsItems.forEach(t => t.selected = false);
        tab.selected = true;
        this.contentTemplate = tab.contentTemplate;
    }
}
DdrTabsComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTabsComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
DdrTabsComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrTabsComponent, selector: "ddr-tabs", queries: [{ propertyName: "tabsItems", predicate: DdrTabItemComponent }], ngImport: i0, template: "<div class=\"ddr-tabs\">\n\n  <div *ngFor=\"let tab of tabsItems\" \n      class=\"ddr-tabs__title\"\n      (click)=\"open(tab)\"\n      [ngClass]=\"{'ddr-tabs__title--selected': tab.selected}\">\n    <span>{{tab.title}}</span>\n  </div>\n\n  <div class=\"ddr-tabs__content\" *ngIf=\"contentTemplate\">\n    <ng-template *ngTemplateOutlet=\"contentTemplate\"></ng-template>\n  </div>\n\n</div>", styles: [".ddr-tabs__title{text-align:center;font-size:18px;padding:15px;display:inline-block;cursor:pointer}.ddr-tabs__title--selected{border-bottom-width:3px;border-bottom-style:solid}.ddr-tabs__content{padding:10px}\n"], directives: [{ type: i1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1.NgTemplateOutlet, selector: "[ngTemplateOutlet]", inputs: ["ngTemplateOutletContext", "ngTemplateOutlet"] }], encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTabsComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-tabs', encapsulation: ViewEncapsulation.None, template: "<div class=\"ddr-tabs\">\n\n  <div *ngFor=\"let tab of tabsItems\" \n      class=\"ddr-tabs__title\"\n      (click)=\"open(tab)\"\n      [ngClass]=\"{'ddr-tabs__title--selected': tab.selected}\">\n    <span>{{tab.title}}</span>\n  </div>\n\n  <div class=\"ddr-tabs__content\" *ngIf=\"contentTemplate\">\n    <ng-template *ngTemplateOutlet=\"contentTemplate\"></ng-template>\n  </div>\n\n</div>", styles: [".ddr-tabs__title{text-align:center;font-size:18px;padding:15px;display:inline-block;cursor:pointer}.ddr-tabs__title--selected{border-bottom-width:3px;border-bottom-style:solid}.ddr-tabs__content{padding:10px}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { tabsItems: [{
                type: ContentChildren,
                args: [DdrTabItemComponent]
            }] } });

class DdrTabsModule {
}
DdrTabsModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTabsModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrTabsModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTabsModule, declarations: [DdrTabsComponent,
        DdrTabItemComponent], imports: [CommonModule], exports: [DdrTabsComponent,
        DdrTabItemComponent] });
DdrTabsModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTabsModule, imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTabsModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        DdrTabsComponent,
                        DdrTabItemComponent
                    ],
                    exports: [
                        DdrTabsComponent,
                        DdrTabItemComponent
                    ]
                }]
        }] });

class DdrAccordionComponent {
    constructor(constants) {
        this.constants = constants;
        this.open = false;
        this.state = this.constants.DdrAccordionConstants.CLOSE;
        this.clickOpen = new EventEmitter();
    }
    ngAfterViewInit() {
        this.state = this.open ? this.constants.DdrAccordionConstants.OPEN : this.constants.DdrAccordionConstants.CLOSE;
        if (this.open) {
            this.contentAccordion.nativeElement.style.overflow = 'hidden';
            setTimeout(() => {
                this.contentAccordion.nativeElement.style.overflow = 'inherit';
            }, 400);
        }
    }
    openClose() {
        if (this.state === this.constants.DdrAccordionConstants.OPEN) {
            this.state = this.constants.DdrAccordionConstants.CLOSE;
        }
        else {
            this.state = this.constants.DdrAccordionConstants.OPEN;
        }
        this.contentAccordion.nativeElement.style.overflow = 'hidden';
        if (this.state == this.constants.DdrAccordionConstants.CLOSE) {
            setTimeout(() => {
                this.open = !this.open;
                this.clickOpen.emit(this.open);
            }, 400);
        }
        else {
            this.open = !this.open;
            this.clickOpen.emit(this.open);
            setTimeout(() => {
                this.contentAccordion.nativeElement.style.overflow = 'inherit';
            }, 400);
        }
    }
}
DdrAccordionComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrAccordionComponent, deps: [{ token: DdrConstantsService }], target: i0.ɵɵFactoryTarget.Component });
DdrAccordionComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrAccordionComponent, selector: "ddr-accordion", inputs: { title: "title", open: "open" }, outputs: { clickOpen: "clickOpen" }, viewQueries: [{ propertyName: "contentAccordion", first: true, predicate: ["contentAccordion"], descendants: true }], ngImport: i0, template: "<div class=\"ddr-accordion\">\n\n  <div class=\"ddr-accordion__title\" \n    [ngClass]=\"{'ddr-accordion__title--is-opened': open,'ddr-accordion__title--is-closed': !open}\"\n    (click)=\"openClose()\">\n\n    <div class=\"row\">\n      <div class=\"col-md-11 col-10 text-left\">\n        <span class=\"ddr-accordion__title--text\">{{title}}</span>\n      </div>\n      <div class=\"col-md-1 col-2 text-right\">\n        <i class=\"fa fa-angle-down ddr-accordion__title--icon\" *ngIf=\"!open\"></i>\n        <i class=\"fa fa-angle-up ddr-accordion__title--icon\" *ngIf=\"open\"></i>\n      </div>\n    </div>\n\n  </div>\n\n  <div class=\"ddr-accordion__content\" #contentAccordion\n    [@slideInOut]=\"state\"\n    [ngClass]=\"{'ddr-accordion__content--is-opened': open, 'ddr-accordion__content--is-closed': !open }\" \n    style=\"height:0px\">\n    \n    <ng-content select=\"[content-accordion]\"></ng-content>\n\n  </div>\n\n</div>", styles: [".ddr-accordion{border-radius:4px;margin-bottom:10px;box-shadow:2px 2px 2px 1px #000000bf}.ddr-accordion__title{padding:5px;border-radius:4px;cursor:pointer}.ddr-accordion__title--text{font-size:24px}.ddr-accordion__title--icon{font-size:22px;margin-right:10px;margin-top:5px}.ddr-accordion__title--is-opened{border-bottom-left-radius:0;border-bottom-right-radius:0}.ddr-accordion__title--is-closed{border-bottom-left-radius:4px;border-bottom-right-radius:4px}.ddr-accordion__content--is-opened{padding:10px;border-top:none;border-radius:0 0 4px 4px/0px 0px 4px 4px;overflow:inherit;border-left-width:1px;border-left-style:solid;border-right-width:1px;border-right-style:solid;border-bottom-width:1px;border-bottom-style:solid}.ddr-accordion__content--is-closed{padding:0;border:none;overflow:hidden}\n"], directives: [{ type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }], animations: [
        trigger('slideInOut', [
            state('open', style({ height: '*' })),
            state('close', style({ height: 0 })),
            transition('open <=> close', group([
                animate('400ms')
            ]))
        ])
    ], encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrAccordionComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-accordion', encapsulation: ViewEncapsulation.None, animations: [
                        trigger('slideInOut', [
                            state('open', style({ height: '*' })),
                            state('close', style({ height: 0 })),
                            transition('open <=> close', group([
                                animate('400ms')
                            ]))
                        ])
                    ], template: "<div class=\"ddr-accordion\">\n\n  <div class=\"ddr-accordion__title\" \n    [ngClass]=\"{'ddr-accordion__title--is-opened': open,'ddr-accordion__title--is-closed': !open}\"\n    (click)=\"openClose()\">\n\n    <div class=\"row\">\n      <div class=\"col-md-11 col-10 text-left\">\n        <span class=\"ddr-accordion__title--text\">{{title}}</span>\n      </div>\n      <div class=\"col-md-1 col-2 text-right\">\n        <i class=\"fa fa-angle-down ddr-accordion__title--icon\" *ngIf=\"!open\"></i>\n        <i class=\"fa fa-angle-up ddr-accordion__title--icon\" *ngIf=\"open\"></i>\n      </div>\n    </div>\n\n  </div>\n\n  <div class=\"ddr-accordion__content\" #contentAccordion\n    [@slideInOut]=\"state\"\n    [ngClass]=\"{'ddr-accordion__content--is-opened': open, 'ddr-accordion__content--is-closed': !open }\" \n    style=\"height:0px\">\n    \n    <ng-content select=\"[content-accordion]\"></ng-content>\n\n  </div>\n\n</div>", styles: [".ddr-accordion{border-radius:4px;margin-bottom:10px;box-shadow:2px 2px 2px 1px #000000bf}.ddr-accordion__title{padding:5px;border-radius:4px;cursor:pointer}.ddr-accordion__title--text{font-size:24px}.ddr-accordion__title--icon{font-size:22px;margin-right:10px;margin-top:5px}.ddr-accordion__title--is-opened{border-bottom-left-radius:0;border-bottom-right-radius:0}.ddr-accordion__title--is-closed{border-bottom-left-radius:4px;border-bottom-right-radius:4px}.ddr-accordion__content--is-opened{padding:10px;border-top:none;border-radius:0 0 4px 4px/0px 0px 4px 4px;overflow:inherit;border-left-width:1px;border-left-style:solid;border-right-width:1px;border-right-style:solid;border-bottom-width:1px;border-bottom-style:solid}.ddr-accordion__content--is-closed{padding:0;border:none;overflow:hidden}\n"] }]
        }], ctorParameters: function () { return [{ type: DdrConstantsService }]; }, propDecorators: { title: [{
                type: Input
            }], open: [{
                type: Input
            }], clickOpen: [{
                type: Output
            }], contentAccordion: [{
                type: ViewChild,
                args: ["contentAccordion"]
            }] } });

class DdrAccordionModule {
}
DdrAccordionModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrAccordionModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrAccordionModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrAccordionModule, declarations: [DdrAccordionComponent], imports: [CommonModule], exports: [DdrAccordionComponent] });
DdrAccordionModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrAccordionModule, imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrAccordionModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        DdrAccordionComponent
                    ],
                    exports: [
                        DdrAccordionComponent
                    ]
                }]
        }] });

class DdrLibraryModule {
}
DdrLibraryModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLibraryModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrLibraryModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLibraryModule, imports: [DdrJoinPipeModule,
        DdrSpinnerModule,
        DdrToastModule,
        DdrClickOutsideModule,
        DdrDropdownModule,
        DdrLoadIframeModule,
        DdrConfigModule,
        DdrDetailModule,
        DdrBlockListModule,
        DdrTabsModule,
        DdrResolutionModule,
        DdrAccordionModule,
        DdrTranslateModule,
        DdrNgModelBaseModule,
        DdrToggleModule,
        DdrModalModule], exports: [DdrJoinPipeModule,
        DdrSpinnerModule,
        DdrToastModule,
        DdrClickOutsideModule,
        DdrDropdownModule,
        DdrLoadIframeModule,
        DdrConfigModule,
        DdrDetailModule,
        DdrBlockListModule,
        DdrTabsModule,
        DdrResolutionModule,
        DdrAccordionModule,
        DdrTranslateModule,
        DdrNgModelBaseModule,
        DdrToggleModule,
        DdrModalModule] });
DdrLibraryModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLibraryModule, providers: [
        DdrConstantsService,
        DdrThemeService
    ], imports: [[
            DdrJoinPipeModule,
            DdrSpinnerModule,
            DdrToastModule,
            DdrClickOutsideModule,
            DdrDropdownModule,
            DdrLoadIframeModule,
            DdrConfigModule,
            DdrDetailModule,
            DdrBlockListModule,
            DdrTabsModule,
            DdrResolutionModule,
            DdrAccordionModule,
            DdrTranslateModule,
            DdrNgModelBaseModule,
            DdrToggleModule,
            DdrModalModule
        ], DdrJoinPipeModule,
        DdrSpinnerModule,
        DdrToastModule,
        DdrClickOutsideModule,
        DdrDropdownModule,
        DdrLoadIframeModule,
        DdrConfigModule,
        DdrDetailModule,
        DdrBlockListModule,
        DdrTabsModule,
        DdrResolutionModule,
        DdrAccordionModule,
        DdrTranslateModule,
        DdrNgModelBaseModule,
        DdrToggleModule,
        DdrModalModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLibraryModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [],
                    imports: [
                        DdrJoinPipeModule,
                        DdrSpinnerModule,
                        DdrToastModule,
                        DdrClickOutsideModule,
                        DdrDropdownModule,
                        DdrLoadIframeModule,
                        DdrConfigModule,
                        DdrDetailModule,
                        DdrBlockListModule,
                        DdrTabsModule,
                        DdrResolutionModule,
                        DdrAccordionModule,
                        DdrTranslateModule,
                        DdrNgModelBaseModule,
                        DdrToggleModule,
                        DdrModalModule
                    ],
                    exports: [
                        DdrJoinPipeModule,
                        DdrSpinnerModule,
                        DdrToastModule,
                        DdrClickOutsideModule,
                        DdrDropdownModule,
                        DdrLoadIframeModule,
                        DdrConfigModule,
                        DdrDetailModule,
                        DdrBlockListModule,
                        DdrTabsModule,
                        DdrResolutionModule,
                        DdrAccordionModule,
                        DdrTranslateModule,
                        DdrNgModelBaseModule,
                        DdrToggleModule,
                        DdrModalModule
                    ],
                    providers: [
                        DdrConstantsService,
                        DdrThemeService
                    ]
                }]
        }] });

class DdrBlockItem {
}

class DdrSelectItem {
    constructor(label, value) {
        this.label = label;
        this.value = value;
    }
}

class DdrAction {
}

// DDR library

/**
 * Generated bundle index. Do not edit.
 */

export { DdrAccordionComponent, DdrAccordionModule, DdrAction, DdrBlockItem, DdrBlockListComponent, DdrBlockListDataItemComponent, DdrBlockListModule, DdrClickOutsideDirective, DdrClickOutsideModule, DdrConfigModule, DdrConfigPipe, DdrConfigService, DdrConstantsService, DdrDetailComponent, DdrDetailModule, DdrDropdownComponent, DdrDropdownModule, DdrJoinPipe, DdrJoinPipeModule, DdrLibraryModule, DdrLoadIframeDirective, DdrLoadIframeModule, DdrModalComponent, DdrModalModule, DdrModalService, DdrNgModelBase, DdrNgModelBaseModule, DdrResolutionDirective, DdrResolutionModule, DdrResolutionService, DdrSelectItem, DdrSelectedItem, DdrSpinnerComponent, DdrSpinnerModule, DdrSpinnerService, DdrTabItemComponent, DdrTabsComponent, DdrTabsModule, DdrThemeService, DdrToastComponent, DdrToastModule, DdrToastService, DdrToggleComponent, DdrToggleModule, DdrTranslateModule, DdrTranslatePipe, DdrTranslateService };
