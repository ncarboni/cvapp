import { ElementRef, EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DdrClickOutsideDirective {
    private elementRef;
    clickOutsideEnabled: boolean;
    avoidFirstTime: boolean;
    clickOutsideDelay: number;
    clickOutside: EventEmitter<MouseEvent>;
    firstTime: boolean;
    constructor(elementRef: ElementRef);
    onDocumentClick(event: MouseEvent): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrClickOutsideDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<DdrClickOutsideDirective, "[ddrClickOutside]", never, { "clickOutsideEnabled": "clickOutsideEnabled"; "avoidFirstTime": "avoidFirstTime"; "clickOutsideDelay": "clickOutsideDelay"; }, { "clickOutside": "clickOutside"; }, never>;
}
