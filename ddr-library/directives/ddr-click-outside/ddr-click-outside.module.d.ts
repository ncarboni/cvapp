import * as i0 from "@angular/core";
import * as i1 from "./ddr-click-outside.directive";
import * as i2 from "@angular/common";
export declare class DdrClickOutsideModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrClickOutsideModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DdrClickOutsideModule, [typeof i1.DdrClickOutsideDirective], [typeof i2.CommonModule], [typeof i1.DdrClickOutsideDirective]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DdrClickOutsideModule>;
}
