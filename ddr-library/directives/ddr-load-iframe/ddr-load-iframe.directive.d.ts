import { EventEmitter, ElementRef } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DdrLoadIframeDirective {
    private el;
    loadIframe: EventEmitter<boolean>;
    constructor(el: ElementRef);
    onLoad(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrLoadIframeDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<DdrLoadIframeDirective, "[ddrLoadIframe]", never, {}, { "loadIframe": "loadIframe"; }, never>;
}
