import * as i0 from "@angular/core";
import * as i1 from "./ddr-load-iframe.directive";
import * as i2 from "@angular/common";
export declare class DdrLoadIframeModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrLoadIframeModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DdrLoadIframeModule, [typeof i1.DdrLoadIframeDirective], [typeof i2.CommonModule], [typeof i1.DdrLoadIframeDirective]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DdrLoadIframeModule>;
}
