import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DdrLibraryComponent } from './ddr-library.component';

describe('DdrLibraryComponent', () => {
  let component: DdrLibraryComponent;
  let fixture: ComponentFixture<DdrLibraryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DdrLibraryComponent]
    });
    fixture = TestBed.createComponent(DdrLibraryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
