import { NgModule } from '@angular/core';
import { DdrLibraryComponent } from './ddr-library.component';



@NgModule({
  declarations: [
    DdrLibraryComponent
  ],
  imports: [
  ],
  exports: [
    DdrLibraryComponent
  ]
})
export class DdrLibraryModule { }
