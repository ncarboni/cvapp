import { TestBed } from '@angular/core/testing';

import { DdrLibraryService } from './ddr-library.service';

describe('DdrLibraryService', () => {
  let service: DdrLibraryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DdrLibraryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
