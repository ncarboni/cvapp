import { DdrConfigService } from './ddr-config.service';
import { PipeTransform } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DdrConfigPipe implements PipeTransform {
    private ddrConfig;
    constructor(ddrConfig: DdrConfigService);
    transform(path: string): any;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrConfigPipe, never>;
    static ɵpipe: i0.ɵɵPipeDeclaration<DdrConfigPipe, "ddrConfig">;
}
