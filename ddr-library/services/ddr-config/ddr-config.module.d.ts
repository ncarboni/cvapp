import * as i0 from "@angular/core";
import * as i1 from "./ddr-config.pipe";
import * as i2 from "@angular/common/http";
export declare class DdrConfigModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrConfigModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DdrConfigModule, [typeof i1.DdrConfigPipe], [typeof i2.HttpClientModule], [typeof i1.DdrConfigPipe]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DdrConfigModule>;
}
