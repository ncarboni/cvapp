import { HttpClient } from '@angular/common/http';
import * as i0 from "@angular/core";
export declare class DdrConfigService {
    private http;
    private _data;
    constructor(http: HttpClient);
    getDataFromJSON(pathJSON: string): Promise<unknown>;
    getData(path: string): any;
    getAllData(): any;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrConfigService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<DdrConfigService>;
}
