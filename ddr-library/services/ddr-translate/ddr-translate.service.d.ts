import { HttpClient } from '@angular/common/http';
import * as i0 from "@angular/core";
export declare class DdrTranslateService {
    private http;
    private _data;
    constructor(http: HttpClient);
    getData(path: string): Promise<unknown>;
    getTranslate(word: string): any;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrTranslateService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<DdrTranslateService>;
}
