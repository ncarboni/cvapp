import { DdrTranslateService } from './ddr-translate.service';
import { PipeTransform } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DdrTranslatePipe implements PipeTransform {
    private translateService;
    constructor(translateService: DdrTranslateService);
    transform(value: string): any;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrTranslatePipe, never>;
    static ɵpipe: i0.ɵɵPipeDeclaration<DdrTranslatePipe, "ddrTranslate">;
}
