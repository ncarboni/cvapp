import * as i0 from "@angular/core";
import * as i1 from "./ddr-translate.pipe";
import * as i2 from "@angular/common";
import * as i3 from "@angular/common/http";
export declare class DdrTranslateModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrTranslateModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DdrTranslateModule, [typeof i1.DdrTranslatePipe], [typeof i2.CommonModule, typeof i3.HttpClientModule], [typeof i1.DdrTranslatePipe]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DdrTranslateModule>;
}
