import { DdrConstantsService } from './../ddr-constants.service';
import * as i0 from "@angular/core";
export declare class DdrResolutionService {
    private constants;
    private _size;
    get size(): string;
    set size(value: string);
    constructor(constants: DdrConstantsService);
    setSize(width: number): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrResolutionService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<DdrResolutionService>;
}
