import { DdrResolutionService } from './ddr-resolution.service';
import { OnInit } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DdrResolutionDirective implements OnInit {
    private resolution;
    constructor(resolution: DdrResolutionService);
    ngOnInit(): void;
    onResize(event: any): void;
    sendSize(width: number): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrResolutionDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<DdrResolutionDirective, "[ddrResolution]", never, {}, {}, never>;
}
