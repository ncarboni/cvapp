import * as i0 from "@angular/core";
import * as i1 from "./ddr-resolution.directive";
import * as i2 from "@angular/common";
export declare class DdrResolutionModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrResolutionModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DdrResolutionModule, [typeof i1.DdrResolutionDirective], [typeof i2.CommonModule], [typeof i1.DdrResolutionDirective]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DdrResolutionModule>;
}
