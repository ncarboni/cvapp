import { DdrConstantsService } from './ddr-constants.service';
import * as i0 from "@angular/core";
export declare class DdrThemeService {
    private constans;
    private _theme;
    get theme(): string;
    private themesAvailables;
    constructor(constans: DdrConstantsService);
    changeTheme(theme: string): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrThemeService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<DdrThemeService>;
}
