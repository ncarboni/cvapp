import { DdrToastConstants, DdrBlockListConstants, DdrResolutionConstants, DdrAccordionConstants, DdrModalTypeConstants, DdrThemeConstants } from './../constans/ddr-constans';
import * as i0 from "@angular/core";
export declare class DdrConstantsService {
    DdrToastConstants: typeof DdrToastConstants;
    DdrBlockListConstants: typeof DdrBlockListConstants;
    DdrResolutionConstants: typeof DdrResolutionConstants;
    DdrAccordionConstants: typeof DdrAccordionConstants;
    DdrModalTypeConstants: typeof DdrModalTypeConstants;
    DdrThemeConstants: typeof DdrThemeConstants;
    constructor();
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrConstantsService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<DdrConstantsService>;
}
