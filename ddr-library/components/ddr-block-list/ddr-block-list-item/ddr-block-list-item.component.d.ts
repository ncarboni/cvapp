import { DdrAction } from './../../../common/ddr-action';
import { DdrBlockItem } from './../bean/ddr-block-item';
import { OnInit, EventEmitter, TemplateRef } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DdrBlockListItemComponent<T> implements OnInit {
    blockItem: DdrBlockItem<T>;
    id: string;
    showHeader: boolean;
    showInfoAdditional: boolean;
    showActions: boolean;
    showBorder: boolean;
    templateHeader: TemplateRef<any>;
    templateInfoAdditional: TemplateRef<any>;
    templateData: TemplateRef<any>;
    actionSelected: EventEmitter<DdrAction<T>>;
    closeOptions: EventEmitter<string>;
    showOptions: boolean;
    constructor();
    ngOnInit(): void;
    openActions($event: MouseEvent): void;
    sendAction($event: MouseEvent, action: DdrAction<T>, index: number): void;
    hideOptions(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrBlockListItemComponent<any>, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DdrBlockListItemComponent<any>, "ddr-block-list-item", never, { "blockItem": "blockItem"; "id": "id"; "showHeader": "showHeader"; "showInfoAdditional": "showInfoAdditional"; "showActions": "showActions"; "showBorder": "showBorder"; "templateHeader": "templateHeader"; "templateInfoAdditional": "templateInfoAdditional"; "templateData": "templateData"; }, { "actionSelected": "actionSelected"; "closeOptions": "closeOptions"; }, never, never>;
}
