import { DdrAction } from './../../../common/ddr-action';
export declare class DdrBlockItem<T> {
    item: T;
    borderColor?: string;
    actions?: DdrAction<T>[];
}
