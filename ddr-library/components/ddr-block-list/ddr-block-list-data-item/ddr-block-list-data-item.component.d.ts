import { OnInit } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DdrBlockListDataItemComponent implements OnInit {
    label: string;
    value: string;
    constructor();
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrBlockListDataItemComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DdrBlockListDataItemComponent, "ddr-block-list-data-item", never, { "label": "label"; "value": "value"; }, {}, never, never>;
}
