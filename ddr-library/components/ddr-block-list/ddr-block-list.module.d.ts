import * as i0 from "@angular/core";
import * as i1 from "./ddr-block-list-item/ddr-block-list-item.component";
import * as i2 from "./ddr-block-list-data-item/ddr-block-list-data-item.component";
import * as i3 from "./ddr-block-list/ddr-block-list.component";
import * as i4 from "@angular/common";
import * as i5 from "../../directives/ddr-click-outside/ddr-click-outside.module";
import * as i6 from "ngx-pagination";
export declare class DdrBlockListModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrBlockListModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DdrBlockListModule, [typeof i1.DdrBlockListItemComponent, typeof i2.DdrBlockListDataItemComponent, typeof i3.DdrBlockListComponent], [typeof i4.CommonModule, typeof i5.DdrClickOutsideModule, typeof i6.NgxPaginationModule], [typeof i3.DdrBlockListComponent, typeof i2.DdrBlockListDataItemComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DdrBlockListModule>;
}
