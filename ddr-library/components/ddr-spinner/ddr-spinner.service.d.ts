import * as i0 from "@angular/core";
export declare class DdrSpinnerService {
    private _show;
    constructor();
    get show(): boolean;
    showSpinner(): void;
    hideSpinner(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrSpinnerService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<DdrSpinnerService>;
}
