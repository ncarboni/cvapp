import { DdrSpinnerService } from './ddr-spinner.service';
import { OnInit } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DdrSpinnerComponent implements OnInit {
    ddrSpinner: DdrSpinnerService;
    embedded: boolean;
    pathImg: string;
    constructor(ddrSpinner: DdrSpinnerService);
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrSpinnerComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DdrSpinnerComponent, "ddr-spinner", never, { "embedded": "embedded"; "pathImg": "pathImg"; }, {}, never, never>;
}
