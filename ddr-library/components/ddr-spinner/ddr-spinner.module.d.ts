import * as i0 from "@angular/core";
import * as i1 from "./ddr-spinner.component";
import * as i2 from "@angular/common";
export declare class DdrSpinnerModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrSpinnerModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DdrSpinnerModule, [typeof i1.DdrSpinnerComponent], [typeof i2.CommonModule], [typeof i1.DdrSpinnerComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DdrSpinnerModule>;
}
