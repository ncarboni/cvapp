import { DdrConstantsService } from './../../services/ddr-constants.service';
import { DdrToast } from './bean/ddr-toast';
import * as i0 from "@angular/core";
export declare class DdrToastService {
    private constans;
    private _toasts;
    private _timeout;
    get toasts(): DdrToast[];
    get timeout(): number;
    set timeout(value: number);
    constructor(constans: DdrConstantsService);
    addInfoMessage(title: string, message: string): void;
    addWarningMessage(title: string, message: string): void;
    addErrorMessage(title: string, message: string): void;
    addSuccessMessage(title: string, message: string): void;
    private addMessage;
    closeToast(toast: DdrToast): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrToastService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<DdrToastService>;
}
