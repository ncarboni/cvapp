export declare class DdrToast {
    title: string;
    message: string;
    type: string;
    constructor(title?: string, message?: string, type?: string);
}
