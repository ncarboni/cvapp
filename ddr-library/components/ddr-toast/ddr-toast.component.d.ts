import { DdrToastService } from './ddr-toast.service';
import { OnInit } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DdrToastComponent implements OnInit {
    toastService: DdrToastService;
    timeout: number;
    constructor(toastService: DdrToastService);
    ngOnInit(): void;
    closeToast(toast: any): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrToastComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DdrToastComponent, "ddr-toast", never, { "timeout": "timeout"; }, {}, never, never>;
}
