import * as i0 from "@angular/core";
import * as i1 from "./ddr-toast.component";
import * as i2 from "@angular/common";
export declare class DdrToastModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrToastModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DdrToastModule, [typeof i1.DdrToastComponent], [typeof i2.CommonModule], [typeof i1.DdrToastComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DdrToastModule>;
}
