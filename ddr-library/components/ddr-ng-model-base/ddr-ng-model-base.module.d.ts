import * as i0 from "@angular/core";
import * as i1 from "./ddr-ng-model-base.component";
import * as i2 from "@angular/common";
export declare class DdrNgModelBaseModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrNgModelBaseModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DdrNgModelBaseModule, [typeof i1.DdrNgModelBase], [typeof i2.CommonModule], [typeof i1.DdrNgModelBase]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DdrNgModelBaseModule>;
}
