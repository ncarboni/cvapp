import { ControlValueAccessor } from '@angular/forms';
import { Subject } from 'rxjs';
import * as i0 from "@angular/core";
export declare class DdrNgModelBase implements ControlValueAccessor {
    private _innerValue;
    private _firstTime;
    private _firstValue;
    get value(): any;
    set value(value: any);
    get firstValue(): Subject<any>;
    set firstValue(value: Subject<any>);
    constructor();
    private onChange;
    writeValue(value: any): void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrNgModelBase, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DdrNgModelBase, "ng-component", never, {}, {}, never, never>;
}
