import * as i0 from "@angular/core";
import * as i1 from "./ddr-modal.component";
import * as i2 from "@angular/common";
import * as i3 from "../../directives/ddr-click-outside/ddr-click-outside.module";
export declare class DdrModalModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrModalModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DdrModalModule, [typeof i1.DdrModalComponent], [typeof i2.CommonModule, typeof i3.DdrClickOutsideModule], [typeof i1.DdrModalComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DdrModalModule>;
}
