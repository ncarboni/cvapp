import { DdrModalService } from './ddr-modal.service';
import { DdrConstantsService } from './../../services/ddr-constants.service';
import { EventEmitter, OnInit } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DdrModalComponent implements OnInit {
    constants: DdrConstantsService;
    private ddrModal;
    id: string;
    type: string;
    labelConfirm: string;
    labelClose: string;
    forceClose: boolean;
    close: EventEmitter<MouseEvent>;
    accept: EventEmitter<MouseEvent>;
    show: boolean;
    constructor(constants: DdrConstantsService, ddrModal: DdrModalService);
    ngOnInit(): void;
    onConfirm($event: MouseEvent): void;
    onClose($event: MouseEvent): void;
    onclickOutside($event: MouseEvent): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrModalComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DdrModalComponent, "ddr-modal", never, { "id": "id"; "type": "type"; "labelConfirm": "labelConfirm"; "labelClose": "labelClose"; "forceClose": "forceClose"; }, { "close": "close"; "accept": "accept"; }, never, ["[modal-title]", "[modal-content]"]>;
}
