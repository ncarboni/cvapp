import { DdrModalComponent } from './ddr-modal.component';
import * as i0 from "@angular/core";
export declare class DdrModalService {
    private modals;
    constructor();
    add(modal: DdrModalComponent): void;
    remove(id: string): void;
    open(id: string): void;
    close(id: string): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrModalService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<DdrModalService>;
}
