import { DdrNgModelBase } from './../ddr-ng-model-base/ddr-ng-model-base.component';
import { DdrSelectItem } from './../../common/ddr-select-item';
import { OnInit, EventEmitter, TemplateRef } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DdrDropdownComponent extends DdrNgModelBase implements OnInit {
    options: DdrSelectItem[];
    labelNoResults: string;
    selectItem: EventEmitter<DdrSelectItem>;
    template: TemplateRef<any>;
    showItems: boolean;
    optionsShow: DdrSelectItem[];
    valueShow: string;
    constructor();
    ngOnInit(): void;
    preload(v: any): void;
    showPanelOptions(): void;
    filter(searchWord: string): void;
    onSelectItem(item: DdrSelectItem): void;
    hidePanelItems(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrDropdownComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DdrDropdownComponent, "ddr-dropdown", never, { "options": "options"; "labelNoResults": "labelNoResults"; }, { "selectItem": "selectItem"; }, ["template"], never>;
}
