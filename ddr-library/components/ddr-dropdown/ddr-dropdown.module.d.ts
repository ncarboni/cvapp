import * as i0 from "@angular/core";
import * as i1 from "./ddr-dropdown.component";
import * as i2 from "@angular/common";
import * as i3 from "@angular/forms";
import * as i4 from "../../directives/ddr-click-outside/ddr-click-outside.module";
import * as i5 from "../ddr-ng-model-base/ddr-ng-model-base.module";
export declare class DdrDropdownModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrDropdownModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DdrDropdownModule, [typeof i1.DdrDropdownComponent], [typeof i2.CommonModule, typeof i3.FormsModule, typeof i4.DdrClickOutsideModule, typeof i5.DdrNgModelBaseModule], [typeof i1.DdrDropdownComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DdrDropdownModule>;
}
