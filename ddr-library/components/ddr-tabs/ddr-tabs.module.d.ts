import * as i0 from "@angular/core";
import * as i1 from "./ddr-tabs.component";
import * as i2 from "./ddr-tab-item/ddr-tab-item.component";
import * as i3 from "@angular/common";
export declare class DdrTabsModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrTabsModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DdrTabsModule, [typeof i1.DdrTabsComponent, typeof i2.DdrTabItemComponent], [typeof i3.CommonModule], [typeof i1.DdrTabsComponent, typeof i2.DdrTabItemComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DdrTabsModule>;
}
