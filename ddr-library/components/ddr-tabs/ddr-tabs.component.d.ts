import { DdrTabItemComponent } from './ddr-tab-item/ddr-tab-item.component';
import { QueryList, AfterViewInit, TemplateRef } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DdrTabsComponent implements AfterViewInit {
    tabsItems: QueryList<DdrTabItemComponent>;
    contentTemplate: TemplateRef<any>;
    constructor();
    ngAfterViewInit(): void;
    open(tab: DdrTabItemComponent): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrTabsComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DdrTabsComponent, "ddr-tabs", never, {}, {}, ["tabsItems"], never>;
}
