import { TemplateRef } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DdrTabItemComponent {
    title: string;
    contentTemplate: TemplateRef<any>;
    selected: boolean;
    constructor();
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrTabItemComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DdrTabItemComponent, "ddr-tab-item", never, { "title": "title"; }, {}, never, ["*"]>;
}
