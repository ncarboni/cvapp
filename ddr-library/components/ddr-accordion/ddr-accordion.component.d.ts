import { DdrConstantsService } from './../../services/ddr-constants.service';
import { EventEmitter, ElementRef, AfterViewInit } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DdrAccordionComponent implements AfterViewInit {
    constants: DdrConstantsService;
    title: string;
    open: boolean;
    clickOpen: EventEmitter<boolean>;
    contentAccordion: ElementRef;
    state: string;
    constructor(constants: DdrConstantsService);
    ngAfterViewInit(): void;
    openClose(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrAccordionComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DdrAccordionComponent, "ddr-accordion", never, { "title": "title"; "open": "open"; }, { "clickOpen": "clickOpen"; }, never, ["[content-accordion]"]>;
}
