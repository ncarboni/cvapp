import * as i0 from "@angular/core";
import * as i1 from "./ddr-accordion.component";
import * as i2 from "@angular/common";
export declare class DdrAccordionModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrAccordionModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DdrAccordionModule, [typeof i1.DdrAccordionComponent], [typeof i2.CommonModule], [typeof i1.DdrAccordionComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DdrAccordionModule>;
}
