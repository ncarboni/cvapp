import * as i0 from "@angular/core";
import * as i1 from "./ddr-toggle.component";
import * as i2 from "@angular/common";
import * as i3 from "../ddr-ng-model-base/ddr-ng-model-base.module";
export declare class DdrToggleModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrToggleModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DdrToggleModule, [typeof i1.DdrToggleComponent], [typeof i2.CommonModule, typeof i3.DdrNgModelBaseModule], [typeof i1.DdrToggleComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DdrToggleModule>;
}
