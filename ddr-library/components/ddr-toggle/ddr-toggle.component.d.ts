import { DdrNgModelBase } from './../ddr-ng-model-base/ddr-ng-model-base.component';
import { EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DdrToggleComponent extends DdrNgModelBase {
    label: string;
    toggled: EventEmitter<boolean>;
    constructor();
    onClick(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrToggleComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DdrToggleComponent, "ddr-toggle", never, { "label": "label"; }, { "toggled": "toggled"; }, never, never>;
}
