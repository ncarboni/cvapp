import * as i0 from "@angular/core";
import * as i1 from "./ddr-detail.component";
import * as i2 from "@angular/common";
import * as i3 from "../../directives/ddr-click-outside/ddr-click-outside.module";
export declare class DdrDetailModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrDetailModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DdrDetailModule, [typeof i1.DdrDetailComponent], [typeof i2.CommonModule, typeof i3.DdrClickOutsideModule], [typeof i1.DdrDetailComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DdrDetailModule>;
}
