import { OnInit, EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DdrDetailComponent implements OnInit {
    showOverlay: boolean;
    showTitle: boolean;
    clickOutsideEnabled: boolean;
    close: EventEmitter<boolean>;
    showDetail: boolean;
    constructor();
    ngOnInit(): void;
    closeDetail(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrDetailComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DdrDetailComponent, "ddr-detail", never, { "showOverlay": "showOverlay"; "showTitle": "showTitle"; "clickOutsideEnabled": "clickOutsideEnabled"; }, { "close": "close"; }, never, ["[detail-title]", "[detail-content]"]>;
}
