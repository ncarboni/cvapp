import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';
import * as i0 from "@angular/core";
export class DdrClickOutsideDirective {
    constructor(elementRef) {
        this.elementRef = elementRef;
        this.clickOutsideEnabled = true;
        this.avoidFirstTime = false;
        this.clickOutside = new EventEmitter();
        this.firstTime = true;
    }
    onDocumentClick(event) {
        if (this.clickOutsideEnabled) {
            const target = event.target;
            if (target && !this.elementRef.nativeElement.contains(target)) {
                if (!this.avoidFirstTime || (this.avoidFirstTime && !this.firstTime)) {
                    if (this.clickOutsideDelay) {
                        setTimeout(() => {
                            this.clickOutside.emit(event);
                        }, this.clickOutsideDelay);
                    }
                    else {
                        this.clickOutside.emit(event);
                    }
                }
                else {
                    this.firstTime = false;
                }
            }
        }
    }
}
DdrClickOutsideDirective.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrClickOutsideDirective, deps: [{ token: i0.ElementRef }], target: i0.ɵɵFactoryTarget.Directive });
DdrClickOutsideDirective.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "13.0.2", type: DdrClickOutsideDirective, selector: "[ddrClickOutside]", inputs: { clickOutsideEnabled: "clickOutsideEnabled", avoidFirstTime: "avoidFirstTime", clickOutsideDelay: "clickOutsideDelay" }, outputs: { clickOutside: "clickOutside" }, host: { listeners: { "document:click": "onDocumentClick($event)" } }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrClickOutsideDirective, decorators: [{
            type: Directive,
            args: [{
                    selector: '[ddrClickOutside]'
                }]
        }], ctorParameters: function () { return [{ type: i0.ElementRef }]; }, propDecorators: { clickOutsideEnabled: [{
                type: Input
            }], avoidFirstTime: [{
                type: Input
            }], clickOutsideDelay: [{
                type: Input
            }], clickOutside: [{
                type: Output
            }], onDocumentClick: [{
                type: HostListener,
                args: ['document:click', ['$event']]
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWNsaWNrLW91dHNpZGUuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL2RpcmVjdGl2ZXMvZGRyLWNsaWNrLW91dHNpZGUvZGRyLWNsaWNrLW91dHNpZGUuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQWMsWUFBWSxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQUtqRyxNQUFNLE9BQU8sd0JBQXdCO0lBVW5DLFlBQ1UsVUFBc0I7UUFBdEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQVR2Qix3QkFBbUIsR0FBWSxJQUFJLENBQUM7UUFDcEMsbUJBQWMsR0FBWSxLQUFLLENBQUM7UUFVdkMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLFlBQVksRUFBYyxDQUFDO1FBQ25ELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO0lBQ3hCLENBQUM7SUFHTSxlQUFlLENBQUMsS0FBaUI7UUFFdEMsSUFBRyxJQUFJLENBQUMsbUJBQW1CLEVBQUM7WUFFMUIsTUFBTSxNQUFNLEdBQUcsS0FBSyxDQUFDLE1BQXFCLENBQUM7WUFFM0MsSUFBRyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEVBQUM7Z0JBRTNELElBQUcsQ0FBQyxJQUFJLENBQUMsY0FBYyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBQztvQkFFbEUsSUFBRyxJQUFJLENBQUMsaUJBQWlCLEVBQUM7d0JBQ3hCLFVBQVUsQ0FBQyxHQUFHLEVBQUU7NEJBQ2QsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ2hDLENBQUMsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztxQkFDNUI7eUJBQUk7d0JBQ0gsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQy9CO2lCQUVGO3FCQUFJO29CQUNILElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO2lCQUN4QjthQUdGO1NBRUY7SUFFSCxDQUFDOztxSEE3Q1Usd0JBQXdCO3lHQUF4Qix3QkFBd0I7MkZBQXhCLHdCQUF3QjtrQkFIcEMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsbUJBQW1CO2lCQUM5QjtpR0FHVSxtQkFBbUI7c0JBQTNCLEtBQUs7Z0JBQ0csY0FBYztzQkFBdEIsS0FBSztnQkFDRyxpQkFBaUI7c0JBQXpCLEtBQUs7Z0JBRUksWUFBWTtzQkFBckIsTUFBTTtnQkFZQSxlQUFlO3NCQURyQixZQUFZO3VCQUFDLGdCQUFnQixFQUFFLENBQUMsUUFBUSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBFbGVtZW50UmVmLCBFdmVudEVtaXR0ZXIsIEhvc3RMaXN0ZW5lciwgSW5wdXQsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbZGRyQ2xpY2tPdXRzaWRlXSdcbn0pXG5leHBvcnQgY2xhc3MgRGRyQ2xpY2tPdXRzaWRlRGlyZWN0aXZlIHtcblxuICBASW5wdXQoKSBjbGlja091dHNpZGVFbmFibGVkOiBib29sZWFuID0gdHJ1ZTtcbiAgQElucHV0KCkgYXZvaWRGaXJzdFRpbWU6IGJvb2xlYW4gPSBmYWxzZTtcbiAgQElucHV0KCkgY2xpY2tPdXRzaWRlRGVsYXk6IG51bWJlcjtcblxuICBAT3V0cHV0KCkgY2xpY2tPdXRzaWRlOiBFdmVudEVtaXR0ZXI8TW91c2VFdmVudD47XG5cbiAgcHVibGljIGZpcnN0VGltZTogYm9vbGVhbjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGVsZW1lbnRSZWY6IEVsZW1lbnRSZWZcbiAgKSB7IFxuICAgIHRoaXMuY2xpY2tPdXRzaWRlID0gbmV3IEV2ZW50RW1pdHRlcjxNb3VzZUV2ZW50PigpO1xuICAgIHRoaXMuZmlyc3RUaW1lID0gdHJ1ZTtcbiAgfVxuXG4gIEBIb3N0TGlzdGVuZXIoJ2RvY3VtZW50OmNsaWNrJywgWyckZXZlbnQnXSlcbiAgcHVibGljIG9uRG9jdW1lbnRDbGljayhldmVudDogTW91c2VFdmVudCl7XG5cbiAgICBpZih0aGlzLmNsaWNrT3V0c2lkZUVuYWJsZWQpe1xuXG4gICAgICBjb25zdCB0YXJnZXQgPSBldmVudC50YXJnZXQgYXMgSFRNTEVsZW1lbnQ7XG5cbiAgICAgIGlmKHRhcmdldCAmJiAhdGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQuY29udGFpbnModGFyZ2V0KSl7XG5cbiAgICAgICAgaWYoIXRoaXMuYXZvaWRGaXJzdFRpbWUgfHwgKHRoaXMuYXZvaWRGaXJzdFRpbWUgJiYgIXRoaXMuZmlyc3RUaW1lKSl7XG5cbiAgICAgICAgICBpZih0aGlzLmNsaWNrT3V0c2lkZURlbGF5KXtcbiAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgICB0aGlzLmNsaWNrT3V0c2lkZS5lbWl0KGV2ZW50KTsgICAgICAgICAgICBcbiAgICAgICAgICAgIH0sIHRoaXMuY2xpY2tPdXRzaWRlRGVsYXkpO1xuICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgdGhpcy5jbGlja091dHNpZGUuZW1pdChldmVudCk7XG4gICAgICAgICAgfVxuXG4gICAgICAgIH1lbHNle1xuICAgICAgICAgIHRoaXMuZmlyc3RUaW1lID0gZmFsc2U7XG4gICAgICAgIH1cblxuXG4gICAgICB9XG5cbiAgICB9XG5cbiAgfVxuXG59XG4iXX0=