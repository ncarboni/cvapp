import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DdrClickOutsideDirective } from './ddr-click-outside.directive';
import * as i0 from "@angular/core";
export class DdrClickOutsideModule {
}
DdrClickOutsideModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrClickOutsideModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrClickOutsideModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrClickOutsideModule, declarations: [DdrClickOutsideDirective], imports: [CommonModule], exports: [DdrClickOutsideDirective] });
DdrClickOutsideModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrClickOutsideModule, imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrClickOutsideModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        DdrClickOutsideDirective
                    ],
                    exports: [
                        DdrClickOutsideDirective
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWNsaWNrLW91dHNpZGUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL2RpcmVjdGl2ZXMvZGRyLWNsaWNrLW91dHNpZGUvZGRyLWNsaWNrLW91dHNpZGUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLCtCQUErQixDQUFDOztBQWF6RSxNQUFNLE9BQU8scUJBQXFCOztrSEFBckIscUJBQXFCO21IQUFyQixxQkFBcUIsaUJBTjlCLHdCQUF3QixhQUh4QixZQUFZLGFBTVosd0JBQXdCO21IQUdmLHFCQUFxQixZQVZ2QjtZQUNQLFlBQVk7U0FDYjsyRkFRVSxxQkFBcUI7a0JBWGpDLFFBQVE7bUJBQUM7b0JBQ1IsT0FBTyxFQUFFO3dCQUNQLFlBQVk7cUJBQ2I7b0JBQ0QsWUFBWSxFQUFFO3dCQUNaLHdCQUF3QjtxQkFDekI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLHdCQUF3QjtxQkFDekI7aUJBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IERkckNsaWNrT3V0c2lkZURpcmVjdGl2ZSB9IGZyb20gJy4vZGRyLWNsaWNrLW91dHNpZGUuZGlyZWN0aXZlJztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZVxuICBdLFxuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBEZHJDbGlja091dHNpZGVEaXJlY3RpdmVcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIERkckNsaWNrT3V0c2lkZURpcmVjdGl2ZVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIERkckNsaWNrT3V0c2lkZU1vZHVsZSB7IH1cbiJdfQ==