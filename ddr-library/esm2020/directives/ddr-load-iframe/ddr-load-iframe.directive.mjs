import { Directive, Output, EventEmitter, HostListener } from '@angular/core';
import * as i0 from "@angular/core";
export class DdrLoadIframeDirective {
    constructor(el) {
        this.el = el;
        this.loadIframe = new EventEmitter();
    }
    onLoad() {
        if (!this.el.nativeElement.contentDocument ||
            this.el.nativeElement.contentDocument.body.children.length > 0) {
            this.loadIframe.emit(true);
        }
    }
}
DdrLoadIframeDirective.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLoadIframeDirective, deps: [{ token: i0.ElementRef }], target: i0.ɵɵFactoryTarget.Directive });
DdrLoadIframeDirective.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "13.0.2", type: DdrLoadIframeDirective, selector: "[ddrLoadIframe]", outputs: { loadIframe: "loadIframe" }, host: { listeners: { "load": "onLoad()" } }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLoadIframeDirective, decorators: [{
            type: Directive,
            args: [{
                    selector: '[ddrLoadIframe]'
                }]
        }], ctorParameters: function () { return [{ type: i0.ElementRef }]; }, propDecorators: { loadIframe: [{
                type: Output
            }], onLoad: [{
                type: HostListener,
                args: ['load']
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWxvYWQtaWZyYW1lLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2Rkci1saWJyYXJ5L3NyYy9kaXJlY3RpdmVzL2Rkci1sb2FkLWlmcmFtZS9kZHItbG9hZC1pZnJhbWUuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBYyxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBSzFGLE1BQU0sT0FBTyxzQkFBc0I7SUFJakMsWUFBb0IsRUFBYztRQUFkLE9BQUUsR0FBRixFQUFFLENBQVk7UUFDaEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLFlBQVksRUFBVyxDQUFDO0lBQ2hELENBQUM7SUFHTSxNQUFNO1FBRVgsSUFBRyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGVBQWU7WUFDckMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBQztZQUMvRCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM5QjtJQUVILENBQUM7O21IQWhCVSxzQkFBc0I7dUdBQXRCLHNCQUFzQjsyRkFBdEIsc0JBQXNCO2tCQUhsQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxpQkFBaUI7aUJBQzVCO2lHQUdXLFVBQVU7c0JBQW5CLE1BQU07Z0JBT0EsTUFBTTtzQkFEWixZQUFZO3VCQUFDLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBFbGVtZW50UmVmLCBIb3N0TGlzdGVuZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW2RkckxvYWRJZnJhbWVdJ1xufSlcbmV4cG9ydCBjbGFzcyBEZHJMb2FkSWZyYW1lRGlyZWN0aXZlIHtcblxuICBAT3V0cHV0KCkgbG9hZElmcmFtZTogRXZlbnRFbWl0dGVyPGJvb2xlYW4+O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZWw6IEVsZW1lbnRSZWYpIHsgXG4gICAgdGhpcy5sb2FkSWZyYW1lID0gbmV3IEV2ZW50RW1pdHRlcjxib29sZWFuPigpO1xuICB9XG5cbiAgQEhvc3RMaXN0ZW5lcignbG9hZCcpXG4gIHB1YmxpYyBvbkxvYWQoKXtcblxuICAgIGlmKCF0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuY29udGVudERvY3VtZW50IHx8IFxuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuY29udGVudERvY3VtZW50LmJvZHkuY2hpbGRyZW4ubGVuZ3RoID4gMCl7XG4gICAgICAgIHRoaXMubG9hZElmcmFtZS5lbWl0KHRydWUpO1xuICAgIH1cblxuICB9XG5cbn1cbiJdfQ==