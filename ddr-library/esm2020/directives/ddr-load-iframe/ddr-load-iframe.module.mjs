import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DdrLoadIframeDirective } from './ddr-load-iframe.directive';
import * as i0 from "@angular/core";
export class DdrLoadIframeModule {
}
DdrLoadIframeModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLoadIframeModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrLoadIframeModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLoadIframeModule, declarations: [DdrLoadIframeDirective], imports: [CommonModule], exports: [DdrLoadIframeDirective] });
DdrLoadIframeModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLoadIframeModule, imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLoadIframeModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        DdrLoadIframeDirective
                    ],
                    exports: [
                        DdrLoadIframeDirective
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWxvYWQtaWZyYW1lLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2Rkci1saWJyYXJ5L3NyYy9kaXJlY3RpdmVzL2Rkci1sb2FkLWlmcmFtZS9kZHItbG9hZC1pZnJhbWUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDZCQUE2QixDQUFDOztBQWFyRSxNQUFNLE9BQU8sbUJBQW1COztnSEFBbkIsbUJBQW1CO2lIQUFuQixtQkFBbUIsaUJBTjVCLHNCQUFzQixhQUh0QixZQUFZLGFBTVosc0JBQXNCO2lIQUdiLG1CQUFtQixZQVZyQjtZQUNQLFlBQVk7U0FDYjsyRkFRVSxtQkFBbUI7a0JBWC9CLFFBQVE7bUJBQUM7b0JBQ1IsT0FBTyxFQUFFO3dCQUNQLFlBQVk7cUJBQ2I7b0JBQ0QsWUFBWSxFQUFFO3dCQUNaLHNCQUFzQjtxQkFDdkI7b0JBQ0QsT0FBTyxFQUFDO3dCQUNOLHNCQUFzQjtxQkFDdkI7aUJBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IERkckxvYWRJZnJhbWVEaXJlY3RpdmUgfSBmcm9tICcuL2Rkci1sb2FkLWlmcmFtZS5kaXJlY3RpdmUnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1x0XG4gICAgRGRyTG9hZElmcmFtZURpcmVjdGl2ZVxuICBdLFxuICBleHBvcnRzOltcbiAgICBEZHJMb2FkSWZyYW1lRGlyZWN0aXZlXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgRGRyTG9hZElmcmFtZU1vZHVsZSB7IH1cbiJdfQ==