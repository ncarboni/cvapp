import { Injectable } from '@angular/core';
import { DdrToast } from './bean/ddr-toast';
import * as i0 from "@angular/core";
import * as i1 from "./../../services/ddr-constants.service";
export class DdrToastService {
    constructor(constans) {
        this.constans = constans;
        this._toasts = [];
        this._timeout = this.constans.DdrToastConstants.TIMEOUT;
    }
    get toasts() {
        return this._toasts;
    }
    get timeout() {
        return this._timeout;
    }
    set timeout(value) {
        this._timeout = value;
    }
    addInfoMessage(title, message) {
        this.addMessage(title, message, this.constans.DdrToastConstants.TYPE_INFO);
    }
    addWarningMessage(title, message) {
        this.addMessage(title, message, this.constans.DdrToastConstants.TYPE_WARNING);
    }
    addErrorMessage(title, message) {
        this.addMessage(title, message, this.constans.DdrToastConstants.TYPE_ERROR);
    }
    addSuccessMessage(title, message) {
        this.addMessage(title, message, this.constans.DdrToastConstants.TYPE_SUCCESS);
    }
    addMessage(title, message, type) {
        let toast = new DdrToast(title, message, type);
        this._toasts.push(toast);
        setTimeout(() => {
            this.closeToast(toast);
        }, this._timeout);
    }
    closeToast(toast) {
        let index = this._toasts.findIndex(t => t === toast);
        if (index !== -1) {
            this._toasts.splice(index, 1);
        }
    }
}
DdrToastService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastService, deps: [{ token: i1.DdrConstantsService }], target: i0.ɵɵFactoryTarget.Injectable });
DdrToastService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return [{ type: i1.DdrConstantsService }]; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLXRvYXN0LnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9kZHItbGlicmFyeS9zcmMvY29tcG9uZW50cy9kZHItdG9hc3QvZGRyLXRvYXN0LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7OztBQUs1QyxNQUFNLE9BQU8sZUFBZTtJQWlCMUIsWUFBb0IsUUFBNkI7UUFBN0IsYUFBUSxHQUFSLFFBQVEsQ0FBcUI7UUFDL0MsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7UUFDbEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQztJQUMxRCxDQUFDO0lBZkQsSUFBVyxNQUFNO1FBQ2YsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3RCLENBQUM7SUFFRCxJQUFXLE9BQU87UUFDaEIsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxJQUFXLE9BQU8sQ0FBQyxLQUFhO1FBQzlCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQ3hCLENBQUM7SUFPRCxjQUFjLENBQUMsS0FBYSxFQUFFLE9BQWU7UUFDM0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDN0UsQ0FBQztJQUVELGlCQUFpQixDQUFDLEtBQWEsRUFBRSxPQUFlO1FBQzlDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ2hGLENBQUM7SUFFRCxlQUFlLENBQUMsS0FBYSxFQUFFLE9BQWU7UUFDNUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDOUUsQ0FBQztJQUVELGlCQUFpQixDQUFDLEtBQWEsRUFBRSxPQUFlO1FBQzlDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ2hGLENBQUM7SUFFTyxVQUFVLENBQUMsS0FBYSxFQUFFLE9BQWUsRUFBRSxJQUFZO1FBQzdELElBQUksS0FBSyxHQUFhLElBQUksUUFBUSxDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDekQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekIsVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNkLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekIsQ0FBQyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNwQixDQUFDO0lBRUQsVUFBVSxDQUFDLEtBQWU7UUFDeEIsSUFBSSxLQUFLLEdBQVcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssS0FBSyxDQUFDLENBQUM7UUFDN0QsSUFBSSxLQUFLLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDaEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQy9CO0lBQ0gsQ0FBQzs7NEdBbkRVLGVBQWU7Z0hBQWYsZUFBZSxjQUZkLE1BQU07MkZBRVAsZUFBZTtrQkFIM0IsVUFBVTttQkFBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEZHJDb25zdGFudHNTZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi9zZXJ2aWNlcy9kZHItY29uc3RhbnRzLnNlcnZpY2UnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRGRyVG9hc3QgfSBmcm9tICcuL2JlYW4vZGRyLXRvYXN0JztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgRGRyVG9hc3RTZXJ2aWNlIHtcblxuICBwcml2YXRlIF90b2FzdHM6IERkclRvYXN0W107XG4gIHByaXZhdGUgX3RpbWVvdXQ6IG51bWJlcjtcblxuICBwdWJsaWMgZ2V0IHRvYXN0cygpOiBEZHJUb2FzdFtde1xuICAgIHJldHVybiB0aGlzLl90b2FzdHM7XG4gIH1cblxuICBwdWJsaWMgZ2V0IHRpbWVvdXQoKTogbnVtYmVye1xuICAgIHJldHVybiB0aGlzLl90aW1lb3V0O1xuICB9XG5cbiAgcHVibGljIHNldCB0aW1lb3V0KHZhbHVlOiBudW1iZXIpe1xuICAgIHRoaXMuX3RpbWVvdXQgPSB2YWx1ZTtcbiAgfVxuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgY29uc3RhbnM6IERkckNvbnN0YW50c1NlcnZpY2Upe1xuICAgIHRoaXMuX3RvYXN0cyA9IFtdO1xuICAgIHRoaXMuX3RpbWVvdXQgPSB0aGlzLmNvbnN0YW5zLkRkclRvYXN0Q29uc3RhbnRzLlRJTUVPVVQ7XG4gIH1cblxuICBhZGRJbmZvTWVzc2FnZSh0aXRsZTogc3RyaW5nLCBtZXNzYWdlOiBzdHJpbmcpe1xuICAgIHRoaXMuYWRkTWVzc2FnZSh0aXRsZSwgbWVzc2FnZSwgdGhpcy5jb25zdGFucy5EZHJUb2FzdENvbnN0YW50cy5UWVBFX0lORk8pO1xuICB9XG4gIFxuICBhZGRXYXJuaW5nTWVzc2FnZSh0aXRsZTogc3RyaW5nLCBtZXNzYWdlOiBzdHJpbmcpe1xuICAgIHRoaXMuYWRkTWVzc2FnZSh0aXRsZSwgbWVzc2FnZSwgdGhpcy5jb25zdGFucy5EZHJUb2FzdENvbnN0YW50cy5UWVBFX1dBUk5JTkcpO1xuICB9XG4gIFxuICBhZGRFcnJvck1lc3NhZ2UodGl0bGU6IHN0cmluZywgbWVzc2FnZTogc3RyaW5nKXtcbiAgICB0aGlzLmFkZE1lc3NhZ2UodGl0bGUsIG1lc3NhZ2UsIHRoaXMuY29uc3RhbnMuRGRyVG9hc3RDb25zdGFudHMuVFlQRV9FUlJPUik7XG4gIH1cbiAgXG4gIGFkZFN1Y2Nlc3NNZXNzYWdlKHRpdGxlOiBzdHJpbmcsIG1lc3NhZ2U6IHN0cmluZyl7XG4gICAgdGhpcy5hZGRNZXNzYWdlKHRpdGxlLCBtZXNzYWdlLCB0aGlzLmNvbnN0YW5zLkRkclRvYXN0Q29uc3RhbnRzLlRZUEVfU1VDQ0VTUyk7XG4gIH1cblxuICBwcml2YXRlIGFkZE1lc3NhZ2UodGl0bGU6IHN0cmluZywgbWVzc2FnZTogc3RyaW5nLCB0eXBlOiBzdHJpbmcpe1xuICAgIGxldCB0b2FzdDogRGRyVG9hc3QgPSBuZXcgRGRyVG9hc3QodGl0bGUsIG1lc3NhZ2UsIHR5cGUpO1xuICAgIHRoaXMuX3RvYXN0cy5wdXNoKHRvYXN0KTtcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIHRoaXMuY2xvc2VUb2FzdCh0b2FzdCk7XG4gICAgfSwgdGhpcy5fdGltZW91dCk7XG4gIH1cblxuICBjbG9zZVRvYXN0KHRvYXN0OiBEZHJUb2FzdCl7XG4gICAgbGV0IGluZGV4OiBudW1iZXIgPSB0aGlzLl90b2FzdHMuZmluZEluZGV4KHQgPT4gdCA9PT0gdG9hc3QpO1xuICAgIGlmIChpbmRleCAhPT0gLTEpIHtcbiAgICAgIHRoaXMuX3RvYXN0cy5zcGxpY2UoaW5kZXgsIDEpO1xuICAgIH1cbiAgfVxuXG5cbn1cbiJdfQ==