import { DdrToastService } from './ddr-toast.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DdrToastComponent } from './ddr-toast.component';
import * as i0 from "@angular/core";
export class DdrToastModule {
}
DdrToastModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrToastModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastModule, declarations: [DdrToastComponent], imports: [CommonModule], exports: [DdrToastComponent] });
DdrToastModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastModule, providers: [
        DdrToastService
    ], imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        DdrToastComponent
                    ],
                    exports: [
                        DdrToastComponent
                    ],
                    providers: [
                        DdrToastService
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLXRvYXN0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2Rkci1saWJyYXJ5L3NyYy9jb21wb25lbnRzL2Rkci10b2FzdC9kZHItdG9hc3QubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQzs7QUFnQjFELE1BQU0sT0FBTyxjQUFjOzsyR0FBZCxjQUFjOzRHQUFkLGNBQWMsaUJBVHZCLGlCQUFpQixhQUhqQixZQUFZLGFBTVosaUJBQWlCOzRHQU1SLGNBQWMsYUFKZDtRQUNULGVBQWU7S0FDaEIsWUFYUTtZQUNQLFlBQVk7U0FDYjsyRkFXVSxjQUFjO2tCQWQxQixRQUFRO21CQUFDO29CQUNSLE9BQU8sRUFBRTt3QkFDUCxZQUFZO3FCQUNiO29CQUNELFlBQVksRUFBRTt3QkFDWixpQkFBaUI7cUJBQ2xCO29CQUNELE9BQU8sRUFBRTt3QkFDUCxpQkFBaUI7cUJBQ2xCO29CQUNELFNBQVMsRUFBRTt3QkFDVCxlQUFlO3FCQUNoQjtpQkFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERkclRvYXN0U2VydmljZSB9IGZyb20gJy4vZGRyLXRvYXN0LnNlcnZpY2UnO1xuaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBEZHJUb2FzdENvbXBvbmVudCB9IGZyb20gJy4vZGRyLXRvYXN0LmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGVcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgRGRyVG9hc3RDb21wb25lbnRcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIERkclRvYXN0Q29tcG9uZW50XG4gIF0sXG4gIHByb3ZpZGVyczogW1xuICAgIERkclRvYXN0U2VydmljZVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIERkclRvYXN0TW9kdWxlIHsgfVxuIl19