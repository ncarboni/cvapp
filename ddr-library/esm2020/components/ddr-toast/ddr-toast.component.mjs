import { Component, Input, ViewEncapsulation } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import * as i0 from "@angular/core";
import * as i1 from "./ddr-toast.service";
import * as i2 from "@angular/common";
export class DdrToastComponent {
    constructor(toastService) {
        this.toastService = toastService;
    }
    ngOnInit() {
        if (this.timeout) {
            this.toastService.timeout = this.timeout;
        }
    }
    closeToast(toast) {
        this.toastService.closeToast(toast);
    }
}
DdrToastComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastComponent, deps: [{ token: i1.DdrToastService }], target: i0.ɵɵFactoryTarget.Component });
DdrToastComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrToastComponent, selector: "ddr-toast", inputs: { timeout: "timeout" }, ngImport: i0, template: "<div class=\"ddr-toast\">\n\n  <div [@overlayAnimation]=\"{value: 'visible'}\" class=\"ddr-toast__container\"\n    [ngClass]=\"'text-light bg-' + toast.type + ' border-' + toast.type\"\n    *ngFor=\"let toast of toastService.toasts\">\n\n    <div class=\"ddr-toast__container--title\">\n      <span class=\"ddr-toast__container--title--message\">{{toast.title}}</span>\n      <i class=\"fa fa-times ddr-toast__container--title--close-toast\" (click)=\"closeToast(toast)\"></i>\n    </div>\n\n    <div class=\"ddr-toast__container--content\">\n      <span>{{toast.message}}</span>\n    </div>\n\n  </div>\n\n</div>", styles: [".ddr-toast{position:absolute;top:10px;right:10px;z-index:99999}.ddr-toast__container{width:300px;padding:5px;margin-bottom:10px;border-radius:4px;border:1px solid #c8c8c8}.ddr-toast__container--content{padding:5px 0;font-size:14px}.ddr-toast__container--title{padding-bottom:5px;border-bottom:1px solid #FFF}.ddr-toast__container--title--message{width:270px;display:inline-block;font-size:18px}.ddr-toast__container--title--close-toast{cursor:pointer;position:absolute;font-size:20px}\n"], directives: [{ type: i2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }], animations: [
        trigger('overlayAnimation', [
            state('void', style({
                transform: 'translateY(5%)',
                opacity: 0
            })),
            state('visible', style({
                transform: 'translateY(0)',
                opacity: 1
            })),
            transition('void => visible', animate('225ms ease-out')),
            transition('visible => void', animate('195ms ease-in'))
        ])
    ], encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToastComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-toast', encapsulation: ViewEncapsulation.None, animations: [
                        trigger('overlayAnimation', [
                            state('void', style({
                                transform: 'translateY(5%)',
                                opacity: 0
                            })),
                            state('visible', style({
                                transform: 'translateY(0)',
                                opacity: 1
                            })),
                            transition('void => visible', animate('225ms ease-out')),
                            transition('visible => void', animate('195ms ease-in'))
                        ])
                    ], template: "<div class=\"ddr-toast\">\n\n  <div [@overlayAnimation]=\"{value: 'visible'}\" class=\"ddr-toast__container\"\n    [ngClass]=\"'text-light bg-' + toast.type + ' border-' + toast.type\"\n    *ngFor=\"let toast of toastService.toasts\">\n\n    <div class=\"ddr-toast__container--title\">\n      <span class=\"ddr-toast__container--title--message\">{{toast.title}}</span>\n      <i class=\"fa fa-times ddr-toast__container--title--close-toast\" (click)=\"closeToast(toast)\"></i>\n    </div>\n\n    <div class=\"ddr-toast__container--content\">\n      <span>{{toast.message}}</span>\n    </div>\n\n  </div>\n\n</div>", styles: [".ddr-toast{position:absolute;top:10px;right:10px;z-index:99999}.ddr-toast__container{width:300px;padding:5px;margin-bottom:10px;border-radius:4px;border:1px solid #c8c8c8}.ddr-toast__container--content{padding:5px 0;font-size:14px}.ddr-toast__container--title{padding-bottom:5px;border-bottom:1px solid #FFF}.ddr-toast__container--title--message{width:270px;display:inline-block;font-size:18px}.ddr-toast__container--title--close-toast{cursor:pointer;position:absolute;font-size:20px}\n"] }]
        }], ctorParameters: function () { return [{ type: i1.DdrToastService }]; }, propDecorators: { timeout: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLXRvYXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2Rkci1saWJyYXJ5L3NyYy9jb21wb25lbnRzL2Rkci10b2FzdC9kZHItdG9hc3QuY29tcG9uZW50LnRzIiwiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL2NvbXBvbmVudHMvZGRyLXRvYXN0L2Rkci10b2FzdC5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBVSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM1RSxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxNQUFNLHFCQUFxQixDQUFDOzs7O0FBc0JqRixNQUFNLE9BQU8saUJBQWlCO0lBSTVCLFlBQ1MsWUFBNkI7UUFBN0IsaUJBQVksR0FBWixZQUFZLENBQWlCO0lBQ2xDLENBQUM7SUFFTCxRQUFRO1FBRU4sSUFBRyxJQUFJLENBQUMsT0FBTyxFQUFDO1lBQ2QsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztTQUMxQztJQUVILENBQUM7SUFFRCxVQUFVLENBQUMsS0FBSztRQUNkLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3RDLENBQUM7OzhHQWxCVSxpQkFBaUI7a0dBQWpCLGlCQUFpQixpRkN4QjlCLHVtQkFpQk0scXNCRFJRO1FBQ1YsT0FBTyxDQUFDLGtCQUFrQixFQUFFO1lBQzFCLEtBQUssQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDO2dCQUNsQixTQUFTLEVBQUUsZ0JBQWdCO2dCQUMzQixPQUFPLEVBQUUsQ0FBQzthQUNYLENBQUMsQ0FBQztZQUNILEtBQUssQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDO2dCQUNyQixTQUFTLEVBQUUsZUFBZTtnQkFDMUIsT0FBTyxFQUFFLENBQUM7YUFDWCxDQUFDLENBQUM7WUFDSCxVQUFVLENBQUMsaUJBQWlCLEVBQUUsT0FBTyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDeEQsVUFBVSxDQUFDLGlCQUFpQixFQUFFLE9BQU8sQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUN4RCxDQUFDO0tBQ0g7MkZBRVUsaUJBQWlCO2tCQXBCN0IsU0FBUzsrQkFDRSxXQUFXLGlCQUdOLGlCQUFpQixDQUFDLElBQUksY0FDekI7d0JBQ1YsT0FBTyxDQUFDLGtCQUFrQixFQUFFOzRCQUMxQixLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQztnQ0FDbEIsU0FBUyxFQUFFLGdCQUFnQjtnQ0FDM0IsT0FBTyxFQUFFLENBQUM7NkJBQ1gsQ0FBQyxDQUFDOzRCQUNILEtBQUssQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDO2dDQUNyQixTQUFTLEVBQUUsZUFBZTtnQ0FDMUIsT0FBTyxFQUFFLENBQUM7NkJBQ1gsQ0FBQyxDQUFDOzRCQUNILFVBQVUsQ0FBQyxpQkFBaUIsRUFBRSxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzs0QkFDeEQsVUFBVSxDQUFDLGlCQUFpQixFQUFFLE9BQU8sQ0FBQyxlQUFlLENBQUMsQ0FBQzt5QkFDeEQsQ0FBQztxQkFDSDtzR0FJUSxPQUFPO3NCQUFmLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEZHJUb2FzdFNlcnZpY2UgfSBmcm9tICcuL2Rkci10b2FzdC5zZXJ2aWNlJztcbmltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IGFuaW1hdGUsIHN0YXRlLCBzdHlsZSwgdHJhbnNpdGlvbiwgdHJpZ2dlciB9IGZyb20gJ0Bhbmd1bGFyL2FuaW1hdGlvbnMnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkZHItdG9hc3QnLFxuICB0ZW1wbGF0ZVVybDogJy4vZGRyLXRvYXN0LmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vZGRyLXRvYXN0LmNvbXBvbmVudC5zY3NzJ10sXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXG4gIGFuaW1hdGlvbnM6IFtcbiAgICB0cmlnZ2VyKCdvdmVybGF5QW5pbWF0aW9uJywgW1xuICAgICAgc3RhdGUoJ3ZvaWQnLCBzdHlsZSh7XG4gICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVkoNSUpJyxcbiAgICAgICAgb3BhY2l0eTogMFxuICAgICAgfSkpLFxuICAgICAgc3RhdGUoJ3Zpc2libGUnLCBzdHlsZSh7XG4gICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVkoMCknLFxuICAgICAgICBvcGFjaXR5OiAxXG4gICAgICB9KSksXG4gICAgICB0cmFuc2l0aW9uKCd2b2lkID0+IHZpc2libGUnLCBhbmltYXRlKCcyMjVtcyBlYXNlLW91dCcpKSxcbiAgICAgIHRyYW5zaXRpb24oJ3Zpc2libGUgPT4gdm9pZCcsIGFuaW1hdGUoJzE5NW1zIGVhc2UtaW4nKSlcbiAgICBdKVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIERkclRvYXN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBASW5wdXQoKSB0aW1lb3V0OiBudW1iZXI7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHVibGljIHRvYXN0U2VydmljZTogRGRyVG9hc3RTZXJ2aWNlXG4gICkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG5cbiAgICBpZih0aGlzLnRpbWVvdXQpe1xuICAgICAgdGhpcy50b2FzdFNlcnZpY2UudGltZW91dCA9IHRoaXMudGltZW91dDtcbiAgICB9XG5cbiAgfVxuXG4gIGNsb3NlVG9hc3QodG9hc3Qpe1xuICAgIHRoaXMudG9hc3RTZXJ2aWNlLmNsb3NlVG9hc3QodG9hc3QpO1xuICB9XG5cbn1cbiIsIjxkaXYgY2xhc3M9XCJkZHItdG9hc3RcIj5cblxuICA8ZGl2IFtAb3ZlcmxheUFuaW1hdGlvbl09XCJ7dmFsdWU6ICd2aXNpYmxlJ31cIiBjbGFzcz1cImRkci10b2FzdF9fY29udGFpbmVyXCJcbiAgICBbbmdDbGFzc109XCIndGV4dC1saWdodCBiZy0nICsgdG9hc3QudHlwZSArICcgYm9yZGVyLScgKyB0b2FzdC50eXBlXCJcbiAgICAqbmdGb3I9XCJsZXQgdG9hc3Qgb2YgdG9hc3RTZXJ2aWNlLnRvYXN0c1wiPlxuXG4gICAgPGRpdiBjbGFzcz1cImRkci10b2FzdF9fY29udGFpbmVyLS10aXRsZVwiPlxuICAgICAgPHNwYW4gY2xhc3M9XCJkZHItdG9hc3RfX2NvbnRhaW5lci0tdGl0bGUtLW1lc3NhZ2VcIj57e3RvYXN0LnRpdGxlfX08L3NwYW4+XG4gICAgICA8aSBjbGFzcz1cImZhIGZhLXRpbWVzIGRkci10b2FzdF9fY29udGFpbmVyLS10aXRsZS0tY2xvc2UtdG9hc3RcIiAoY2xpY2spPVwiY2xvc2VUb2FzdCh0b2FzdClcIj48L2k+XG4gICAgPC9kaXY+XG5cbiAgICA8ZGl2IGNsYXNzPVwiZGRyLXRvYXN0X19jb250YWluZXItLWNvbnRlbnRcIj5cbiAgICAgIDxzcGFuPnt7dG9hc3QubWVzc2FnZX19PC9zcGFuPlxuICAgIDwvZGl2PlxuXG4gIDwvZGl2PlxuXG48L2Rpdj4iXX0=