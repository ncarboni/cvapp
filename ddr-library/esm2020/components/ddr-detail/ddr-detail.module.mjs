import { DdrClickOutsideModule } from './../../directives/ddr-click-outside/ddr-click-outside.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DdrDetailComponent } from './ddr-detail.component';
import * as i0 from "@angular/core";
export class DdrDetailModule {
}
DdrDetailModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDetailModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrDetailModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDetailModule, declarations: [DdrDetailComponent], imports: [CommonModule,
        DdrClickOutsideModule], exports: [DdrDetailComponent] });
DdrDetailModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDetailModule, imports: [[
            CommonModule,
            DdrClickOutsideModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDetailModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule,
                        DdrClickOutsideModule
                    ],
                    declarations: [
                        DdrDetailComponent
                    ],
                    exports: [
                        DdrDetailComponent
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWRldGFpbC5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9kZHItbGlicmFyeS9zcmMvY29tcG9uZW50cy9kZHItZGV0YWlsL2Rkci1kZXRhaWwubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLCtEQUErRCxDQUFDO0FBQ3RHLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDOztBQWM1RCxNQUFNLE9BQU8sZUFBZTs7NEdBQWYsZUFBZTs2R0FBZixlQUFlLGlCQU54QixrQkFBa0IsYUFKbEIsWUFBWTtRQUNaLHFCQUFxQixhQU1yQixrQkFBa0I7NkdBR1QsZUFBZSxZQVhqQjtZQUNQLFlBQVk7WUFDWixxQkFBcUI7U0FDdEI7MkZBUVUsZUFBZTtrQkFaM0IsUUFBUTttQkFBQztvQkFDUixPQUFPLEVBQUU7d0JBQ1AsWUFBWTt3QkFDWixxQkFBcUI7cUJBQ3RCO29CQUNELFlBQVksRUFBRTt3QkFDWixrQkFBa0I7cUJBQ25CO29CQUNELE9BQU8sRUFBRTt3QkFDUCxrQkFBa0I7cUJBQ25CO2lCQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGRyQ2xpY2tPdXRzaWRlTW9kdWxlIH0gZnJvbSAnLi8uLi8uLi9kaXJlY3RpdmVzL2Rkci1jbGljay1vdXRzaWRlL2Rkci1jbGljay1vdXRzaWRlLm1vZHVsZSc7XG5pbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IERkckRldGFpbENvbXBvbmVudCB9IGZyb20gJy4vZGRyLWRldGFpbC5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIERkckNsaWNrT3V0c2lkZU1vZHVsZVxuICBdLFxuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBEZHJEZXRhaWxDb21wb25lbnRcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIERkckRldGFpbENvbXBvbmVudFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIERkckRldGFpbE1vZHVsZSB7IH1cbiJdfQ==