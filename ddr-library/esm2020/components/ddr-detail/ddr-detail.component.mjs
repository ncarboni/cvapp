import { animate, style, transition, trigger } from '@angular/animations';
import { Component, Output, ViewEncapsulation, EventEmitter, Input } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "../../directives/ddr-click-outside/ddr-click-outside.directive";
export class DdrDetailComponent {
    constructor() {
        this.showOverlay = true;
        this.showTitle = true;
        this.clickOutsideEnabled = true;
        this.close = new EventEmitter();
        this.showDetail = true;
    }
    ngOnInit() {
    }
    closeDetail() {
        this.showDetail = false;
        setTimeout(() => {
            this.close.emit(true);
        }, 600);
    }
}
DdrDetailComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDetailComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
DdrDetailComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrDetailComponent, selector: "ddr-detail", inputs: { showOverlay: "showOverlay", showTitle: "showTitle", clickOutsideEnabled: "clickOutsideEnabled" }, outputs: { close: "close" }, ngImport: i0, template: "<div class=\"ddr-detail\" *ngIf=\"showDetail\" \n  [@slide] \n  ddrClickOutside \n  [avoidFirstTime]=\"true\"\n  [clickOutsideEnabled]=\"clickOutsideEnabled\" \n  (clickOutside)=\"closeDetail()\">\n\n  <div class=\"row ddr-detail__title\" *ngIf=\"showTitle\">\n    <div class=\"col-lg-10 col-md-9 col-10\">\n      <ng-content select=\"[detail-title]\"></ng-content>\n    </div>\n    <div class=\"col-lg-2 col-md-3 col-2 p-3 text-right\">\n      <i class=\"fa fa-times ddr-detail__close\" (click)=\"closeDetail()\"></i>\n    </div>\n  </div>\n\n  <div class=\"row ddr-detail__content\">\n    <div class=\"col-12\">\n      <ng-content select=\"[detail-content]\"></ng-content>\n    </div>\n  </div>\n\n</div>\n\n<div class=\"ddr-overlay\" *ngIf=\"showDetail && showOverlay\"></div>", styles: [".ddr-detail{position:fixed;top:0;right:0;padding:15px;z-index:9999;height:100%}.ddr-detail__close{font-size:26px;cursor:pointer;margin-right:5px}@media (min-width: 992px){.ddr-detail{width:40%}}@media (max-width: 992px){.ddr-detail{width:100%}}\n"], directives: [{ type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i2.DdrClickOutsideDirective, selector: "[ddrClickOutside]", inputs: ["clickOutsideEnabled", "avoidFirstTime", "clickOutsideDelay"], outputs: ["clickOutside"] }], animations: [
        trigger('slide', [
            transition(':enter', [
                style({ transform: 'translateX(100%)' }),
                animate('600ms ease-in', style({ transform: 'translateX(0%)' }))
            ]),
            transition(':leave', [
                animate('600ms ease-in', style({ transform: 'translateX(100%)' }))
            ])
        ])
    ], encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDetailComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-detail', encapsulation: ViewEncapsulation.None, animations: [
                        trigger('slide', [
                            transition(':enter', [
                                style({ transform: 'translateX(100%)' }),
                                animate('600ms ease-in', style({ transform: 'translateX(0%)' }))
                            ]),
                            transition(':leave', [
                                animate('600ms ease-in', style({ transform: 'translateX(100%)' }))
                            ])
                        ])
                    ], template: "<div class=\"ddr-detail\" *ngIf=\"showDetail\" \n  [@slide] \n  ddrClickOutside \n  [avoidFirstTime]=\"true\"\n  [clickOutsideEnabled]=\"clickOutsideEnabled\" \n  (clickOutside)=\"closeDetail()\">\n\n  <div class=\"row ddr-detail__title\" *ngIf=\"showTitle\">\n    <div class=\"col-lg-10 col-md-9 col-10\">\n      <ng-content select=\"[detail-title]\"></ng-content>\n    </div>\n    <div class=\"col-lg-2 col-md-3 col-2 p-3 text-right\">\n      <i class=\"fa fa-times ddr-detail__close\" (click)=\"closeDetail()\"></i>\n    </div>\n  </div>\n\n  <div class=\"row ddr-detail__content\">\n    <div class=\"col-12\">\n      <ng-content select=\"[detail-content]\"></ng-content>\n    </div>\n  </div>\n\n</div>\n\n<div class=\"ddr-overlay\" *ngIf=\"showDetail && showOverlay\"></div>", styles: [".ddr-detail{position:fixed;top:0;right:0;padding:15px;z-index:9999;height:100%}.ddr-detail__close{font-size:26px;cursor:pointer;margin-right:5px}@media (min-width: 992px){.ddr-detail{width:40%}}@media (max-width: 992px){.ddr-detail{width:100%}}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { showOverlay: [{
                type: Input
            }], showTitle: [{
                type: Input
            }], clickOutsideEnabled: [{
                type: Input
            }], close: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWRldGFpbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9kZHItbGlicmFyeS9zcmMvY29tcG9uZW50cy9kZHItZGV0YWlsL2Rkci1kZXRhaWwuY29tcG9uZW50LnRzIiwiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL2NvbXBvbmVudHMvZGRyLWRldGFpbC9kZHItZGV0YWlsLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUMxRSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxpQkFBaUIsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7O0FBbUJsRyxNQUFNLE9BQU8sa0JBQWtCO0lBUzdCO1FBUFMsZ0JBQVcsR0FBWSxJQUFJLENBQUM7UUFDNUIsY0FBUyxHQUFZLElBQUksQ0FBQztRQUMxQix3QkFBbUIsR0FBWSxJQUFJLENBQUM7UUFNM0MsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLFlBQVksRUFBVyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO0lBQ3pCLENBQUM7SUFFRCxRQUFRO0lBQ1IsQ0FBQztJQUVELFdBQVc7UUFDVCxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUN4QixVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ2QsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ1YsQ0FBQzs7K0dBdEJVLGtCQUFrQjttR0FBbEIsa0JBQWtCLDJMQ3BCL0IsNndCQXdCaUUsMGhCRGhCbkQ7UUFDVixPQUFPLENBQUMsT0FBTyxFQUFFO1lBQ2YsVUFBVSxDQUFDLFFBQVEsRUFBRTtnQkFDbkIsS0FBSyxDQUFDLEVBQUUsU0FBUyxFQUFFLGtCQUFrQixFQUFDLENBQUM7Z0JBQ3ZDLE9BQU8sQ0FBQyxlQUFlLEVBQUUsS0FBSyxDQUFDLEVBQUUsU0FBUyxFQUFFLGdCQUFnQixFQUFDLENBQUMsQ0FBQzthQUNoRSxDQUFDO1lBQ0YsVUFBVSxDQUFDLFFBQVEsRUFBRTtnQkFDbkIsT0FBTyxDQUFDLGVBQWUsRUFBRSxLQUFLLENBQUMsRUFBRSxTQUFTLEVBQUUsa0JBQWtCLEVBQUMsQ0FBQyxDQUFDO2FBQ2xFLENBQUM7U0FDSCxDQUFDO0tBQ0g7MkZBRVUsa0JBQWtCO2tCQWpCOUIsU0FBUzsrQkFDRSxZQUFZLGlCQUdQLGlCQUFpQixDQUFDLElBQUksY0FDekI7d0JBQ1YsT0FBTyxDQUFDLE9BQU8sRUFBRTs0QkFDZixVQUFVLENBQUMsUUFBUSxFQUFFO2dDQUNuQixLQUFLLENBQUMsRUFBRSxTQUFTLEVBQUUsa0JBQWtCLEVBQUMsQ0FBQztnQ0FDdkMsT0FBTyxDQUFDLGVBQWUsRUFBRSxLQUFLLENBQUMsRUFBRSxTQUFTLEVBQUUsZ0JBQWdCLEVBQUMsQ0FBQyxDQUFDOzZCQUNoRSxDQUFDOzRCQUNGLFVBQVUsQ0FBQyxRQUFRLEVBQUU7Z0NBQ25CLE9BQU8sQ0FBQyxlQUFlLEVBQUUsS0FBSyxDQUFDLEVBQUUsU0FBUyxFQUFFLGtCQUFrQixFQUFDLENBQUMsQ0FBQzs2QkFDbEUsQ0FBQzt5QkFDSCxDQUFDO3FCQUNIOzBFQUlRLFdBQVc7c0JBQW5CLEtBQUs7Z0JBQ0csU0FBUztzQkFBakIsS0FBSztnQkFDRyxtQkFBbUI7c0JBQTNCLEtBQUs7Z0JBQ0ksS0FBSztzQkFBZCxNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgYW5pbWF0ZSwgc3R5bGUsIHRyYW5zaXRpb24sIHRyaWdnZXIgfSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPdXRwdXQsIFZpZXdFbmNhcHN1bGF0aW9uLCBFdmVudEVtaXR0ZXIsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2Rkci1kZXRhaWwnLFxuICB0ZW1wbGF0ZVVybDogJy4vZGRyLWRldGFpbC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2Rkci1kZXRhaWwuY29tcG9uZW50LnNjc3MnXSxcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcbiAgYW5pbWF0aW9uczogW1xuICAgIHRyaWdnZXIoJ3NsaWRlJywgW1xuICAgICAgdHJhbnNpdGlvbignOmVudGVyJywgW1xuICAgICAgICBzdHlsZSh7IHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoMTAwJSknfSksXG4gICAgICAgIGFuaW1hdGUoJzYwMG1zIGVhc2UtaW4nLCBzdHlsZSh7IHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoMCUpJ30pKVxuICAgICAgXSksXG4gICAgICB0cmFuc2l0aW9uKCc6bGVhdmUnLCBbXG4gICAgICAgIGFuaW1hdGUoJzYwMG1zIGVhc2UtaW4nLCBzdHlsZSh7IHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoMTAwJSknfSkpXG4gICAgICBdKVxuICAgIF0pXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgRGRyRGV0YWlsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBASW5wdXQoKSBzaG93T3ZlcmxheTogYm9vbGVhbiA9IHRydWU7XG4gIEBJbnB1dCgpIHNob3dUaXRsZTogYm9vbGVhbiA9IHRydWU7XG4gIEBJbnB1dCgpIGNsaWNrT3V0c2lkZUVuYWJsZWQ6IGJvb2xlYW4gPSB0cnVlO1xuICBAT3V0cHV0KCkgY2xvc2U6IEV2ZW50RW1pdHRlcjxib29sZWFuPjtcblxuICBwdWJsaWMgc2hvd0RldGFpbDogYm9vbGVhbjtcblxuICBjb25zdHJ1Y3RvcigpIHsgXG4gICAgdGhpcy5jbG9zZSA9IG5ldyBFdmVudEVtaXR0ZXI8Ym9vbGVhbj4oKTtcbiAgICB0aGlzLnNob3dEZXRhaWwgPSB0cnVlO1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBjbG9zZURldGFpbCgpe1xuICAgIHRoaXMuc2hvd0RldGFpbCA9IGZhbHNlO1xuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgdGhpcy5jbG9zZS5lbWl0KHRydWUpO1xuICAgIH0sIDYwMCk7XG4gIH1cblxufVxuIiwiPGRpdiBjbGFzcz1cImRkci1kZXRhaWxcIiAqbmdJZj1cInNob3dEZXRhaWxcIiBcbiAgW0BzbGlkZV0gXG4gIGRkckNsaWNrT3V0c2lkZSBcbiAgW2F2b2lkRmlyc3RUaW1lXT1cInRydWVcIlxuICBbY2xpY2tPdXRzaWRlRW5hYmxlZF09XCJjbGlja091dHNpZGVFbmFibGVkXCIgXG4gIChjbGlja091dHNpZGUpPVwiY2xvc2VEZXRhaWwoKVwiPlxuXG4gIDxkaXYgY2xhc3M9XCJyb3cgZGRyLWRldGFpbF9fdGl0bGVcIiAqbmdJZj1cInNob3dUaXRsZVwiPlxuICAgIDxkaXYgY2xhc3M9XCJjb2wtbGctMTAgY29sLW1kLTkgY29sLTEwXCI+XG4gICAgICA8bmctY29udGVudCBzZWxlY3Q9XCJbZGV0YWlsLXRpdGxlXVwiPjwvbmctY29udGVudD5cbiAgICA8L2Rpdj5cbiAgICA8ZGl2IGNsYXNzPVwiY29sLWxnLTIgY29sLW1kLTMgY29sLTIgcC0zIHRleHQtcmlnaHRcIj5cbiAgICAgIDxpIGNsYXNzPVwiZmEgZmEtdGltZXMgZGRyLWRldGFpbF9fY2xvc2VcIiAoY2xpY2spPVwiY2xvc2VEZXRhaWwoKVwiPjwvaT5cbiAgICA8L2Rpdj5cbiAgPC9kaXY+XG5cbiAgPGRpdiBjbGFzcz1cInJvdyBkZHItZGV0YWlsX19jb250ZW50XCI+XG4gICAgPGRpdiBjbGFzcz1cImNvbC0xMlwiPlxuICAgICAgPG5nLWNvbnRlbnQgc2VsZWN0PVwiW2RldGFpbC1jb250ZW50XVwiPjwvbmctY29udGVudD5cbiAgICA8L2Rpdj5cbiAgPC9kaXY+XG5cbjwvZGl2PlxuXG48ZGl2IGNsYXNzPVwiZGRyLW92ZXJsYXlcIiAqbmdJZj1cInNob3dEZXRhaWwgJiYgc2hvd092ZXJsYXlcIj48L2Rpdj4iXX0=