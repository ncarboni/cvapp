import { DdrNgModelBase } from './../ddr-ng-model-base/ddr-ng-model-base.component';
import { Component, EventEmitter, ViewEncapsulation, Input, Output, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
export class DdrToggleComponent extends DdrNgModelBase {
    constructor() {
        super();
        this.toggled = new EventEmitter();
    }
    onClick() {
        this.value = !this.value;
        this.toggled.emit(this.value);
    }
}
DdrToggleComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToggleComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
DdrToggleComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrToggleComponent, selector: "ddr-toggle", inputs: { label: "label" }, outputs: { toggled: "toggled" }, providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DdrToggleComponent),
            multi: true
        }
    ], usesInheritance: true, ngImport: i0, template: "<div class=\"ddr-toggle\">\n  <div class=\"ddr-toggle__label\" *ngIf=\"label\">\n    <label>{{label}}</label>\n  </div>\n  <button class=\"ddr-toggle__btn\"\n    [ngClass]=\"{'ddr-toggle__btn--on': value, 'ddr-toggle__btn--off': !value}\"\n    (click)=\"onClick()\">\n\n  </button>\n</div> ", styles: [".ddr-toggle{margin-top:5px;margin-bottom:5px;display:inline-block}.ddr-toggle__label{text-transform:uppercase}.ddr-toggle__btn{display:inline-block;width:60px;height:30px;position:relative;cursor:pointer;border-radius:20px;padding:2px;border:none}.ddr-toggle__btn:after{content:\"\";display:block;position:relative;width:50%;height:100%;border-radius:20px;transition:all .3s cubic-bezier(.175,.885,.32,1.275),padding .3s ease,margin .3s ease}.ddr-toggle__btn--on{background:#28a745}.ddr-toggle__btn--on:after{left:50%}.ddr-toggle__btn--off{background:#afafafaf}.ddr-toggle__btn--off:after{left:0}\n"], directives: [{ type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }], encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToggleComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-toggle', encapsulation: ViewEncapsulation.None, providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef(() => DdrToggleComponent),
                            multi: true
                        }
                    ], template: "<div class=\"ddr-toggle\">\n  <div class=\"ddr-toggle__label\" *ngIf=\"label\">\n    <label>{{label}}</label>\n  </div>\n  <button class=\"ddr-toggle__btn\"\n    [ngClass]=\"{'ddr-toggle__btn--on': value, 'ddr-toggle__btn--off': !value}\"\n    (click)=\"onClick()\">\n\n  </button>\n</div> ", styles: [".ddr-toggle{margin-top:5px;margin-bottom:5px;display:inline-block}.ddr-toggle__label{text-transform:uppercase}.ddr-toggle__btn{display:inline-block;width:60px;height:30px;position:relative;cursor:pointer;border-radius:20px;padding:2px;border:none}.ddr-toggle__btn:after{content:\"\";display:block;position:relative;width:50%;height:100%;border-radius:20px;transition:all .3s cubic-bezier(.175,.885,.32,1.275),padding .3s ease,margin .3s ease}.ddr-toggle__btn--on{background:#28a745}.ddr-toggle__btn--on:after{left:50%}.ddr-toggle__btn--off{background:#afafafaf}.ddr-toggle__btn--off:after{left:0}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { label: [{
                type: Input
            }], toggled: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLXRvZ2dsZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9kZHItbGlicmFyeS9zcmMvY29tcG9uZW50cy9kZHItdG9nZ2xlL2Rkci10b2dnbGUuY29tcG9uZW50LnRzIiwiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL2NvbXBvbmVudHMvZGRyLXRvZ2dsZS9kZHItdG9nZ2xlLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvREFBb0QsQ0FBQztBQUNwRixPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBVSxpQkFBaUIsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5RyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7O0FBZW5ELE1BQU0sT0FBTyxrQkFBbUIsU0FBUSxjQUFjO0lBS3BEO1FBQ0UsS0FBSyxFQUFFLENBQUM7UUFDUixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksWUFBWSxFQUFXLENBQUM7SUFDN0MsQ0FBQztJQUVELE9BQU87UUFDTCxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDaEMsQ0FBQzs7K0dBYlUsa0JBQWtCO21HQUFsQixrQkFBa0Isa0dBUmxCO1FBQ1Q7WUFDRSxPQUFPLEVBQUUsaUJBQWlCO1lBQzFCLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsa0JBQWtCLENBQUM7WUFDakQsS0FBSyxFQUFFLElBQUk7U0FDWjtLQUNGLGlEQ2ZILG9TQVNPOzJGRFFNLGtCQUFrQjtrQkFiOUIsU0FBUzsrQkFDRSxZQUFZLGlCQUdQLGlCQUFpQixDQUFDLElBQUksYUFDMUI7d0JBQ1Q7NEJBQ0UsT0FBTyxFQUFFLGlCQUFpQjs0QkFDMUIsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsbUJBQW1CLENBQUM7NEJBQ2pELEtBQUssRUFBRSxJQUFJO3lCQUNaO3FCQUNGOzBFQUlRLEtBQUs7c0JBQWIsS0FBSztnQkFDSSxPQUFPO3NCQUFoQixNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGRyTmdNb2RlbEJhc2UgfSBmcm9tICcuLy4uL2Rkci1uZy1tb2RlbC1iYXNlL2Rkci1uZy1tb2RlbC1iYXNlLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiwgSW5wdXQsIE91dHB1dCwgZm9yd2FyZFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTkdfVkFMVUVfQUNDRVNTT1IgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2Rkci10b2dnbGUnLFxuICB0ZW1wbGF0ZVVybDogJy4vZGRyLXRvZ2dsZS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2Rkci10b2dnbGUuY29tcG9uZW50LnNjc3MnXSxcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcbiAgcHJvdmlkZXJzOiBbXG4gICAge1xuICAgICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBEZHJUb2dnbGVDb21wb25lbnQpLFxuICAgICAgbXVsdGk6IHRydWVcbiAgICB9XG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgRGRyVG9nZ2xlQ29tcG9uZW50IGV4dGVuZHMgRGRyTmdNb2RlbEJhc2UgIHtcblxuICBASW5wdXQoKSBsYWJlbDogc3RyaW5nO1xuICBAT3V0cHV0KCkgdG9nZ2xlZDogRXZlbnRFbWl0dGVyPGJvb2xlYW4+O1xuXG4gIGNvbnN0cnVjdG9yKCkgeyBcbiAgICBzdXBlcigpO1xuICAgIHRoaXMudG9nZ2xlZCA9IG5ldyBFdmVudEVtaXR0ZXI8Ym9vbGVhbj4oKTtcbiAgfVxuXG4gIG9uQ2xpY2soKXtcbiAgICB0aGlzLnZhbHVlID0gIXRoaXMudmFsdWU7XG4gICAgdGhpcy50b2dnbGVkLmVtaXQodGhpcy52YWx1ZSk7XG4gIH1cblxufVxuIiwiPGRpdiBjbGFzcz1cImRkci10b2dnbGVcIj5cbiAgPGRpdiBjbGFzcz1cImRkci10b2dnbGVfX2xhYmVsXCIgKm5nSWY9XCJsYWJlbFwiPlxuICAgIDxsYWJlbD57e2xhYmVsfX08L2xhYmVsPlxuICA8L2Rpdj5cbiAgPGJ1dHRvbiBjbGFzcz1cImRkci10b2dnbGVfX2J0blwiXG4gICAgW25nQ2xhc3NdPVwieydkZHItdG9nZ2xlX19idG4tLW9uJzogdmFsdWUsICdkZHItdG9nZ2xlX19idG4tLW9mZic6ICF2YWx1ZX1cIlxuICAgIChjbGljayk9XCJvbkNsaWNrKClcIj5cblxuICA8L2J1dHRvbj5cbjwvZGl2PiAiXX0=