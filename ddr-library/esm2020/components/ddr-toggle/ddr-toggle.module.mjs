import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DdrToggleComponent } from './ddr-toggle.component';
import { DdrNgModelBaseModule } from '../ddr-ng-model-base/ddr-ng-model-base.module';
import * as i0 from "@angular/core";
export class DdrToggleModule {
}
DdrToggleModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToggleModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrToggleModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToggleModule, declarations: [DdrToggleComponent], imports: [CommonModule,
        DdrNgModelBaseModule], exports: [DdrToggleComponent] });
DdrToggleModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToggleModule, imports: [[
            CommonModule,
            DdrNgModelBaseModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrToggleModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule,
                        DdrNgModelBaseModule
                    ],
                    declarations: [
                        DdrToggleComponent
                    ],
                    exports: [
                        DdrToggleComponent
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLXRvZ2dsZS5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9kZHItbGlicmFyeS9zcmMvY29tcG9uZW50cy9kZHItdG9nZ2xlL2Rkci10b2dnbGUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzVELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLCtDQUErQyxDQUFDOztBQWNyRixNQUFNLE9BQU8sZUFBZTs7NEdBQWYsZUFBZTs2R0FBZixlQUFlLGlCQU54QixrQkFBa0IsYUFKbEIsWUFBWTtRQUNaLG9CQUFvQixhQU1wQixrQkFBa0I7NkdBR1QsZUFBZSxZQVhqQjtZQUNQLFlBQVk7WUFDWixvQkFBb0I7U0FDckI7MkZBUVUsZUFBZTtrQkFaM0IsUUFBUTttQkFBQztvQkFDUixPQUFPLEVBQUU7d0JBQ1AsWUFBWTt3QkFDWixvQkFBb0I7cUJBQ3JCO29CQUNELFlBQVksRUFBRTt3QkFDWixrQkFBa0I7cUJBQ25CO29CQUNELE9BQU8sRUFBRTt3QkFDUCxrQkFBa0I7cUJBQ25CO2lCQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBEZHJUb2dnbGVDb21wb25lbnQgfSBmcm9tICcuL2Rkci10b2dnbGUuY29tcG9uZW50JztcbmltcG9ydCB7IERkck5nTW9kZWxCYXNlTW9kdWxlIH0gZnJvbSAnLi4vZGRyLW5nLW1vZGVsLWJhc2UvZGRyLW5nLW1vZGVsLWJhc2UubW9kdWxlJztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBEZHJOZ01vZGVsQmFzZU1vZHVsZVxuICBdLFxuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBEZHJUb2dnbGVDb21wb25lbnRcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIERkclRvZ2dsZUNvbXBvbmVudFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIERkclRvZ2dsZU1vZHVsZSB7IH1cbiJdfQ==