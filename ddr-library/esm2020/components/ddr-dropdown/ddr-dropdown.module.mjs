import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DdrDropdownComponent } from './ddr-dropdown.component';
import { FormsModule } from '@angular/forms';
import { DdrClickOutsideModule } from '../../directives/ddr-click-outside/ddr-click-outside.module';
import { DdrNgModelBaseModule } from '../ddr-ng-model-base/ddr-ng-model-base.module';
import * as i0 from "@angular/core";
export class DdrDropdownModule {
}
DdrDropdownModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDropdownModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrDropdownModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDropdownModule, declarations: [DdrDropdownComponent], imports: [CommonModule,
        FormsModule,
        DdrClickOutsideModule,
        DdrNgModelBaseModule], exports: [DdrDropdownComponent] });
DdrDropdownModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDropdownModule, imports: [[
            CommonModule,
            FormsModule,
            DdrClickOutsideModule,
            DdrNgModelBaseModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrDropdownModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule,
                        FormsModule,
                        DdrClickOutsideModule,
                        DdrNgModelBaseModule
                    ],
                    declarations: [
                        DdrDropdownComponent
                    ],
                    exports: [
                        DdrDropdownComponent
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWRyb3Bkb3duLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2Rkci1saWJyYXJ5L3NyYy9jb21wb25lbnRzL2Rkci1kcm9wZG93bi9kZHItZHJvcGRvd24ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ2hFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSw2REFBNkQsQ0FBQztBQUNwRyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQzs7QUFnQnJGLE1BQU0sT0FBTyxpQkFBaUI7OzhHQUFqQixpQkFBaUI7K0dBQWpCLGlCQUFpQixpQkFOMUIsb0JBQW9CLGFBTnBCLFlBQVk7UUFDWixXQUFXO1FBQ1gscUJBQXFCO1FBQ3JCLG9CQUFvQixhQU1wQixvQkFBb0I7K0dBR1gsaUJBQWlCLFlBYm5CO1lBQ1AsWUFBWTtZQUNaLFdBQVc7WUFDWCxxQkFBcUI7WUFDckIsb0JBQW9CO1NBQ3JCOzJGQVFVLGlCQUFpQjtrQkFkN0IsUUFBUTttQkFBQztvQkFDUixPQUFPLEVBQUU7d0JBQ1AsWUFBWTt3QkFDWixXQUFXO3dCQUNYLHFCQUFxQjt3QkFDckIsb0JBQW9CO3FCQUNyQjtvQkFDRCxZQUFZLEVBQUU7d0JBQ1osb0JBQW9CO3FCQUNyQjtvQkFDRCxPQUFPLEVBQUU7d0JBQ1Asb0JBQW9CO3FCQUNyQjtpQkFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgRGRyRHJvcGRvd25Db21wb25lbnQgfSBmcm9tICcuL2Rkci1kcm9wZG93bi5jb21wb25lbnQnO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBEZHJDbGlja091dHNpZGVNb2R1bGUgfSBmcm9tICcuLi8uLi9kaXJlY3RpdmVzL2Rkci1jbGljay1vdXRzaWRlL2Rkci1jbGljay1vdXRzaWRlLm1vZHVsZSc7XG5pbXBvcnQgeyBEZHJOZ01vZGVsQmFzZU1vZHVsZSB9IGZyb20gJy4uL2Rkci1uZy1tb2RlbC1iYXNlL2Rkci1uZy1tb2RlbC1iYXNlLm1vZHVsZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGUsXG4gICAgRm9ybXNNb2R1bGUsXG4gICAgRGRyQ2xpY2tPdXRzaWRlTW9kdWxlLFxuICAgIERkck5nTW9kZWxCYXNlTW9kdWxlXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1xuICAgIERkckRyb3Bkb3duQ29tcG9uZW50XG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBEZHJEcm9wZG93bkNvbXBvbmVudFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIERkckRyb3Bkb3duTW9kdWxlIHsgfVxuIl19