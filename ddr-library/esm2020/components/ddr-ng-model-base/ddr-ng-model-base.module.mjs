import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DdrNgModelBase } from './ddr-ng-model-base.component';
import * as i0 from "@angular/core";
export class DdrNgModelBaseModule {
}
DdrNgModelBaseModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrNgModelBaseModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrNgModelBaseModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrNgModelBaseModule, declarations: [DdrNgModelBase], imports: [CommonModule], exports: [DdrNgModelBase] });
DdrNgModelBaseModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrNgModelBaseModule, imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrNgModelBaseModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        DdrNgModelBase
                    ],
                    exports: [
                        DdrNgModelBase
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLW5nLW1vZGVsLWJhc2UubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL2NvbXBvbmVudHMvZGRyLW5nLW1vZGVsLWJhc2UvZGRyLW5nLW1vZGVsLWJhc2UubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQzs7QUFhL0QsTUFBTSxPQUFPLG9CQUFvQjs7aUhBQXBCLG9CQUFvQjtrSEFBcEIsb0JBQW9CLGlCQU43QixjQUFjLGFBSGQsWUFBWSxhQU1aLGNBQWM7a0hBR0wsb0JBQW9CLFlBVnRCO1lBQ1AsWUFBWTtTQUNiOzJGQVFVLG9CQUFvQjtrQkFYaEMsUUFBUTttQkFBQztvQkFDUixPQUFPLEVBQUU7d0JBQ1AsWUFBWTtxQkFDYjtvQkFDRCxZQUFZLEVBQUU7d0JBQ1osY0FBYztxQkFDZjtvQkFDRCxPQUFPLEVBQUU7d0JBQ1AsY0FBYztxQkFDZjtpQkFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgRGRyTmdNb2RlbEJhc2UgfSBmcm9tICcuL2Rkci1uZy1tb2RlbC1iYXNlLmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGVcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgRGRyTmdNb2RlbEJhc2VcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIERkck5nTW9kZWxCYXNlXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgRGRyTmdNb2RlbEJhc2VNb2R1bGUgeyB9XG4iXX0=