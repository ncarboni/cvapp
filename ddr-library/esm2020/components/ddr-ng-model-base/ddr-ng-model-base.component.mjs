import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import * as i0 from "@angular/core";
export class DdrNgModelBase {
    constructor() {
        this._innerValue = null;
        this._firstTime = true;
        this._firstValue = new Subject();
    }
    get value() {
        return this._innerValue;
    }
    set value(value) {
        this._innerValue = value;
        if (this.onChange) {
            this.onChange(value);
        }
    }
    get firstValue() {
        return this._firstValue;
    }
    set firstValue(value) {
        this._firstValue = value;
    }
    writeValue(value) {
        if (value !== this._innerValue) {
            this.value = value;
            if (this._firstTime) {
                this._firstValue.next(this.value);
                this._firstTime = false;
            }
        }
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) { }
}
DdrNgModelBase.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrNgModelBase, deps: [], target: i0.ɵɵFactoryTarget.Component });
DdrNgModelBase.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrNgModelBase, selector: "ng-component", ngImport: i0, template: '', isInline: true });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrNgModelBase, decorators: [{
            type: Component,
            args: [{
                    template: ''
                }]
        }], ctorParameters: function () { return []; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLW5nLW1vZGVsLWJhc2UuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL2NvbXBvbmVudHMvZGRyLW5nLW1vZGVsLWJhc2UvZGRyLW5nLW1vZGVsLWJhc2UuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFMUMsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQzs7QUFLL0IsTUFBTSxPQUFPLGNBQWM7SUF3QnpCO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLE9BQU8sRUFBTyxDQUFDO0lBQ3hDLENBQUM7SUF0QkQsSUFBVyxLQUFLO1FBQ2QsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzFCLENBQUM7SUFDRCxJQUFXLEtBQUssQ0FBQyxLQUFVO1FBQ3pCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLElBQUcsSUFBSSxDQUFDLFFBQVEsRUFBQztZQUNmLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDdEI7SUFDSCxDQUFDO0lBRUQsSUFBVyxVQUFVO1FBQ25CLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUMxQixDQUFDO0lBRUQsSUFBVyxVQUFVLENBQUMsS0FBbUI7UUFDdkMsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7SUFDM0IsQ0FBQztJQVVELFVBQVUsQ0FBQyxLQUFVO1FBQ25CLElBQUcsS0FBSyxLQUFLLElBQUksQ0FBQyxXQUFXLEVBQUM7WUFDNUIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7WUFDbkIsSUFBRyxJQUFJLENBQUMsVUFBVSxFQUFDO2dCQUNqQixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2xDLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO2FBQ3pCO1NBQ0Y7SUFDSCxDQUFDO0lBQ0QsZ0JBQWdCLENBQUMsRUFBTztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRUQsaUJBQWlCLENBQUMsRUFBTyxJQUFVLENBQUM7OzJHQTdDekIsY0FBYzsrRkFBZCxjQUFjLG9EQUZmLEVBQUU7MkZBRUQsY0FBYztrQkFIMUIsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsRUFBRTtpQkFDYiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29udHJvbFZhbHVlQWNjZXNzb3IgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5cbkBDb21wb25lbnQoe1xuICB0ZW1wbGF0ZTogJydcbn0pXG5leHBvcnQgY2xhc3MgRGRyTmdNb2RlbEJhc2UgaW1wbGVtZW50cyBDb250cm9sVmFsdWVBY2Nlc3NvciB7XG5cbiAgcHJpdmF0ZSBfaW5uZXJWYWx1ZTogYW55O1xuICBwcml2YXRlIF9maXJzdFRpbWU6IGJvb2xlYW47XG4gIHByaXZhdGUgX2ZpcnN0VmFsdWU6IFN1YmplY3Q8YW55PjtcblxuICBwdWJsaWMgZ2V0IHZhbHVlKCk6IGFueSB7XG4gICAgcmV0dXJuIHRoaXMuX2lubmVyVmFsdWU7XG4gIH1cbiAgcHVibGljIHNldCB2YWx1ZSh2YWx1ZTogYW55KSB7XG4gICAgdGhpcy5faW5uZXJWYWx1ZSA9IHZhbHVlO1xuICAgIGlmKHRoaXMub25DaGFuZ2Upe1xuICAgICAgdGhpcy5vbkNoYW5nZSh2YWx1ZSk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGdldCBmaXJzdFZhbHVlKCk6IFN1YmplY3Q8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuX2ZpcnN0VmFsdWU7XG4gIH1cblxuICBwdWJsaWMgc2V0IGZpcnN0VmFsdWUodmFsdWU6IFN1YmplY3Q8YW55Pikge1xuICAgIHRoaXMuX2ZpcnN0VmFsdWUgPSB2YWx1ZTtcbiAgfVxuICBcbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5faW5uZXJWYWx1ZSA9IG51bGw7XG4gICAgdGhpcy5fZmlyc3RUaW1lID0gdHJ1ZTtcbiAgICB0aGlzLl9maXJzdFZhbHVlID0gbmV3IFN1YmplY3Q8YW55PigpO1xuICB9XG5cbiAgcHJpdmF0ZSBvbkNoYW5nZTogKF86IGFueSkgPT4ge307XG5cbiAgd3JpdGVWYWx1ZSh2YWx1ZTogYW55KTogdm9pZCB7XG4gICAgaWYodmFsdWUgIT09IHRoaXMuX2lubmVyVmFsdWUpe1xuICAgICAgdGhpcy52YWx1ZSA9IHZhbHVlO1xuICAgICAgaWYodGhpcy5fZmlyc3RUaW1lKXtcbiAgICAgICAgdGhpcy5fZmlyc3RWYWx1ZS5uZXh0KHRoaXMudmFsdWUpO1xuICAgICAgICB0aGlzLl9maXJzdFRpbWUgPSBmYWxzZTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgcmVnaXN0ZXJPbkNoYW5nZShmbjogYW55KTogdm9pZCB7XG4gICAgdGhpcy5vbkNoYW5nZSA9IGZuO1xuICB9XG5cbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm46IGFueSk6IHZvaWQgeyB9XG5cbn1cbiJdfQ==