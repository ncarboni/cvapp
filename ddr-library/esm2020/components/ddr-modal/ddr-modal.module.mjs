import { DdrClickOutsideModule } from './../../directives/ddr-click-outside/ddr-click-outside.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DdrModalComponent } from './ddr-modal.component';
import { DdrModalService } from './ddr-modal.service';
import * as i0 from "@angular/core";
export class DdrModalModule {
}
DdrModalModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrModalModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrModalModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrModalModule, declarations: [DdrModalComponent], imports: [CommonModule,
        DdrClickOutsideModule], exports: [DdrModalComponent] });
DdrModalModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrModalModule, providers: [
        DdrModalService
    ], imports: [[
            CommonModule,
            DdrClickOutsideModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrModalModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule,
                        DdrClickOutsideModule
                    ],
                    declarations: [
                        DdrModalComponent
                    ],
                    exports: [
                        DdrModalComponent
                    ],
                    providers: [
                        DdrModalService
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLW1vZGFsLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2Rkci1saWJyYXJ5L3NyYy9jb21wb25lbnRzL2Rkci1tb2RhbC9kZHItbW9kYWwubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLCtEQUErRCxDQUFDO0FBQ3RHLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzFELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQzs7QUFpQnRELE1BQU0sT0FBTyxjQUFjOzsyR0FBZCxjQUFjOzRHQUFkLGNBQWMsaUJBVHZCLGlCQUFpQixhQUpqQixZQUFZO1FBQ1oscUJBQXFCLGFBTXJCLGlCQUFpQjs0R0FNUixjQUFjLGFBSmQ7UUFDVCxlQUFlO0tBQ2hCLFlBWlE7WUFDUCxZQUFZO1lBQ1oscUJBQXFCO1NBQ3RCOzJGQVdVLGNBQWM7a0JBZjFCLFFBQVE7bUJBQUM7b0JBQ1IsT0FBTyxFQUFFO3dCQUNQLFlBQVk7d0JBQ1oscUJBQXFCO3FCQUN0QjtvQkFDRCxZQUFZLEVBQUU7d0JBQ1osaUJBQWlCO3FCQUNsQjtvQkFDRCxPQUFPLEVBQUU7d0JBQ1AsaUJBQWlCO3FCQUNsQjtvQkFDRCxTQUFTLEVBQUU7d0JBQ1QsZUFBZTtxQkFDaEI7aUJBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEZHJDbGlja091dHNpZGVNb2R1bGUgfSBmcm9tICcuLy4uLy4uL2RpcmVjdGl2ZXMvZGRyLWNsaWNrLW91dHNpZGUvZGRyLWNsaWNrLW91dHNpZGUubW9kdWxlJztcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgRGRyTW9kYWxDb21wb25lbnQgfSBmcm9tICcuL2Rkci1tb2RhbC5jb21wb25lbnQnO1xuaW1wb3J0IHsgRGRyTW9kYWxTZXJ2aWNlIH0gZnJvbSAnLi9kZHItbW9kYWwuc2VydmljZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGUsXG4gICAgRGRyQ2xpY2tPdXRzaWRlTW9kdWxlXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1xuICAgIERkck1vZGFsQ29tcG9uZW50XG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBEZHJNb2RhbENvbXBvbmVudFxuICBdLFxuICBwcm92aWRlcnM6IFtcbiAgICBEZHJNb2RhbFNlcnZpY2VcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBEZHJNb2RhbE1vZHVsZSB7IH1cbiJdfQ==