import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class DdrModalService {
    constructor() {
        this.modals = [];
    }
    add(modal) {
        if (!this.modals.find(m => m.id === modal.id)) {
            this.modals.push(modal);
        }
    }
    remove(id) {
        this.modals = this.modals.filter(m => m.id != id);
    }
    open(id) {
        const modal = this.modals.find(x => x.id == id);
        if (modal) {
            modal.show = true;
        }
        else {
            console.error("Modal not exists");
        }
    }
    close(id) {
        const modal = this.modals.find(x => x.id == id);
        if (modal) {
            modal.show = false;
        }
        else {
            console.error("Modal not exists");
        }
    }
}
DdrModalService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrModalService, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
DdrModalService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrModalService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrModalService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLW1vZGFsLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9kZHItbGlicmFyeS9zcmMvY29tcG9uZW50cy9kZHItbW9kYWwvZGRyLW1vZGFsLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFNM0MsTUFBTSxPQUFPLGVBQWU7SUFJMUI7UUFDRSxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQTtJQUNsQixDQUFDO0lBRUQsR0FBRyxDQUFDLEtBQXdCO1FBQzFCLElBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssS0FBSyxDQUFDLEVBQUUsQ0FBQyxFQUFDO1lBQzNDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pCO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQyxFQUFVO1FBQ2YsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVELElBQUksQ0FBQyxFQUFVO1FBQ2IsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBQ2hELElBQUcsS0FBSyxFQUFDO1lBQ1AsS0FBSyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7U0FDbkI7YUFBSTtZQUNILE9BQU8sQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsQ0FBQztTQUNuQztJQUNILENBQUM7SUFFRCxLQUFLLENBQUMsRUFBVTtRQUNkLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUNoRCxJQUFHLEtBQUssRUFBQztZQUNQLEtBQUssQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO1NBQ3BCO2FBQUk7WUFDSCxPQUFPLENBQUMsS0FBSyxDQUFDLGtCQUFrQixDQUFDLENBQUM7U0FDbkM7SUFDSCxDQUFDOzs0R0FsQ1UsZUFBZTtnSEFBZixlQUFlLGNBRmQsTUFBTTsyRkFFUCxlQUFlO2tCQUgzQixVQUFVO21CQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IERkck1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi9kZHItbW9kYWwuY29tcG9uZW50JztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgRGRyTW9kYWxTZXJ2aWNlIHtcblxuICBwcml2YXRlIG1vZGFsczogRGRyTW9kYWxDb21wb25lbnRbXTtcblxuICBjb25zdHJ1Y3RvcigpIHsgXG4gICAgdGhpcy5tb2RhbHMgPSBbXVxuICB9XG5cbiAgYWRkKG1vZGFsOiBEZHJNb2RhbENvbXBvbmVudCl7XG4gICAgaWYoIXRoaXMubW9kYWxzLmZpbmQobSA9PiBtLmlkID09PSBtb2RhbC5pZCkpe1xuICAgICAgdGhpcy5tb2RhbHMucHVzaChtb2RhbCk7XG4gICAgfVxuICB9XG5cbiAgcmVtb3ZlKGlkOiBzdHJpbmcpe1xuICAgIHRoaXMubW9kYWxzID0gdGhpcy5tb2RhbHMuZmlsdGVyKG0gPT4gbS5pZCAhPSBpZCk7XG4gIH1cblxuICBvcGVuKGlkOiBzdHJpbmcpe1xuICAgIGNvbnN0IG1vZGFsID0gdGhpcy5tb2RhbHMuZmluZCh4ID0+IHguaWQgPT0gaWQpO1xuICAgIGlmKG1vZGFsKXtcbiAgICAgIG1vZGFsLnNob3cgPSB0cnVlO1xuICAgIH1lbHNle1xuICAgICAgY29uc29sZS5lcnJvcihcIk1vZGFsIG5vdCBleGlzdHNcIik7XG4gICAgfVxuICB9XG5cbiAgY2xvc2UoaWQ6IHN0cmluZyl7XG4gICAgY29uc3QgbW9kYWwgPSB0aGlzLm1vZGFscy5maW5kKHggPT4geC5pZCA9PSBpZCk7XG4gICAgaWYobW9kYWwpe1xuICAgICAgbW9kYWwuc2hvdyA9IGZhbHNlO1xuICAgIH1lbHNle1xuICAgICAgY29uc29sZS5lcnJvcihcIk1vZGFsIG5vdCBleGlzdHNcIik7XG4gICAgfVxuICB9XG5cbn1cbiJdfQ==