import { DdrSelectedItem } from './../../../common/ddr-selected-item';
import { DdrBlockListItemComponent } from './../ddr-block-list-item/ddr-block-list-item.component';
import { Component, ContentChild, EventEmitter, Input, Output, ViewEncapsulation, ViewChildren } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "./../../../services/ddr-constants.service";
import * as i2 from "../ddr-block-list-item/ddr-block-list-item.component";
import * as i3 from "ngx-pagination";
import * as i4 from "@angular/common";
export class DdrBlockListComponent {
    constructor(constants) {
        this.constants = constants;
        this.labelNoResults = 'No results';
        this.showHeader = true;
        this.showInfoAdditional = true;
        this.showActions = true;
        this.showBorder = true;
        this.pagination = this.constants.DdrBlockListConstants.PAGINATION_DEFAULT;
        this.previousLabel = 'Previuos';
        this.nextLabel = 'Next';
        this.page = 1;
        this.itemSelected = new EventEmitter();
        this.actionSelected = new EventEmitter();
    }
    ngOnInit() {
        if (this.pagination < 0) {
            this.pagination = this.constants.DdrBlockListConstants.PAGINATION_DEFAULT;
        }
    }
    ngOnChanges(changes) {
        if (changes) {
            if (changes['blockItems'] && !changes['blockItems'].firstChange) {
                if (((this.page - 1) * this.pagination) == changes['blockItems'].currentValue.length) {
                    this.page = this.page - 1;
                }
            }
        }
    }
    sendAction($event) {
        this.actionSelected.emit($event);
    }
    selectItem(blockItem, index) {
        let selectedItem = new DdrSelectedItem();
        selectedItem.item = blockItem.item;
        selectedItem.index = ((this.page - 1) * this.pagination) + index;
        this.itemSelected.emit(selectedItem);
    }
    closeAllOptionsExcept(id) {
        this.blocks.forEach(block => {
            if (block.id !== id) {
                block.showOptions = false;
            }
        });
    }
}
DdrBlockListComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrBlockListComponent, deps: [{ token: i1.DdrConstantsService }], target: i0.ɵɵFactoryTarget.Component });
DdrBlockListComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrBlockListComponent, selector: "ddr-block-list", inputs: { blockItems: "blockItems", labelNoResults: "labelNoResults", showHeader: "showHeader", showInfoAdditional: "showInfoAdditional", showActions: "showActions", showBorder: "showBorder", pagination: "pagination", previousLabel: "previousLabel", nextLabel: "nextLabel" }, outputs: { itemSelected: "itemSelected", actionSelected: "actionSelected" }, queries: [{ propertyName: "templateHeader", first: true, predicate: ["templateHeader"], descendants: true }, { propertyName: "templateInfoAdditional", first: true, predicate: ["templateInfoAdditional"], descendants: true }, { propertyName: "templateData", first: true, predicate: ["templateData"], descendants: true }], viewQueries: [{ propertyName: "blocks", predicate: DdrBlockListItemComponent, descendants: true }], usesOnChanges: true, ngImport: i0, template: "\n<div class=\"ddr-block-list row\">\n  <div class=\"col-12\">\n\n    <div class=\"row\" *ngIf=\"blockItems?.length == 0\">\n      <div class=\"col-12 text-center\">\n        <span>{{labelNoResults}}</span>\n      </div>\n    </div>\n\n    <div class=\"row\" *ngIf=\"blockItems?.length > 0\">\n      <div class=\"col-12\">\n        \n        <ddr-block-list-item *ngFor=\"let blockItem of blockItems | paginate: { itemsPerPage: pagination, currentPage: page }; let index = index\"\n          [blockItem]=\"blockItem\"\n          [id]=\"'block-item-' + index\"\n          [showHeader]=\"showHeader\"\n          [showInfoAdditional]=\"showInfoAdditional\"\n          [showActions]=\"showActions\"\n          [showBorder]=\"showBorder\"\n          [templateHeader]=\"templateHeader\"\n          [templateInfoAdditional]=\"templateInfoAdditional\"\n          [templateData]=\"templateData\"\n          (actionSelected)=\"sendAction($event)\"\n          (closeOptions)=\"closeAllOptionsExcept($event)\"\n          (click)=\"selectItem(blockItem, index)\">\n        </ddr-block-list-item>\n\n      </div>\n    </div>\n    \n    <!--Pagination-->\n    <div class=\"row\">\n      <div class=\"col-12 p-4 text-center\">\n        <pagination-controls \n          [autoHide]=\"true\"\n          [previousLabel]=\"previousLabel\"\n          [nextLabel]=\"nextLabel\"\n          (pageChange)=\"page = $event\">\n        </pagination-controls>\n      </div>\n    </div>\n\n  </div>\n</div>", styles: [""], components: [{ type: i2.DdrBlockListItemComponent, selector: "ddr-block-list-item", inputs: ["blockItem", "id", "showHeader", "showInfoAdditional", "showActions", "showBorder", "templateHeader", "templateInfoAdditional", "templateData"], outputs: ["actionSelected", "closeOptions"] }, { type: i3.PaginationControlsComponent, selector: "pagination-controls", inputs: ["maxSize", "previousLabel", "nextLabel", "screenReaderPaginationLabel", "screenReaderPageLabel", "screenReaderCurrentLabel", "directionLinks", "autoHide", "responsive", "id"], outputs: ["pageChange", "pageBoundsCorrection"] }], directives: [{ type: i4.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i4.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }], pipes: { "paginate": i3.PaginatePipe }, encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrBlockListComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-block-list', encapsulation: ViewEncapsulation.None, template: "\n<div class=\"ddr-block-list row\">\n  <div class=\"col-12\">\n\n    <div class=\"row\" *ngIf=\"blockItems?.length == 0\">\n      <div class=\"col-12 text-center\">\n        <span>{{labelNoResults}}</span>\n      </div>\n    </div>\n\n    <div class=\"row\" *ngIf=\"blockItems?.length > 0\">\n      <div class=\"col-12\">\n        \n        <ddr-block-list-item *ngFor=\"let blockItem of blockItems | paginate: { itemsPerPage: pagination, currentPage: page }; let index = index\"\n          [blockItem]=\"blockItem\"\n          [id]=\"'block-item-' + index\"\n          [showHeader]=\"showHeader\"\n          [showInfoAdditional]=\"showInfoAdditional\"\n          [showActions]=\"showActions\"\n          [showBorder]=\"showBorder\"\n          [templateHeader]=\"templateHeader\"\n          [templateInfoAdditional]=\"templateInfoAdditional\"\n          [templateData]=\"templateData\"\n          (actionSelected)=\"sendAction($event)\"\n          (closeOptions)=\"closeAllOptionsExcept($event)\"\n          (click)=\"selectItem(blockItem, index)\">\n        </ddr-block-list-item>\n\n      </div>\n    </div>\n    \n    <!--Pagination-->\n    <div class=\"row\">\n      <div class=\"col-12 p-4 text-center\">\n        <pagination-controls \n          [autoHide]=\"true\"\n          [previousLabel]=\"previousLabel\"\n          [nextLabel]=\"nextLabel\"\n          (pageChange)=\"page = $event\">\n        </pagination-controls>\n      </div>\n    </div>\n\n  </div>\n</div>", styles: [""] }]
        }], ctorParameters: function () { return [{ type: i1.DdrConstantsService }]; }, propDecorators: { blockItems: [{
                type: Input
            }], labelNoResults: [{
                type: Input
            }], showHeader: [{
                type: Input
            }], showInfoAdditional: [{
                type: Input
            }], showActions: [{
                type: Input
            }], showBorder: [{
                type: Input
            }], pagination: [{
                type: Input
            }], previousLabel: [{
                type: Input
            }], nextLabel: [{
                type: Input
            }], itemSelected: [{
                type: Output
            }], actionSelected: [{
                type: Output
            }], templateHeader: [{
                type: ContentChild,
                args: ["templateHeader", { static: false }]
            }], templateInfoAdditional: [{
                type: ContentChild,
                args: ["templateInfoAdditional", { static: false }]
            }], templateData: [{
                type: ContentChild,
                args: ["templateData", { static: false }]
            }], blocks: [{
                type: ViewChildren,
                args: [DdrBlockListItemComponent]
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWJsb2NrLWxpc3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL2NvbXBvbmVudHMvZGRyLWJsb2NrLWxpc3QvZGRyLWJsb2NrLWxpc3QvZGRyLWJsb2NrLWxpc3QuY29tcG9uZW50LnRzIiwiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL2NvbXBvbmVudHMvZGRyLWJsb2NrLWxpc3QvZGRyLWJsb2NrLWxpc3QvZGRyLWJsb2NrLWxpc3QuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBRXRFLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLHdEQUF3RCxDQUFDO0FBR25HLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQVUsTUFBTSxFQUFFLGlCQUFpQixFQUFlLFlBQVksRUFBdUMsTUFBTSxlQUFlLENBQUM7Ozs7OztBQVFoTCxNQUFNLE9BQU8scUJBQXFCO0lBeUJoQyxZQUNVLFNBQThCO1FBQTlCLGNBQVMsR0FBVCxTQUFTLENBQXFCO1FBdkIvQixtQkFBYyxHQUFXLFlBQVksQ0FBQztRQUV0QyxlQUFVLEdBQVksSUFBSSxDQUFDO1FBQzNCLHVCQUFrQixHQUFZLElBQUksQ0FBQztRQUNuQyxnQkFBVyxHQUFZLElBQUksQ0FBQztRQUM1QixlQUFVLEdBQVksSUFBSSxDQUFDO1FBRTNCLGVBQVUsR0FBVyxJQUFJLENBQUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLGtCQUFrQixDQUFDO1FBQzdFLGtCQUFhLEdBQVcsVUFBVSxDQUFDO1FBQ25DLGNBQVMsR0FBVyxNQUFNLENBQUM7UUFnQmxDLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLFlBQVksRUFBc0IsQ0FBQztRQUMzRCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksWUFBWSxFQUFnQixDQUFDO0lBQ3pELENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBRyxJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsRUFBQztZQUNyQixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsa0JBQWtCLENBQUM7U0FDM0U7SUFDSCxDQUFDO0lBRUQsV0FBVyxDQUFDLE9BQXNCO1FBQ2hDLElBQUcsT0FBTyxFQUFDO1lBQ1QsSUFBRyxPQUFPLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUMsV0FBVyxFQUFDO2dCQUM3RCxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRTtvQkFDcEYsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQztpQkFDM0I7YUFDRjtTQUNGO0lBQ0gsQ0FBQztJQUdELFVBQVUsQ0FBQyxNQUFvQjtRQUM3QixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsVUFBVSxDQUFDLFNBQTBCLEVBQUUsS0FBYTtRQUNsRCxJQUFJLFlBQVksR0FBRyxJQUFJLGVBQWUsRUFBSyxDQUFDO1FBQzVDLFlBQVksQ0FBQyxJQUFJLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQztRQUNuQyxZQUFZLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxLQUFLLENBQUM7UUFDakUsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELHFCQUFxQixDQUFDLEVBQVU7UUFFOUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDMUIsSUFBRyxLQUFLLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBQztnQkFDakIsS0FBSyxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7YUFDM0I7UUFDSCxDQUFDLENBQUMsQ0FBQTtJQUVKLENBQUM7O2tIQXJFVSxxQkFBcUI7c0dBQXJCLHFCQUFxQixrdkJBdUJsQix5QkFBeUIscUVDcEN6QyxxOENBNENNOzJGRC9CTyxxQkFBcUI7a0JBTmpDLFNBQVM7K0JBQ0UsZ0JBQWdCLGlCQUdYLGlCQUFpQixDQUFDLElBQUk7MEdBSTVCLFVBQVU7c0JBQWxCLEtBQUs7Z0JBQ0csY0FBYztzQkFBdEIsS0FBSztnQkFFRyxVQUFVO3NCQUFsQixLQUFLO2dCQUNHLGtCQUFrQjtzQkFBMUIsS0FBSztnQkFDRyxXQUFXO3NCQUFuQixLQUFLO2dCQUNHLFVBQVU7c0JBQWxCLEtBQUs7Z0JBRUcsVUFBVTtzQkFBbEIsS0FBSztnQkFDRyxhQUFhO3NCQUFyQixLQUFLO2dCQUNHLFNBQVM7c0JBQWpCLEtBQUs7Z0JBRUksWUFBWTtzQkFBckIsTUFBTTtnQkFDRyxjQUFjO3NCQUF2QixNQUFNO2dCQUUwQyxjQUFjO3NCQUE5RCxZQUFZO3VCQUFDLGdCQUFnQixFQUFFLEVBQUMsTUFBTSxFQUFFLEtBQUssRUFBQztnQkFDVSxzQkFBc0I7c0JBQTlFLFlBQVk7dUJBQUMsd0JBQXdCLEVBQUUsRUFBQyxNQUFNLEVBQUUsS0FBSyxFQUFDO2dCQUNSLFlBQVk7c0JBQTFELFlBQVk7dUJBQUMsY0FBYyxFQUFFLEVBQUMsTUFBTSxFQUFFLEtBQUssRUFBQztnQkFJSixNQUFNO3NCQUE5QyxZQUFZO3VCQUFDLHlCQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERkclNlbGVjdGVkSXRlbSB9IGZyb20gJy4vLi4vLi4vLi4vY29tbW9uL2Rkci1zZWxlY3RlZC1pdGVtJztcbmltcG9ydCB7IERkckFjdGlvbiB9IGZyb20gJy4vLi4vLi4vLi4vY29tbW9uL2Rkci1hY3Rpb24nO1xuaW1wb3J0IHsgRGRyQmxvY2tMaXN0SXRlbUNvbXBvbmVudCB9IGZyb20gJy4vLi4vZGRyLWJsb2NrLWxpc3QtaXRlbS9kZHItYmxvY2stbGlzdC1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEZHJDb25zdGFudHNTZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi8uLi9zZXJ2aWNlcy9kZHItY29uc3RhbnRzLnNlcnZpY2UnO1xuaW1wb3J0IHsgRGRyQmxvY2tJdGVtIH0gZnJvbSAnLi8uLi9iZWFuL2Rkci1ibG9jay1pdGVtJztcbmltcG9ydCB7IENvbXBvbmVudCwgQ29udGVudENoaWxkLCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkluaXQsIE91dHB1dCwgVmlld0VuY2Fwc3VsYXRpb24sIFRlbXBsYXRlUmVmLCBWaWV3Q2hpbGRyZW4sIFF1ZXJ5TGlzdCwgT25DaGFuZ2VzLCBTaW1wbGVDaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2Rkci1ibG9jay1saXN0JyxcbiAgdGVtcGxhdGVVcmw6ICcuL2Rkci1ibG9jay1saXN0LmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vZGRyLWJsb2NrLWxpc3QuY29tcG9uZW50LnNjc3MnXSxcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxufSlcbmV4cG9ydCBjbGFzcyBEZHJCbG9ja0xpc3RDb21wb25lbnQ8VD4gaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XG5cbiAgQElucHV0KCkgYmxvY2tJdGVtczogRGRyQmxvY2tJdGVtPFQ+W107XG4gIEBJbnB1dCgpIGxhYmVsTm9SZXN1bHRzOiBzdHJpbmcgPSAnTm8gcmVzdWx0cyc7XG5cbiAgQElucHV0KCkgc2hvd0hlYWRlcjogYm9vbGVhbiA9IHRydWU7XG4gIEBJbnB1dCgpIHNob3dJbmZvQWRkaXRpb25hbDogYm9vbGVhbiA9IHRydWU7XG4gIEBJbnB1dCgpIHNob3dBY3Rpb25zOiBib29sZWFuID0gdHJ1ZTtcbiAgQElucHV0KCkgc2hvd0JvcmRlcjogYm9vbGVhbiA9IHRydWU7XG5cbiAgQElucHV0KCkgcGFnaW5hdGlvbjogbnVtYmVyID0gdGhpcy5jb25zdGFudHMuRGRyQmxvY2tMaXN0Q29uc3RhbnRzLlBBR0lOQVRJT05fREVGQVVMVDtcbiAgQElucHV0KCkgcHJldmlvdXNMYWJlbDogc3RyaW5nID0gJ1ByZXZpdW9zJztcbiAgQElucHV0KCkgbmV4dExhYmVsOiBzdHJpbmcgPSAnTmV4dCc7XG5cbiAgQE91dHB1dCgpIGl0ZW1TZWxlY3RlZDogRXZlbnRFbWl0dGVyPERkclNlbGVjdGVkSXRlbTxUPj47XG4gIEBPdXRwdXQoKSBhY3Rpb25TZWxlY3RlZDogRXZlbnRFbWl0dGVyPERkckFjdGlvbjxUPj47XG5cbiAgQENvbnRlbnRDaGlsZChcInRlbXBsYXRlSGVhZGVyXCIsIHtzdGF0aWM6IGZhbHNlfSkgdGVtcGxhdGVIZWFkZXI6IFRlbXBsYXRlUmVmPGFueT47XG4gIEBDb250ZW50Q2hpbGQoXCJ0ZW1wbGF0ZUluZm9BZGRpdGlvbmFsXCIsIHtzdGF0aWM6IGZhbHNlfSkgdGVtcGxhdGVJbmZvQWRkaXRpb25hbDogVGVtcGxhdGVSZWY8YW55PjtcbiAgQENvbnRlbnRDaGlsZChcInRlbXBsYXRlRGF0YVwiLCB7c3RhdGljOiBmYWxzZX0pIHRlbXBsYXRlRGF0YTogVGVtcGxhdGVSZWY8YW55PjtcblxuICBwdWJsaWMgcGFnZTogbnVtYmVyO1xuXG4gIEBWaWV3Q2hpbGRyZW4oRGRyQmxvY2tMaXN0SXRlbUNvbXBvbmVudCkgYmxvY2tzOiBRdWVyeUxpc3Q8RGRyQmxvY2tMaXN0SXRlbUNvbXBvbmVudDxUPj47XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBjb25zdGFudHM6IERkckNvbnN0YW50c1NlcnZpY2VcbiAgKSB7IFxuICAgIHRoaXMucGFnZSA9IDE7XG4gICAgdGhpcy5pdGVtU2VsZWN0ZWQgPSBuZXcgRXZlbnRFbWl0dGVyPERkclNlbGVjdGVkSXRlbTxUPj4oKTtcbiAgICB0aGlzLmFjdGlvblNlbGVjdGVkID0gbmV3IEV2ZW50RW1pdHRlcjxEZHJBY3Rpb248VD4+KCk7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBpZih0aGlzLnBhZ2luYXRpb24gPCAwKXtcbiAgICAgIHRoaXMucGFnaW5hdGlvbiA9IHRoaXMuY29uc3RhbnRzLkRkckJsb2NrTGlzdENvbnN0YW50cy5QQUdJTkFUSU9OX0RFRkFVTFQ7XG4gICAgfVxuICB9XG5cbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xuICAgIGlmKGNoYW5nZXMpe1xuICAgICAgaWYoY2hhbmdlc1snYmxvY2tJdGVtcyddICYmICFjaGFuZ2VzWydibG9ja0l0ZW1zJ10uZmlyc3RDaGFuZ2Upe1xuICAgICAgICBpZiggKCh0aGlzLnBhZ2UgLSAxKSAqIHRoaXMucGFnaW5hdGlvbikgPT0gY2hhbmdlc1snYmxvY2tJdGVtcyddLmN1cnJlbnRWYWx1ZS5sZW5ndGggKXtcbiAgICAgICAgICB0aGlzLnBhZ2UgPSB0aGlzLnBhZ2UgLSAxO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG4gIFxuXG4gIHNlbmRBY3Rpb24oJGV2ZW50OiBEZHJBY3Rpb248VD4pe1xuICAgIHRoaXMuYWN0aW9uU2VsZWN0ZWQuZW1pdCgkZXZlbnQpO1xuICB9XG5cbiAgc2VsZWN0SXRlbShibG9ja0l0ZW06IERkckJsb2NrSXRlbTxUPiwgaW5kZXg6IG51bWJlcil7XG4gICAgbGV0IHNlbGVjdGVkSXRlbSA9IG5ldyBEZHJTZWxlY3RlZEl0ZW08VD4oKTtcbiAgICBzZWxlY3RlZEl0ZW0uaXRlbSA9IGJsb2NrSXRlbS5pdGVtO1xuICAgIHNlbGVjdGVkSXRlbS5pbmRleCA9ICgodGhpcy5wYWdlIC0gMSkgKiB0aGlzLnBhZ2luYXRpb24pICsgaW5kZXg7XG4gICAgdGhpcy5pdGVtU2VsZWN0ZWQuZW1pdChzZWxlY3RlZEl0ZW0pO1xuICB9XG4gIFxuICBjbG9zZUFsbE9wdGlvbnNFeGNlcHQoaWQ6IHN0cmluZyl7XG5cbiAgICB0aGlzLmJsb2Nrcy5mb3JFYWNoKGJsb2NrID0+IHtcbiAgICAgIGlmKGJsb2NrLmlkICE9PSBpZCl7XG4gICAgICAgIGJsb2NrLnNob3dPcHRpb25zID0gZmFsc2U7XG4gICAgICB9XG4gICAgfSlcblxuICB9XG5cbn1cbiIsIlxuPGRpdiBjbGFzcz1cImRkci1ibG9jay1saXN0IHJvd1wiPlxuICA8ZGl2IGNsYXNzPVwiY29sLTEyXCI+XG5cbiAgICA8ZGl2IGNsYXNzPVwicm93XCIgKm5nSWY9XCJibG9ja0l0ZW1zPy5sZW5ndGggPT0gMFwiPlxuICAgICAgPGRpdiBjbGFzcz1cImNvbC0xMiB0ZXh0LWNlbnRlclwiPlxuICAgICAgICA8c3Bhbj57e2xhYmVsTm9SZXN1bHRzfX08L3NwYW4+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cblxuICAgIDxkaXYgY2xhc3M9XCJyb3dcIiAqbmdJZj1cImJsb2NrSXRlbXM/Lmxlbmd0aCA+IDBcIj5cbiAgICAgIDxkaXYgY2xhc3M9XCJjb2wtMTJcIj5cbiAgICAgICAgXG4gICAgICAgIDxkZHItYmxvY2stbGlzdC1pdGVtICpuZ0Zvcj1cImxldCBibG9ja0l0ZW0gb2YgYmxvY2tJdGVtcyB8IHBhZ2luYXRlOiB7IGl0ZW1zUGVyUGFnZTogcGFnaW5hdGlvbiwgY3VycmVudFBhZ2U6IHBhZ2UgfTsgbGV0IGluZGV4ID0gaW5kZXhcIlxuICAgICAgICAgIFtibG9ja0l0ZW1dPVwiYmxvY2tJdGVtXCJcbiAgICAgICAgICBbaWRdPVwiJ2Jsb2NrLWl0ZW0tJyArIGluZGV4XCJcbiAgICAgICAgICBbc2hvd0hlYWRlcl09XCJzaG93SGVhZGVyXCJcbiAgICAgICAgICBbc2hvd0luZm9BZGRpdGlvbmFsXT1cInNob3dJbmZvQWRkaXRpb25hbFwiXG4gICAgICAgICAgW3Nob3dBY3Rpb25zXT1cInNob3dBY3Rpb25zXCJcbiAgICAgICAgICBbc2hvd0JvcmRlcl09XCJzaG93Qm9yZGVyXCJcbiAgICAgICAgICBbdGVtcGxhdGVIZWFkZXJdPVwidGVtcGxhdGVIZWFkZXJcIlxuICAgICAgICAgIFt0ZW1wbGF0ZUluZm9BZGRpdGlvbmFsXT1cInRlbXBsYXRlSW5mb0FkZGl0aW9uYWxcIlxuICAgICAgICAgIFt0ZW1wbGF0ZURhdGFdPVwidGVtcGxhdGVEYXRhXCJcbiAgICAgICAgICAoYWN0aW9uU2VsZWN0ZWQpPVwic2VuZEFjdGlvbigkZXZlbnQpXCJcbiAgICAgICAgICAoY2xvc2VPcHRpb25zKT1cImNsb3NlQWxsT3B0aW9uc0V4Y2VwdCgkZXZlbnQpXCJcbiAgICAgICAgICAoY2xpY2spPVwic2VsZWN0SXRlbShibG9ja0l0ZW0sIGluZGV4KVwiPlxuICAgICAgICA8L2Rkci1ibG9jay1saXN0LWl0ZW0+XG5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICAgIFxuICAgIDwhLS1QYWdpbmF0aW9uLS0+XG4gICAgPGRpdiBjbGFzcz1cInJvd1wiPlxuICAgICAgPGRpdiBjbGFzcz1cImNvbC0xMiBwLTQgdGV4dC1jZW50ZXJcIj5cbiAgICAgICAgPHBhZ2luYXRpb24tY29udHJvbHMgXG4gICAgICAgICAgW2F1dG9IaWRlXT1cInRydWVcIlxuICAgICAgICAgIFtwcmV2aW91c0xhYmVsXT1cInByZXZpb3VzTGFiZWxcIlxuICAgICAgICAgIFtuZXh0TGFiZWxdPVwibmV4dExhYmVsXCJcbiAgICAgICAgICAocGFnZUNoYW5nZSk9XCJwYWdlID0gJGV2ZW50XCI+XG4gICAgICAgIDwvcGFnaW5hdGlvbi1jb250cm9scz5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuXG4gIDwvZGl2PlxuPC9kaXY+Il19