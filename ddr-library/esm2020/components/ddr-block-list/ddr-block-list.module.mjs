import { DdrClickOutsideModule } from './../../directives/ddr-click-outside/ddr-click-outside.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DdrBlockListItemComponent } from './ddr-block-list-item/ddr-block-list-item.component';
import { DdrBlockListDataItemComponent } from './ddr-block-list-data-item/ddr-block-list-data-item.component';
import { DdrBlockListComponent } from './ddr-block-list/ddr-block-list.component';
import { NgxPaginationModule } from 'ngx-pagination';
import * as i0 from "@angular/core";
export class DdrBlockListModule {
}
DdrBlockListModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrBlockListModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrBlockListModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrBlockListModule, declarations: [DdrBlockListItemComponent,
        DdrBlockListDataItemComponent,
        DdrBlockListComponent], imports: [CommonModule,
        DdrClickOutsideModule,
        NgxPaginationModule], exports: [DdrBlockListComponent,
        DdrBlockListDataItemComponent] });
DdrBlockListModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrBlockListModule, imports: [[
            CommonModule,
            DdrClickOutsideModule,
            NgxPaginationModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrBlockListModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule,
                        DdrClickOutsideModule,
                        NgxPaginationModule
                    ],
                    declarations: [
                        DdrBlockListItemComponent,
                        DdrBlockListDataItemComponent,
                        DdrBlockListComponent
                    ],
                    exports: [
                        DdrBlockListComponent,
                        DdrBlockListDataItemComponent
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWJsb2NrLWxpc3QubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL2NvbXBvbmVudHMvZGRyLWJsb2NrLWxpc3QvZGRyLWJsb2NrLWxpc3QubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLCtEQUErRCxDQUFDO0FBQ3RHLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLHFEQUFxRCxDQUFDO0FBQ2hHLE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLCtEQUErRCxDQUFDO0FBQzlHLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDOztBQWtCckQsTUFBTSxPQUFPLGtCQUFrQjs7K0dBQWxCLGtCQUFrQjtnSEFBbEIsa0JBQWtCLGlCQVQzQix5QkFBeUI7UUFDekIsNkJBQTZCO1FBQzdCLHFCQUFxQixhQVByQixZQUFZO1FBQ1oscUJBQXFCO1FBQ3JCLG1CQUFtQixhQVFuQixxQkFBcUI7UUFDckIsNkJBQTZCO2dIQUdwQixrQkFBa0IsWUFmcEI7WUFDUCxZQUFZO1lBQ1oscUJBQXFCO1lBQ3JCLG1CQUFtQjtTQUNwQjsyRkFXVSxrQkFBa0I7a0JBaEI5QixRQUFRO21CQUFDO29CQUNSLE9BQU8sRUFBRTt3QkFDUCxZQUFZO3dCQUNaLHFCQUFxQjt3QkFDckIsbUJBQW1CO3FCQUNwQjtvQkFDRCxZQUFZLEVBQUU7d0JBQ1oseUJBQXlCO3dCQUN6Qiw2QkFBNkI7d0JBQzdCLHFCQUFxQjtxQkFDdEI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLHFCQUFxQjt3QkFDckIsNkJBQTZCO3FCQUM5QjtpQkFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERkckNsaWNrT3V0c2lkZU1vZHVsZSB9IGZyb20gJy4vLi4vLi4vZGlyZWN0aXZlcy9kZHItY2xpY2stb3V0c2lkZS9kZHItY2xpY2stb3V0c2lkZS5tb2R1bGUnO1xuaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBEZHJCbG9ja0xpc3RJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9kZHItYmxvY2stbGlzdC1pdGVtL2Rkci1ibG9jay1saXN0LWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IERkckJsb2NrTGlzdERhdGFJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9kZHItYmxvY2stbGlzdC1kYXRhLWl0ZW0vZGRyLWJsb2NrLWxpc3QtZGF0YS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEZHJCbG9ja0xpc3RDb21wb25lbnQgfSBmcm9tICcuL2Rkci1ibG9jay1saXN0L2Rkci1ibG9jay1saXN0LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBOZ3hQYWdpbmF0aW9uTW9kdWxlIH0gZnJvbSAnbmd4LXBhZ2luYXRpb24nO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIERkckNsaWNrT3V0c2lkZU1vZHVsZSxcbiAgICBOZ3hQYWdpbmF0aW9uTW9kdWxlXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1xuICAgIERkckJsb2NrTGlzdEl0ZW1Db21wb25lbnQsXG4gICAgRGRyQmxvY2tMaXN0RGF0YUl0ZW1Db21wb25lbnQsXG4gICAgRGRyQmxvY2tMaXN0Q29tcG9uZW50XG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBEZHJCbG9ja0xpc3RDb21wb25lbnQsXG4gICAgRGRyQmxvY2tMaXN0RGF0YUl0ZW1Db21wb25lbnRcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBEZHJCbG9ja0xpc3RNb2R1bGUgeyB9XG4iXX0=