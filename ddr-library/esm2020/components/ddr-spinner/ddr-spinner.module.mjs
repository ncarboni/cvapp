import { DdrSpinnerService } from './ddr-spinner.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DdrSpinnerComponent } from './ddr-spinner.component';
import * as i0 from "@angular/core";
export class DdrSpinnerModule {
}
DdrSpinnerModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrSpinnerModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrSpinnerModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrSpinnerModule, declarations: [DdrSpinnerComponent], imports: [CommonModule], exports: [DdrSpinnerComponent] });
DdrSpinnerModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrSpinnerModule, providers: [
        DdrSpinnerService
    ], imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrSpinnerModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        DdrSpinnerComponent
                    ],
                    exports: [
                        DdrSpinnerComponent
                    ],
                    providers: [
                        DdrSpinnerService
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLXNwaW5uZXIubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL2NvbXBvbmVudHMvZGRyLXNwaW5uZXIvZGRyLXNwaW5uZXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzFELE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlCQUF5QixDQUFDOztBQWdCOUQsTUFBTSxPQUFPLGdCQUFnQjs7NkdBQWhCLGdCQUFnQjs4R0FBaEIsZ0JBQWdCLGlCQVR6QixtQkFBbUIsYUFIbkIsWUFBWSxhQU1aLG1CQUFtQjs4R0FNVixnQkFBZ0IsYUFKaEI7UUFDVCxpQkFBaUI7S0FDbEIsWUFYUTtZQUNQLFlBQVk7U0FDYjsyRkFXVSxnQkFBZ0I7a0JBZDVCLFFBQVE7bUJBQUM7b0JBQ1IsT0FBTyxFQUFFO3dCQUNQLFlBQVk7cUJBQ2I7b0JBQ0QsWUFBWSxFQUFFO3dCQUNaLG1CQUFtQjtxQkFDcEI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLG1CQUFtQjtxQkFDcEI7b0JBQ0QsU0FBUyxFQUFFO3dCQUNULGlCQUFpQjtxQkFDbEI7aUJBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEZHJTcGlubmVyU2VydmljZSB9IGZyb20gJy4vZGRyLXNwaW5uZXIuc2VydmljZSc7XG5pbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IERkclNwaW5uZXJDb21wb25lbnQgfSBmcm9tICcuL2Rkci1zcGlubmVyLmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGVcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgRGRyU3Bpbm5lckNvbXBvbmVudFxuICBdLFxuICBleHBvcnRzOiBbXG4gICAgRGRyU3Bpbm5lckNvbXBvbmVudFxuICBdLFxuICBwcm92aWRlcnM6IFtcbiAgICBEZHJTcGlubmVyU2VydmljZVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIERkclNwaW5uZXJNb2R1bGUgeyB9XG4iXX0=