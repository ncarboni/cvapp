import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class DdrSpinnerService {
    constructor() { }
    get show() {
        return this._show;
    }
    showSpinner() {
        this._show = true;
    }
    hideSpinner() {
        this._show = false;
    }
}
DdrSpinnerService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrSpinnerService, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
DdrSpinnerService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrSpinnerService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrSpinnerService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLXNwaW5uZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2Rkci1saWJyYXJ5L3NyYy9jb21wb25lbnRzL2Rkci1zcGlubmVyL2Rkci1zcGlubmVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFLM0MsTUFBTSxPQUFPLGlCQUFpQjtJQUk1QixnQkFBZ0IsQ0FBQztJQUVqQixJQUFXLElBQUk7UUFDYixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDcEIsQ0FBQztJQUVELFdBQVc7UUFDVCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztJQUNwQixDQUFDO0lBRUQsV0FBVztRQUNULElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO0lBQ3JCLENBQUM7OzhHQWhCVSxpQkFBaUI7a0hBQWpCLGlCQUFpQixjQUZoQixNQUFNOzJGQUVQLGlCQUFpQjtrQkFIN0IsVUFBVTttQkFBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIERkclNwaW5uZXJTZXJ2aWNlIHtcblxuICBwcml2YXRlIF9zaG93OiBib29sZWFuO1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgcHVibGljIGdldCBzaG93KCk6IGJvb2xlYW57XG4gICAgcmV0dXJuIHRoaXMuX3Nob3c7XG4gIH1cblxuICBzaG93U3Bpbm5lcigpe1xuICAgIHRoaXMuX3Nob3cgPSB0cnVlO1xuICB9IFxuXG4gIGhpZGVTcGlubmVyKCl7XG4gICAgdGhpcy5fc2hvdyA9IGZhbHNlO1xuICB9XG5cblxufVxuIl19