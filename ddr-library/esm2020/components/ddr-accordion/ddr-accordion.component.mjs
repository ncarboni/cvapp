import { Component, ViewEncapsulation, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { trigger, state, style, transition, animate, group } from '@angular/animations';
import * as i0 from "@angular/core";
import * as i1 from "./../../services/ddr-constants.service";
import * as i2 from "@angular/common";
export class DdrAccordionComponent {
    constructor(constants) {
        this.constants = constants;
        this.open = false;
        this.state = this.constants.DdrAccordionConstants.CLOSE;
        this.clickOpen = new EventEmitter();
    }
    ngAfterViewInit() {
        this.state = this.open ? this.constants.DdrAccordionConstants.OPEN : this.constants.DdrAccordionConstants.CLOSE;
        if (this.open) {
            this.contentAccordion.nativeElement.style.overflow = 'hidden';
            setTimeout(() => {
                this.contentAccordion.nativeElement.style.overflow = 'inherit';
            }, 400);
        }
    }
    openClose() {
        if (this.state === this.constants.DdrAccordionConstants.OPEN) {
            this.state = this.constants.DdrAccordionConstants.CLOSE;
        }
        else {
            this.state = this.constants.DdrAccordionConstants.OPEN;
        }
        this.contentAccordion.nativeElement.style.overflow = 'hidden';
        if (this.state == this.constants.DdrAccordionConstants.CLOSE) {
            setTimeout(() => {
                this.open = !this.open;
                this.clickOpen.emit(this.open);
            }, 400);
        }
        else {
            this.open = !this.open;
            this.clickOpen.emit(this.open);
            setTimeout(() => {
                this.contentAccordion.nativeElement.style.overflow = 'inherit';
            }, 400);
        }
    }
}
DdrAccordionComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrAccordionComponent, deps: [{ token: i1.DdrConstantsService }], target: i0.ɵɵFactoryTarget.Component });
DdrAccordionComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrAccordionComponent, selector: "ddr-accordion", inputs: { title: "title", open: "open" }, outputs: { clickOpen: "clickOpen" }, viewQueries: [{ propertyName: "contentAccordion", first: true, predicate: ["contentAccordion"], descendants: true }], ngImport: i0, template: "<div class=\"ddr-accordion\">\n\n  <div class=\"ddr-accordion__title\" \n    [ngClass]=\"{'ddr-accordion__title--is-opened': open,'ddr-accordion__title--is-closed': !open}\"\n    (click)=\"openClose()\">\n\n    <div class=\"row\">\n      <div class=\"col-md-11 col-10 text-left\">\n        <span class=\"ddr-accordion__title--text\">{{title}}</span>\n      </div>\n      <div class=\"col-md-1 col-2 text-right\">\n        <i class=\"fa fa-angle-down ddr-accordion__title--icon\" *ngIf=\"!open\"></i>\n        <i class=\"fa fa-angle-up ddr-accordion__title--icon\" *ngIf=\"open\"></i>\n      </div>\n    </div>\n\n  </div>\n\n  <div class=\"ddr-accordion__content\" #contentAccordion\n    [@slideInOut]=\"state\"\n    [ngClass]=\"{'ddr-accordion__content--is-opened': open, 'ddr-accordion__content--is-closed': !open }\" \n    style=\"height:0px\">\n    \n    <ng-content select=\"[content-accordion]\"></ng-content>\n\n  </div>\n\n</div>", styles: [".ddr-accordion{border-radius:4px;margin-bottom:10px;box-shadow:2px 2px 2px 1px #000000bf}.ddr-accordion__title{padding:5px;border-radius:4px;cursor:pointer}.ddr-accordion__title--text{font-size:24px}.ddr-accordion__title--icon{font-size:22px;margin-right:10px;margin-top:5px}.ddr-accordion__title--is-opened{border-bottom-left-radius:0;border-bottom-right-radius:0}.ddr-accordion__title--is-closed{border-bottom-left-radius:4px;border-bottom-right-radius:4px}.ddr-accordion__content--is-opened{padding:10px;border-top:none;border-radius:0 0 4px 4px/0px 0px 4px 4px;overflow:inherit;border-left-width:1px;border-left-style:solid;border-right-width:1px;border-right-style:solid;border-bottom-width:1px;border-bottom-style:solid}.ddr-accordion__content--is-closed{padding:0;border:none;overflow:hidden}\n"], directives: [{ type: i2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }], animations: [
        trigger('slideInOut', [
            state('open', style({ height: '*' })),
            state('close', style({ height: 0 })),
            transition('open <=> close', group([
                animate('400ms')
            ]))
        ])
    ], encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrAccordionComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-accordion', encapsulation: ViewEncapsulation.None, animations: [
                        trigger('slideInOut', [
                            state('open', style({ height: '*' })),
                            state('close', style({ height: 0 })),
                            transition('open <=> close', group([
                                animate('400ms')
                            ]))
                        ])
                    ], template: "<div class=\"ddr-accordion\">\n\n  <div class=\"ddr-accordion__title\" \n    [ngClass]=\"{'ddr-accordion__title--is-opened': open,'ddr-accordion__title--is-closed': !open}\"\n    (click)=\"openClose()\">\n\n    <div class=\"row\">\n      <div class=\"col-md-11 col-10 text-left\">\n        <span class=\"ddr-accordion__title--text\">{{title}}</span>\n      </div>\n      <div class=\"col-md-1 col-2 text-right\">\n        <i class=\"fa fa-angle-down ddr-accordion__title--icon\" *ngIf=\"!open\"></i>\n        <i class=\"fa fa-angle-up ddr-accordion__title--icon\" *ngIf=\"open\"></i>\n      </div>\n    </div>\n\n  </div>\n\n  <div class=\"ddr-accordion__content\" #contentAccordion\n    [@slideInOut]=\"state\"\n    [ngClass]=\"{'ddr-accordion__content--is-opened': open, 'ddr-accordion__content--is-closed': !open }\" \n    style=\"height:0px\">\n    \n    <ng-content select=\"[content-accordion]\"></ng-content>\n\n  </div>\n\n</div>", styles: [".ddr-accordion{border-radius:4px;margin-bottom:10px;box-shadow:2px 2px 2px 1px #000000bf}.ddr-accordion__title{padding:5px;border-radius:4px;cursor:pointer}.ddr-accordion__title--text{font-size:24px}.ddr-accordion__title--icon{font-size:22px;margin-right:10px;margin-top:5px}.ddr-accordion__title--is-opened{border-bottom-left-radius:0;border-bottom-right-radius:0}.ddr-accordion__title--is-closed{border-bottom-left-radius:4px;border-bottom-right-radius:4px}.ddr-accordion__content--is-opened{padding:10px;border-top:none;border-radius:0 0 4px 4px/0px 0px 4px 4px;overflow:inherit;border-left-width:1px;border-left-style:solid;border-right-width:1px;border-right-style:solid;border-bottom-width:1px;border-bottom-style:solid}.ddr-accordion__content--is-closed{padding:0;border:none;overflow:hidden}\n"] }]
        }], ctorParameters: function () { return [{ type: i1.DdrConstantsService }]; }, propDecorators: { title: [{
                type: Input
            }], open: [{
                type: Input
            }], clickOpen: [{
                type: Output
            }], contentAccordion: [{
                type: ViewChild,
                args: ["contentAccordion"]
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWFjY29yZGlvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9kZHItbGlicmFyeS9zcmMvY29tcG9uZW50cy9kZHItYWNjb3JkaW9uL2Rkci1hY2NvcmRpb24uY29tcG9uZW50LnRzIiwiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL2NvbXBvbmVudHMvZGRyLWFjY29yZGlvbi9kZHItYWNjb3JkaW9uLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFBRSxTQUFTLEVBQVUsaUJBQWlCLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUE2QixNQUFNLGVBQWUsQ0FBQztBQUN4SSxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBQyxLQUFLLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQzs7OztBQW1CdkYsTUFBTSxPQUFPLHFCQUFxQjtJQVdoQyxZQUNTLFNBQThCO1FBQTlCLGNBQVMsR0FBVCxTQUFTLENBQXFCO1FBVDlCLFNBQUksR0FBWSxLQUFLLENBQUM7UUFXN0IsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQztRQUN4RCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksWUFBWSxFQUFXLENBQUM7SUFDL0MsQ0FBQztJQUVELGVBQWU7UUFDYixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQztRQUNoSCxJQUFHLElBQUksQ0FBQyxJQUFJLEVBQUM7WUFDWCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1lBQzlELFVBQVUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQztZQUNqRSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7U0FDVDtJQUNILENBQUM7SUFFRCxTQUFTO1FBRVAsSUFBRyxJQUFJLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsSUFBSSxFQUFDO1lBQzFELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUM7U0FDekQ7YUFBSTtZQUNILElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUM7U0FDeEQ7UUFFRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBQzlELElBQUcsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLEtBQUssRUFBQztZQUMxRCxVQUFVLENBQUMsR0FBRyxFQUFFO2dCQUNaLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUN2QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbkMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ1Q7YUFBSTtZQUNILElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMvQixVQUFVLENBQUMsR0FBRyxFQUFFO2dCQUNkLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUM7WUFDakUsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ1Q7SUFFSCxDQUFDOztrSEFsRFUscUJBQXFCO3NHQUFyQixxQkFBcUIsMFBDckJsQywyNkJBMkJNLHUrQkRsQlE7UUFDVixPQUFPLENBQUMsWUFBWSxFQUFHO1lBQ3JCLEtBQUssQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLEVBQUMsTUFBTSxFQUFFLEdBQUcsRUFBQyxDQUFDLENBQUM7WUFDbkMsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsRUFBQyxNQUFNLEVBQUUsQ0FBQyxFQUFDLENBQUMsQ0FBQztZQUNsQyxVQUFVLENBQUMsZ0JBQWdCLEVBQUUsS0FBSyxDQUNoQztnQkFDRSxPQUFPLENBQUMsT0FBTyxDQUFDO2FBQ2pCLENBQ0YsQ0FBQztTQUNILENBQUM7S0FDSDsyRkFFVSxxQkFBcUI7a0JBakJqQyxTQUFTOytCQUNFLGVBQWUsaUJBR1YsaUJBQWlCLENBQUMsSUFBSSxjQUN6Qjt3QkFDVixPQUFPLENBQUMsWUFBWSxFQUFHOzRCQUNyQixLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxFQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUMsQ0FBQyxDQUFDOzRCQUNuQyxLQUFLLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxFQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUMsQ0FBQyxDQUFDOzRCQUNsQyxVQUFVLENBQUMsZ0JBQWdCLEVBQUUsS0FBSyxDQUNoQztnQ0FDRSxPQUFPLENBQUMsT0FBTyxDQUFDOzZCQUNqQixDQUNGLENBQUM7eUJBQ0gsQ0FBQztxQkFDSDswR0FJUSxLQUFLO3NCQUFiLEtBQUs7Z0JBQ0csSUFBSTtzQkFBWixLQUFLO2dCQUVJLFNBQVM7c0JBQWxCLE1BQU07Z0JBRXdCLGdCQUFnQjtzQkFBOUMsU0FBUzt1QkFBQyxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEZHJDb25zdGFudHNTZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi9zZXJ2aWNlcy9kZHItY29uc3RhbnRzLnNlcnZpY2UnO1xuaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFZpZXdFbmNhcHN1bGF0aW9uLCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIFZpZXdDaGlsZCwgRWxlbWVudFJlZiwgQWZ0ZXJWaWV3SW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgdHJpZ2dlciwgc3RhdGUsIHN0eWxlLCB0cmFuc2l0aW9uLCBhbmltYXRlLGdyb3VwIH0gZnJvbSAnQGFuZ3VsYXIvYW5pbWF0aW9ucyc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2Rkci1hY2NvcmRpb24nLFxuICB0ZW1wbGF0ZVVybDogJy4vZGRyLWFjY29yZGlvbi5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2Rkci1hY2NvcmRpb24uY29tcG9uZW50LnNjc3MnXSxcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcbiAgYW5pbWF0aW9uczogW1xuICAgIHRyaWdnZXIoJ3NsaWRlSW5PdXQnICwgW1xuICAgICAgc3RhdGUoJ29wZW4nLCBzdHlsZSh7aGVpZ2h0OiAnKid9KSksXG4gICAgICBzdGF0ZSgnY2xvc2UnLCBzdHlsZSh7aGVpZ2h0OiAwfSkpLFxuICAgICAgdHJhbnNpdGlvbignb3BlbiA8PT4gY2xvc2UnLCBncm91cChcbiAgICAgICAgW1xuICAgICAgICAgIGFuaW1hdGUoJzQwMG1zJylcbiAgICAgICAgXVxuICAgICAgKSlcbiAgICBdKVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIERkckFjY29yZGlvbkNvbXBvbmVudCBpbXBsZW1lbnRzIEFmdGVyVmlld0luaXQge1xuXG4gIEBJbnB1dCgpIHRpdGxlOiBzdHJpbmc7XG4gIEBJbnB1dCgpIG9wZW46IGJvb2xlYW4gPSBmYWxzZTtcblxuICBAT3V0cHV0KCkgY2xpY2tPcGVuOiBFdmVudEVtaXR0ZXI8Ym9vbGVhbj47XG5cbiAgQFZpZXdDaGlsZChcImNvbnRlbnRBY2NvcmRpb25cIikgY29udGVudEFjY29yZGlvbjogRWxlbWVudFJlZjtcblxuICBwdWJsaWMgc3RhdGU6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwdWJsaWMgY29uc3RhbnRzOiBEZHJDb25zdGFudHNTZXJ2aWNlXG4gICkgeyBcbiAgICB0aGlzLnN0YXRlID0gdGhpcy5jb25zdGFudHMuRGRyQWNjb3JkaW9uQ29uc3RhbnRzLkNMT1NFO1xuICAgIHRoaXMuY2xpY2tPcGVuID0gbmV3IEV2ZW50RW1pdHRlcjxib29sZWFuPigpO1xuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCkge1xuICAgIHRoaXMuc3RhdGUgPSB0aGlzLm9wZW4gPyB0aGlzLmNvbnN0YW50cy5EZHJBY2NvcmRpb25Db25zdGFudHMuT1BFTiA6IHRoaXMuY29uc3RhbnRzLkRkckFjY29yZGlvbkNvbnN0YW50cy5DTE9TRTtcbiAgICBpZih0aGlzLm9wZW4pe1xuICAgICAgdGhpcy5jb250ZW50QWNjb3JkaW9uLm5hdGl2ZUVsZW1lbnQuc3R5bGUub3ZlcmZsb3cgPSAnaGlkZGVuJztcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICB0aGlzLmNvbnRlbnRBY2NvcmRpb24ubmF0aXZlRWxlbWVudC5zdHlsZS5vdmVyZmxvdyA9ICdpbmhlcml0JztcbiAgICAgIH0sIDQwMCk7XG4gICAgfVxuICB9XG5cbiAgb3BlbkNsb3NlKCl7XG5cbiAgICBpZih0aGlzLnN0YXRlID09PSB0aGlzLmNvbnN0YW50cy5EZHJBY2NvcmRpb25Db25zdGFudHMuT1BFTil7XG4gICAgICB0aGlzLnN0YXRlID0gdGhpcy5jb25zdGFudHMuRGRyQWNjb3JkaW9uQ29uc3RhbnRzLkNMT1NFO1xuICAgIH1lbHNle1xuICAgICAgdGhpcy5zdGF0ZSA9IHRoaXMuY29uc3RhbnRzLkRkckFjY29yZGlvbkNvbnN0YW50cy5PUEVOO1xuICAgIH1cblxuICAgIHRoaXMuY29udGVudEFjY29yZGlvbi5uYXRpdmVFbGVtZW50LnN0eWxlLm92ZXJmbG93ID0gJ2hpZGRlbic7XG4gICAgaWYodGhpcy5zdGF0ZSA9PSB0aGlzLmNvbnN0YW50cy5EZHJBY2NvcmRpb25Db25zdGFudHMuQ0xPU0Upe1xuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgdGhpcy5vcGVuID0gIXRoaXMub3BlbjtcbiAgICAgICAgICB0aGlzLmNsaWNrT3Blbi5lbWl0KHRoaXMub3Blbik7XG4gICAgICB9LCA0MDApO1xuICAgIH1lbHNle1xuICAgICAgdGhpcy5vcGVuID0gIXRoaXMub3BlbjtcbiAgICAgIHRoaXMuY2xpY2tPcGVuLmVtaXQodGhpcy5vcGVuKTtcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICB0aGlzLmNvbnRlbnRBY2NvcmRpb24ubmF0aXZlRWxlbWVudC5zdHlsZS5vdmVyZmxvdyA9ICdpbmhlcml0JztcbiAgICAgIH0sIDQwMCk7XG4gICAgfVxuXG4gIH1cblxufVxuIiwiPGRpdiBjbGFzcz1cImRkci1hY2NvcmRpb25cIj5cblxuICA8ZGl2IGNsYXNzPVwiZGRyLWFjY29yZGlvbl9fdGl0bGVcIiBcbiAgICBbbmdDbGFzc109XCJ7J2Rkci1hY2NvcmRpb25fX3RpdGxlLS1pcy1vcGVuZWQnOiBvcGVuLCdkZHItYWNjb3JkaW9uX190aXRsZS0taXMtY2xvc2VkJzogIW9wZW59XCJcbiAgICAoY2xpY2spPVwib3BlbkNsb3NlKClcIj5cblxuICAgIDxkaXYgY2xhc3M9XCJyb3dcIj5cbiAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMTEgY29sLTEwIHRleHQtbGVmdFwiPlxuICAgICAgICA8c3BhbiBjbGFzcz1cImRkci1hY2NvcmRpb25fX3RpdGxlLS10ZXh0XCI+e3t0aXRsZX19PC9zcGFuPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTEgY29sLTIgdGV4dC1yaWdodFwiPlxuICAgICAgICA8aSBjbGFzcz1cImZhIGZhLWFuZ2xlLWRvd24gZGRyLWFjY29yZGlvbl9fdGl0bGUtLWljb25cIiAqbmdJZj1cIiFvcGVuXCI+PC9pPlxuICAgICAgICA8aSBjbGFzcz1cImZhIGZhLWFuZ2xlLXVwIGRkci1hY2NvcmRpb25fX3RpdGxlLS1pY29uXCIgKm5nSWY9XCJvcGVuXCI+PC9pPlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG5cbiAgPC9kaXY+XG5cbiAgPGRpdiBjbGFzcz1cImRkci1hY2NvcmRpb25fX2NvbnRlbnRcIiAjY29udGVudEFjY29yZGlvblxuICAgIFtAc2xpZGVJbk91dF09XCJzdGF0ZVwiXG4gICAgW25nQ2xhc3NdPVwieydkZHItYWNjb3JkaW9uX19jb250ZW50LS1pcy1vcGVuZWQnOiBvcGVuLCAnZGRyLWFjY29yZGlvbl9fY29udGVudC0taXMtY2xvc2VkJzogIW9wZW4gfVwiIFxuICAgIHN0eWxlPVwiaGVpZ2h0OjBweFwiPlxuICAgIFxuICAgIDxuZy1jb250ZW50IHNlbGVjdD1cIltjb250ZW50LWFjY29yZGlvbl1cIj48L25nLWNvbnRlbnQ+XG5cbiAgPC9kaXY+XG5cbjwvZGl2PiJdfQ==