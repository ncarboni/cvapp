import { DdrTabItemComponent } from './ddr-tab-item/ddr-tab-item.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DdrTabsComponent } from './ddr-tabs.component';
import * as i0 from "@angular/core";
export class DdrTabsModule {
}
DdrTabsModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTabsModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrTabsModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTabsModule, declarations: [DdrTabsComponent,
        DdrTabItemComponent], imports: [CommonModule], exports: [DdrTabsComponent,
        DdrTabItemComponent] });
DdrTabsModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTabsModule, imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTabsModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        DdrTabsComponent,
                        DdrTabItemComponent
                    ],
                    exports: [
                        DdrTabsComponent,
                        DdrTabItemComponent
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLXRhYnMubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL2NvbXBvbmVudHMvZGRyLXRhYnMvZGRyLXRhYnMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDOztBQWV4RCxNQUFNLE9BQU8sYUFBYTs7MEdBQWIsYUFBYTsyR0FBYixhQUFhLGlCQVJ0QixnQkFBZ0I7UUFDaEIsbUJBQW1CLGFBSm5CLFlBQVksYUFPWixnQkFBZ0I7UUFDaEIsbUJBQW1COzJHQUdWLGFBQWEsWUFaZjtZQUNQLFlBQVk7U0FDYjsyRkFVVSxhQUFhO2tCQWJ6QixRQUFRO21CQUFDO29CQUNSLE9BQU8sRUFBRTt3QkFDUCxZQUFZO3FCQUNiO29CQUNELFlBQVksRUFBRTt3QkFDWixnQkFBZ0I7d0JBQ2hCLG1CQUFtQjtxQkFDcEI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLGdCQUFnQjt3QkFDaEIsbUJBQW1CO3FCQUNwQjtpQkFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERkclRhYkl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2Rkci10YWItaXRlbS9kZHItdGFiLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgRGRyVGFic0NvbXBvbmVudCB9IGZyb20gJy4vZGRyLXRhYnMuY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZVxuICBdLFxuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBEZHJUYWJzQ29tcG9uZW50LFxuICAgIERkclRhYkl0ZW1Db21wb25lbnRcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIERkclRhYnNDb21wb25lbnQsXG4gICAgRGRyVGFiSXRlbUNvbXBvbmVudFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIERkclRhYnNNb2R1bGUgeyB9XG4iXX0=