import { Component, Input, ViewChild, ViewEncapsulation } from '@angular/core';
import * as i0 from "@angular/core";
export class DdrTabItemComponent {
    constructor() {
        this.selected = false;
    }
}
DdrTabItemComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTabItemComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
DdrTabItemComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.0.2", type: DdrTabItemComponent, selector: "ddr-tab-item", inputs: { title: "title" }, viewQueries: [{ propertyName: "contentTemplate", first: true, predicate: ["contentTemplate"], descendants: true }], ngImport: i0, template: "<ng-template #contentTemplate>\n  <ng-content></ng-content>\n</ng-template>", styles: [""], encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTabItemComponent, decorators: [{
            type: Component,
            args: [{ selector: 'ddr-tab-item', encapsulation: ViewEncapsulation.None, template: "<ng-template #contentTemplate>\n  <ng-content></ng-content>\n</ng-template>", styles: [""] }]
        }], ctorParameters: function () { return []; }, propDecorators: { title: [{
                type: Input
            }], contentTemplate: [{
                type: ViewChild,
                args: ['contentTemplate', { static: false }]
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLXRhYi1pdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2Rkci1saWJyYXJ5L3NyYy9jb21wb25lbnRzL2Rkci10YWJzL2Rkci10YWItaXRlbS9kZHItdGFiLWl0ZW0uY29tcG9uZW50LnRzIiwiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL2NvbXBvbmVudHMvZGRyLXRhYnMvZGRyLXRhYi1pdGVtL2Rkci10YWItaXRlbS5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBVSxTQUFTLEVBQUUsaUJBQWlCLEVBQWUsTUFBTSxlQUFlLENBQUM7O0FBUXBHLE1BQU0sT0FBTyxtQkFBbUI7SUFNOUI7UUFDRSxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDOztnSEFSVSxtQkFBbUI7b0dBQW5CLG1CQUFtQixvTUNSaEMsNkVBRWM7MkZETUQsbUJBQW1CO2tCQU4vQixTQUFTOytCQUNFLGNBQWMsaUJBR1QsaUJBQWlCLENBQUMsSUFBSTswRUFJNUIsS0FBSztzQkFBYixLQUFLO2dCQUN5QyxlQUFlO3NCQUE3RCxTQUFTO3VCQUFDLGlCQUFpQixFQUFFLEVBQUMsTUFBTSxFQUFFLEtBQUssRUFBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCwgVmlld0NoaWxkLCBWaWV3RW5jYXBzdWxhdGlvbiwgVGVtcGxhdGVSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZGRyLXRhYi1pdGVtJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2Rkci10YWItaXRlbS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2Rkci10YWItaXRlbS5jb21wb25lbnQuc2NzcyddLFxuICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXG59KVxuZXhwb3J0IGNsYXNzIERkclRhYkl0ZW1Db21wb25lbnQge1xuXG4gIEBJbnB1dCgpIHRpdGxlOiBzdHJpbmc7XG4gIEBWaWV3Q2hpbGQoJ2NvbnRlbnRUZW1wbGF0ZScsIHtzdGF0aWM6IGZhbHNlfSkgY29udGVudFRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xuICBwdWJsaWMgc2VsZWN0ZWQ6IGJvb2xlYW47XG5cbiAgY29uc3RydWN0b3IoKSB7IFxuICAgIHRoaXMuc2VsZWN0ZWQgPSBmYWxzZTtcbiAgfVxuXG59XG4iLCI8bmctdGVtcGxhdGUgI2NvbnRlbnRUZW1wbGF0ZT5cbiAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxuPC9uZy10ZW1wbGF0ZT4iXX0=