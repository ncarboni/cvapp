import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DdrJoinPipe } from './ddr-join.pipe';
import * as i0 from "@angular/core";
export class DdrJoinPipeModule {
}
DdrJoinPipeModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrJoinPipeModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrJoinPipeModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrJoinPipeModule, declarations: [DdrJoinPipe], imports: [CommonModule], exports: [DdrJoinPipe] });
DdrJoinPipeModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrJoinPipeModule, imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrJoinPipeModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        DdrJoinPipe
                    ],
                    exports: [
                        DdrJoinPipe
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWpvaW4tcGlwZS5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9kZHItbGlicmFyeS9zcmMvcGlwZXMvZGRyLWpvaW4tcGlwZS9kZHItam9pbi1waXBlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0saUJBQWlCLENBQUM7O0FBYTlDLE1BQU0sT0FBTyxpQkFBaUI7OzhHQUFqQixpQkFBaUI7K0dBQWpCLGlCQUFpQixpQkFOMUIsV0FBVyxhQUhYLFlBQVksYUFNWixXQUFXOytHQUdGLGlCQUFpQixZQVZuQjtZQUNQLFlBQVk7U0FDYjsyRkFRVSxpQkFBaUI7a0JBWDdCLFFBQVE7bUJBQUM7b0JBQ1IsT0FBTyxFQUFFO3dCQUNQLFlBQVk7cUJBQ2I7b0JBQ0QsWUFBWSxFQUFFO3dCQUNaLFdBQVc7cUJBQ1o7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLFdBQVc7cUJBQ1o7aUJBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IERkckpvaW5QaXBlIH0gZnJvbSAnLi9kZHItam9pbi5waXBlJztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZVxuICBdLFxuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBEZHJKb2luUGlwZVxuICBdLFxuICBleHBvcnRzOiBbXG4gICAgRGRySm9pblBpcGVcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBEZHJKb2luUGlwZU1vZHVsZSB7IH1cbiJdfQ==