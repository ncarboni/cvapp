import { Pipe } from '@angular/core';
import * as i0 from "@angular/core";
export class DdrJoinPipe {
    transform(value, separator = ',') {
        if (!value) {
            return '';
        }
        return value.join(separator);
    }
}
DdrJoinPipe.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrJoinPipe, deps: [], target: i0.ɵɵFactoryTarget.Pipe });
DdrJoinPipe.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrJoinPipe, name: "join" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrJoinPipe, decorators: [{
            type: Pipe,
            args: [{
                    name: 'join'
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWpvaW4ucGlwZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2Rkci1saWJyYXJ5L3NyYy9waXBlcy9kZHItam9pbi1waXBlL2Rkci1qb2luLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7O0FBS3BELE1BQU0sT0FBTyxXQUFXO0lBRXRCLFNBQVMsQ0FBQyxLQUFlLEVBQUUsWUFBb0IsR0FBRztRQUNoRCxJQUFHLENBQUMsS0FBSyxFQUFDO1lBQ1IsT0FBTyxFQUFFLENBQUM7U0FDWDtRQUNELE9BQU8sS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUMvQixDQUFDOzt3R0FQVSxXQUFXO3NHQUFYLFdBQVc7MkZBQVgsV0FBVztrQkFIdkIsSUFBSTttQkFBQztvQkFDSixJQUFJLEVBQUUsTUFBTTtpQkFDYiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe1xuICBuYW1lOiAnam9pbidcbn0pXG5leHBvcnQgY2xhc3MgRGRySm9pblBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcblxuICB0cmFuc2Zvcm0odmFsdWU6IHN0cmluZ1tdLCBzZXBhcmF0b3I6IHN0cmluZyA9ICcsJyk6IGFueSB7XG4gICAgaWYoIXZhbHVlKXtcbiAgICAgIHJldHVybiAnJztcbiAgICB9XG4gICAgcmV0dXJuIHZhbHVlLmpvaW4oc2VwYXJhdG9yKTtcbiAgfVxuXG59XG4iXX0=