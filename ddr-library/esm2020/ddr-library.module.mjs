import { DdrThemeService } from './services/ddr-theme.service';
import { DdrModalModule } from './components/ddr-modal/ddr-modal.module';
import { DdrToggleModule } from './components/ddr-toggle/ddr-toggle.module';
import { DdrNgModelBaseModule } from './components/ddr-ng-model-base/ddr-ng-model-base.module';
import { DdrTranslateModule } from './services/ddr-translate/ddr-translate.module';
import { DdrResolutionModule } from './services/ddr-resolution/ddr-resolution.module';
import { DdrDetailModule } from './components/ddr-detail/ddr-detail.module';
import { DdrConfigModule } from './services/ddr-config/ddr-config.module';
import { DdrLoadIframeModule } from './directives/ddr-load-iframe/ddr-load-iframe.module';
import { DdrSpinnerModule } from './components/ddr-spinner/ddr-spinner.module';
import { DdrJoinPipeModule } from './pipes/ddr-join-pipe/ddr-join-pipe.module';
import { NgModule } from '@angular/core';
import { DdrToastModule } from './components/ddr-toast/ddr-toast.module';
import { DdrConstantsService } from './services/ddr-constants.service';
import { DdrClickOutsideModule } from './directives/ddr-click-outside/ddr-click-outside.module';
import { DdrDropdownModule } from './components/ddr-dropdown/ddr-dropdown.module';
import { DdrBlockListModule } from './components/ddr-block-list/ddr-block-list.module';
import { DdrTabsModule } from './components/ddr-tabs/ddr-tabs.module';
import { DdrAccordionModule } from './components/ddr-accordion/ddr-accordion.module';
import * as i0 from "@angular/core";
export class DdrLibraryModule {
}
DdrLibraryModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLibraryModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrLibraryModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLibraryModule, imports: [DdrJoinPipeModule,
        DdrSpinnerModule,
        DdrToastModule,
        DdrClickOutsideModule,
        DdrDropdownModule,
        DdrLoadIframeModule,
        DdrConfigModule,
        DdrDetailModule,
        DdrBlockListModule,
        DdrTabsModule,
        DdrResolutionModule,
        DdrAccordionModule,
        DdrTranslateModule,
        DdrNgModelBaseModule,
        DdrToggleModule,
        DdrModalModule], exports: [DdrJoinPipeModule,
        DdrSpinnerModule,
        DdrToastModule,
        DdrClickOutsideModule,
        DdrDropdownModule,
        DdrLoadIframeModule,
        DdrConfigModule,
        DdrDetailModule,
        DdrBlockListModule,
        DdrTabsModule,
        DdrResolutionModule,
        DdrAccordionModule,
        DdrTranslateModule,
        DdrNgModelBaseModule,
        DdrToggleModule,
        DdrModalModule] });
DdrLibraryModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLibraryModule, providers: [
        DdrConstantsService,
        DdrThemeService
    ], imports: [[
            DdrJoinPipeModule,
            DdrSpinnerModule,
            DdrToastModule,
            DdrClickOutsideModule,
            DdrDropdownModule,
            DdrLoadIframeModule,
            DdrConfigModule,
            DdrDetailModule,
            DdrBlockListModule,
            DdrTabsModule,
            DdrResolutionModule,
            DdrAccordionModule,
            DdrTranslateModule,
            DdrNgModelBaseModule,
            DdrToggleModule,
            DdrModalModule
        ], DdrJoinPipeModule,
        DdrSpinnerModule,
        DdrToastModule,
        DdrClickOutsideModule,
        DdrDropdownModule,
        DdrLoadIframeModule,
        DdrConfigModule,
        DdrDetailModule,
        DdrBlockListModule,
        DdrTabsModule,
        DdrResolutionModule,
        DdrAccordionModule,
        DdrTranslateModule,
        DdrNgModelBaseModule,
        DdrToggleModule,
        DdrModalModule] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrLibraryModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [],
                    imports: [
                        DdrJoinPipeModule,
                        DdrSpinnerModule,
                        DdrToastModule,
                        DdrClickOutsideModule,
                        DdrDropdownModule,
                        DdrLoadIframeModule,
                        DdrConfigModule,
                        DdrDetailModule,
                        DdrBlockListModule,
                        DdrTabsModule,
                        DdrResolutionModule,
                        DdrAccordionModule,
                        DdrTranslateModule,
                        DdrNgModelBaseModule,
                        DdrToggleModule,
                        DdrModalModule
                    ],
                    exports: [
                        DdrJoinPipeModule,
                        DdrSpinnerModule,
                        DdrToastModule,
                        DdrClickOutsideModule,
                        DdrDropdownModule,
                        DdrLoadIframeModule,
                        DdrConfigModule,
                        DdrDetailModule,
                        DdrBlockListModule,
                        DdrTabsModule,
                        DdrResolutionModule,
                        DdrAccordionModule,
                        DdrTranslateModule,
                        DdrNgModelBaseModule,
                        DdrToggleModule,
                        DdrModalModule
                    ],
                    providers: [
                        DdrConstantsService,
                        DdrThemeService
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWxpYnJhcnkubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL2Rkci1saWJyYXJ5Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDL0QsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ3pFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUM1RSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx5REFBeUQsQ0FBQztBQUMvRixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUNuRixPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxpREFBaUQsQ0FBQztBQUN0RixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFDNUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHFEQUFxRCxDQUFDO0FBQzFGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ3pFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlEQUF5RCxDQUFDO0FBQ2hHLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG1EQUFtRCxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUN0RSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxpREFBaUQsQ0FBQzs7QUE4Q3JGLE1BQU0sT0FBTyxnQkFBZ0I7OzZHQUFoQixnQkFBZ0I7OEdBQWhCLGdCQUFnQixZQXhDekIsaUJBQWlCO1FBQ2pCLGdCQUFnQjtRQUNoQixjQUFjO1FBQ2QscUJBQXFCO1FBQ3JCLGlCQUFpQjtRQUNqQixtQkFBbUI7UUFDbkIsZUFBZTtRQUNmLGVBQWU7UUFDZixrQkFBa0I7UUFDbEIsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQixrQkFBa0I7UUFDbEIsa0JBQWtCO1FBQ2xCLG9CQUFvQjtRQUNwQixlQUFlO1FBQ2YsY0FBYyxhQUdkLGlCQUFpQjtRQUNqQixnQkFBZ0I7UUFDaEIsY0FBYztRQUNkLHFCQUFxQjtRQUNyQixpQkFBaUI7UUFDakIsbUJBQW1CO1FBQ25CLGVBQWU7UUFDZixlQUFlO1FBQ2Ysa0JBQWtCO1FBQ2xCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsa0JBQWtCO1FBQ2xCLGtCQUFrQjtRQUNsQixvQkFBb0I7UUFDcEIsZUFBZTtRQUNmLGNBQWM7OEdBT0wsZ0JBQWdCLGFBTGhCO1FBQ1QsbUJBQW1CO1FBQ25CLGVBQWU7S0FDaEIsWUF2Q1E7WUFDUCxpQkFBaUI7WUFDakIsZ0JBQWdCO1lBQ2hCLGNBQWM7WUFDZCxxQkFBcUI7WUFDckIsaUJBQWlCO1lBQ2pCLG1CQUFtQjtZQUNuQixlQUFlO1lBQ2YsZUFBZTtZQUNmLGtCQUFrQjtZQUNsQixhQUFhO1lBQ2IsbUJBQW1CO1lBQ25CLGtCQUFrQjtZQUNsQixrQkFBa0I7WUFDbEIsb0JBQW9CO1lBQ3BCLGVBQWU7WUFDZixjQUFjO1NBQ2YsRUFFQyxpQkFBaUI7UUFDakIsZ0JBQWdCO1FBQ2hCLGNBQWM7UUFDZCxxQkFBcUI7UUFDckIsaUJBQWlCO1FBQ2pCLG1CQUFtQjtRQUNuQixlQUFlO1FBQ2YsZUFBZTtRQUNmLGtCQUFrQjtRQUNsQixhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLGtCQUFrQjtRQUNsQixrQkFBa0I7UUFDbEIsb0JBQW9CO1FBQ3BCLGVBQWU7UUFDZixjQUFjOzJGQU9MLGdCQUFnQjtrQkE1QzVCLFFBQVE7bUJBQUM7b0JBQ1IsWUFBWSxFQUFFLEVBQ2I7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLGlCQUFpQjt3QkFDakIsZ0JBQWdCO3dCQUNoQixjQUFjO3dCQUNkLHFCQUFxQjt3QkFDckIsaUJBQWlCO3dCQUNqQixtQkFBbUI7d0JBQ25CLGVBQWU7d0JBQ2YsZUFBZTt3QkFDZixrQkFBa0I7d0JBQ2xCLGFBQWE7d0JBQ2IsbUJBQW1CO3dCQUNuQixrQkFBa0I7d0JBQ2xCLGtCQUFrQjt3QkFDbEIsb0JBQW9CO3dCQUNwQixlQUFlO3dCQUNmLGNBQWM7cUJBQ2Y7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLGlCQUFpQjt3QkFDakIsZ0JBQWdCO3dCQUNoQixjQUFjO3dCQUNkLHFCQUFxQjt3QkFDckIsaUJBQWlCO3dCQUNqQixtQkFBbUI7d0JBQ25CLGVBQWU7d0JBQ2YsZUFBZTt3QkFDZixrQkFBa0I7d0JBQ2xCLGFBQWE7d0JBQ2IsbUJBQW1CO3dCQUNuQixrQkFBa0I7d0JBQ2xCLGtCQUFrQjt3QkFDbEIsb0JBQW9CO3dCQUNwQixlQUFlO3dCQUNmLGNBQWM7cUJBQ2Y7b0JBQ0QsU0FBUyxFQUFFO3dCQUNULG1CQUFtQjt3QkFDbkIsZUFBZTtxQkFDaEI7aUJBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEZHJUaGVtZVNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2Rkci10aGVtZS5zZXJ2aWNlJztcbmltcG9ydCB7IERkck1vZGFsTW9kdWxlIH0gZnJvbSAnLi9jb21wb25lbnRzL2Rkci1tb2RhbC9kZHItbW9kYWwubW9kdWxlJztcbmltcG9ydCB7IERkclRvZ2dsZU1vZHVsZSB9IGZyb20gJy4vY29tcG9uZW50cy9kZHItdG9nZ2xlL2Rkci10b2dnbGUubW9kdWxlJztcbmltcG9ydCB7IERkck5nTW9kZWxCYXNlTW9kdWxlIH0gZnJvbSAnLi9jb21wb25lbnRzL2Rkci1uZy1tb2RlbC1iYXNlL2Rkci1uZy1tb2RlbC1iYXNlLm1vZHVsZSc7XG5pbXBvcnQgeyBEZHJUcmFuc2xhdGVNb2R1bGUgfSBmcm9tICcuL3NlcnZpY2VzL2Rkci10cmFuc2xhdGUvZGRyLXRyYW5zbGF0ZS5tb2R1bGUnO1xuaW1wb3J0IHsgRGRyUmVzb2x1dGlvbk1vZHVsZSB9IGZyb20gJy4vc2VydmljZXMvZGRyLXJlc29sdXRpb24vZGRyLXJlc29sdXRpb24ubW9kdWxlJztcbmltcG9ydCB7IERkckRldGFpbE1vZHVsZSB9IGZyb20gJy4vY29tcG9uZW50cy9kZHItZGV0YWlsL2Rkci1kZXRhaWwubW9kdWxlJztcbmltcG9ydCB7IERkckNvbmZpZ01vZHVsZSB9IGZyb20gJy4vc2VydmljZXMvZGRyLWNvbmZpZy9kZHItY29uZmlnLm1vZHVsZSc7XG5pbXBvcnQgeyBEZHJMb2FkSWZyYW1lTW9kdWxlIH0gZnJvbSAnLi9kaXJlY3RpdmVzL2Rkci1sb2FkLWlmcmFtZS9kZHItbG9hZC1pZnJhbWUubW9kdWxlJztcbmltcG9ydCB7IERkclNwaW5uZXJNb2R1bGUgfSBmcm9tICcuL2NvbXBvbmVudHMvZGRyLXNwaW5uZXIvZGRyLXNwaW5uZXIubW9kdWxlJztcbmltcG9ydCB7IERkckpvaW5QaXBlTW9kdWxlIH0gZnJvbSAnLi9waXBlcy9kZHItam9pbi1waXBlL2Rkci1qb2luLXBpcGUubW9kdWxlJztcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBEZHJUb2FzdE1vZHVsZSB9IGZyb20gJy4vY29tcG9uZW50cy9kZHItdG9hc3QvZGRyLXRvYXN0Lm1vZHVsZSc7XG5pbXBvcnQgeyBEZHJDb25zdGFudHNTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9kZHItY29uc3RhbnRzLnNlcnZpY2UnO1xuaW1wb3J0IHsgRGRyQ2xpY2tPdXRzaWRlTW9kdWxlIH0gZnJvbSAnLi9kaXJlY3RpdmVzL2Rkci1jbGljay1vdXRzaWRlL2Rkci1jbGljay1vdXRzaWRlLm1vZHVsZSc7XG5pbXBvcnQgeyBEZHJEcm9wZG93bk1vZHVsZSB9IGZyb20gJy4vY29tcG9uZW50cy9kZHItZHJvcGRvd24vZGRyLWRyb3Bkb3duLm1vZHVsZSc7XG5pbXBvcnQgeyBEZHJCbG9ja0xpc3RNb2R1bGUgfSBmcm9tICcuL2NvbXBvbmVudHMvZGRyLWJsb2NrLWxpc3QvZGRyLWJsb2NrLWxpc3QubW9kdWxlJztcbmltcG9ydCB7IERkclRhYnNNb2R1bGUgfSBmcm9tICcuL2NvbXBvbmVudHMvZGRyLXRhYnMvZGRyLXRhYnMubW9kdWxlJztcbmltcG9ydCB7IERkckFjY29yZGlvbk1vZHVsZSB9IGZyb20gJy4vY29tcG9uZW50cy9kZHItYWNjb3JkaW9uL2Rkci1hY2NvcmRpb24ubW9kdWxlJztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbXG4gIF0sXG4gIGltcG9ydHM6IFtcbiAgICBEZHJKb2luUGlwZU1vZHVsZSxcbiAgICBEZHJTcGlubmVyTW9kdWxlLFxuICAgIERkclRvYXN0TW9kdWxlLFxuICAgIERkckNsaWNrT3V0c2lkZU1vZHVsZSxcbiAgICBEZHJEcm9wZG93bk1vZHVsZSxcbiAgICBEZHJMb2FkSWZyYW1lTW9kdWxlLFxuICAgIERkckNvbmZpZ01vZHVsZSxcbiAgICBEZHJEZXRhaWxNb2R1bGUsXG4gICAgRGRyQmxvY2tMaXN0TW9kdWxlLFxuICAgIERkclRhYnNNb2R1bGUsXG4gICAgRGRyUmVzb2x1dGlvbk1vZHVsZSxcbiAgICBEZHJBY2NvcmRpb25Nb2R1bGUsXG4gICAgRGRyVHJhbnNsYXRlTW9kdWxlLFxuICAgIERkck5nTW9kZWxCYXNlTW9kdWxlLFxuICAgIERkclRvZ2dsZU1vZHVsZSxcbiAgICBEZHJNb2RhbE1vZHVsZVxuICBdLFxuICBleHBvcnRzOiBbXG4gICAgRGRySm9pblBpcGVNb2R1bGUsXG4gICAgRGRyU3Bpbm5lck1vZHVsZSxcbiAgICBEZHJUb2FzdE1vZHVsZSxcbiAgICBEZHJDbGlja091dHNpZGVNb2R1bGUsXG4gICAgRGRyRHJvcGRvd25Nb2R1bGUsXG4gICAgRGRyTG9hZElmcmFtZU1vZHVsZSxcbiAgICBEZHJDb25maWdNb2R1bGUsXG4gICAgRGRyRGV0YWlsTW9kdWxlLFxuICAgIERkckJsb2NrTGlzdE1vZHVsZSxcbiAgICBEZHJUYWJzTW9kdWxlLFxuICAgIERkclJlc29sdXRpb25Nb2R1bGUsXG4gICAgRGRyQWNjb3JkaW9uTW9kdWxlLFxuICAgIERkclRyYW5zbGF0ZU1vZHVsZSxcbiAgICBEZHJOZ01vZGVsQmFzZU1vZHVsZSxcbiAgICBEZHJUb2dnbGVNb2R1bGUsXG4gICAgRGRyTW9kYWxNb2R1bGVcbiAgXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAgRGRyQ29uc3RhbnRzU2VydmljZSxcbiAgICBEZHJUaGVtZVNlcnZpY2VcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBEZHJMaWJyYXJ5TW9kdWxlIHsgfVxuIl19