import { Injectable } from '@angular/core';
import { has, get, cloneDeep } from 'lodash-es';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
export class DdrConfigService {
    constructor(http) {
        this.http = http;
    }
    getDataFromJSON(pathJSON) {
        return new Promise((resolve, reject) => {
            this.http.get(pathJSON).subscribe(data => {
                this._data = data;
                resolve(true);
            }, error => {
                console.error("DDR-CONFIG: " + error);
                reject(true);
            });
        });
    }
    getData(path) {
        if (has(this._data, path)) {
            return get(this._data, path);
        }
        else {
            console.error("Not exists path: " + path);
            return null;
        }
    }
    getAllData() {
        return cloneDeep(this._data);
    }
}
DdrConfigService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigService, deps: [{ token: i1.HttpClient }], target: i0.ɵɵFactoryTarget.Injectable });
DdrConfigService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return [{ type: i1.HttpClient }]; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWNvbmZpZy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL3NlcnZpY2VzL2Rkci1jb25maWcvZGRyLWNvbmZpZy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsU0FBUyxFQUFFLE1BQU0sV0FBVyxDQUFDOzs7QUFLaEQsTUFBTSxPQUFPLGdCQUFnQjtJQUkzQixZQUNVLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7SUFDdEIsQ0FBQztJQUVMLGVBQWUsQ0FBQyxRQUFnQjtRQUM5QixPQUFPLElBQUksT0FBTyxDQUFFLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDdkMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7Z0JBQ2xCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNoQixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1QsT0FBTyxDQUFDLEtBQUssQ0FBQyxjQUFjLEdBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3BDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNmLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDO0lBRUQsT0FBTyxDQUFDLElBQVk7UUFDbEIsSUFBRyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBQztZQUN2QixPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQzlCO2FBQUk7WUFDSCxPQUFPLENBQUMsS0FBSyxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxDQUFDO1lBQzFDLE9BQU8sSUFBSSxDQUFDO1NBQ2I7SUFDSCxDQUFDO0lBRUQsVUFBVTtRQUNSLE9BQU8sU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMvQixDQUFDOzs2R0EvQlUsZ0JBQWdCO2lIQUFoQixnQkFBZ0IsY0FGZixNQUFNOzJGQUVQLGdCQUFnQjtrQkFINUIsVUFBVTttQkFBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgaGFzLCBnZXQsIGNsb25lRGVlcCB9IGZyb20gJ2xvZGFzaC1lcyc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIERkckNvbmZpZ1NlcnZpY2Uge1xuXG4gIHByaXZhdGUgX2RhdGE6IGFueTtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGh0dHA6IEh0dHBDbGllbnRcbiAgKSB7IH1cblxuICBnZXREYXRhRnJvbUpTT04ocGF0aEpTT046IHN0cmluZyl7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKCAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICB0aGlzLmh0dHAuZ2V0KHBhdGhKU09OKS5zdWJzY3JpYmUoZGF0YSA9PiB7XG4gICAgICAgIHRoaXMuX2RhdGEgPSBkYXRhO1xuICAgICAgICByZXNvbHZlKHRydWUpO1xuICAgICAgfSwgZXJyb3IgPT4ge1xuICAgICAgICBjb25zb2xlLmVycm9yKFwiRERSLUNPTkZJRzogXCIrZXJyb3IpO1xuICAgICAgICByZWplY3QodHJ1ZSk7XG4gICAgICB9KTtcbiAgICB9KVxuICB9XG5cbiAgZ2V0RGF0YShwYXRoOiBzdHJpbmcpe1xuICAgIGlmKGhhcyh0aGlzLl9kYXRhLCBwYXRoKSl7XG4gICAgICByZXR1cm4gZ2V0KHRoaXMuX2RhdGEsIHBhdGgpO1xuICAgIH1lbHNle1xuICAgICAgY29uc29sZS5lcnJvcihcIk5vdCBleGlzdHMgcGF0aDogXCIgKyBwYXRoKTtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgfVxuXG4gIGdldEFsbERhdGEoKXtcbiAgICByZXR1cm4gY2xvbmVEZWVwKHRoaXMuX2RhdGEpO1xuICB9XG5cbn1cbiJdfQ==