import { Pipe } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "./ddr-config.service";
export class DdrConfigPipe {
    constructor(ddrConfig) {
        this.ddrConfig = ddrConfig;
    }
    transform(path) {
        return this.ddrConfig.getData(path);
    }
}
DdrConfigPipe.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigPipe, deps: [{ token: i1.DdrConfigService }], target: i0.ɵɵFactoryTarget.Pipe });
DdrConfigPipe.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigPipe, name: "ddrConfig" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigPipe, decorators: [{
            type: Pipe,
            args: [{
                    name: 'ddrConfig'
                }]
        }], ctorParameters: function () { return [{ type: i1.DdrConfigService }]; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWNvbmZpZy5waXBlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL3NlcnZpY2VzL2Rkci1jb25maWcvZGRyLWNvbmZpZy5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDOzs7QUFLcEQsTUFBTSxPQUFPLGFBQWE7SUFFeEIsWUFDVSxTQUEyQjtRQUEzQixjQUFTLEdBQVQsU0FBUyxDQUFrQjtJQUNuQyxDQUFDO0lBRUgsU0FBUyxDQUFDLElBQVk7UUFDcEIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN0QyxDQUFDOzswR0FSVSxhQUFhO3dHQUFiLGFBQWE7MkZBQWIsYUFBYTtrQkFIekIsSUFBSTttQkFBQztvQkFDSixJQUFJLEVBQUUsV0FBVztpQkFDbEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEZHJDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi9kZHItY29uZmlnLnNlcnZpY2UnO1xuaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AUGlwZSh7XG4gIG5hbWU6ICdkZHJDb25maWcnXG59KVxuZXhwb3J0IGNsYXNzIERkckNvbmZpZ1BpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGRkckNvbmZpZzogRGRyQ29uZmlnU2VydmljZVxuICApe31cblxuICB0cmFuc2Zvcm0ocGF0aDogc3RyaW5nKTogYW55IHtcbiAgICByZXR1cm4gdGhpcy5kZHJDb25maWcuZ2V0RGF0YShwYXRoKTtcbiAgfVxuXG59XG4iXX0=