import { DdrConfigService } from './ddr-config.service';
import { NgModule } from '@angular/core';
import { DdrConfigPipe } from './ddr-config.pipe';
import { HttpClientModule } from '@angular/common/http';
import * as i0 from "@angular/core";
export class DdrConfigModule {
}
DdrConfigModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrConfigModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigModule, declarations: [DdrConfigPipe], imports: [HttpClientModule], exports: [DdrConfigPipe] });
DdrConfigModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigModule, providers: [
        DdrConfigService
    ], imports: [[
            HttpClientModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConfigModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        HttpClientModule
                    ],
                    declarations: [
                        DdrConfigPipe
                    ],
                    exports: [
                        DdrConfigPipe
                    ],
                    providers: [
                        DdrConfigService
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWNvbmZpZy5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9kZHItbGlicmFyeS9zcmMvc2VydmljZXMvZGRyLWNvbmZpZy9kZHItY29uZmlnLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQzs7QUFnQnhELE1BQU0sT0FBTyxlQUFlOzs0R0FBZixlQUFlOzZHQUFmLGVBQWUsaUJBVHhCLGFBQWEsYUFIYixnQkFBZ0IsYUFNaEIsYUFBYTs2R0FNSixlQUFlLGFBSmY7UUFDVCxnQkFBZ0I7S0FDakIsWUFYUTtZQUNQLGdCQUFnQjtTQUNqQjsyRkFXVSxlQUFlO2tCQWQzQixRQUFRO21CQUFDO29CQUNSLE9BQU8sRUFBRTt3QkFDUCxnQkFBZ0I7cUJBQ2pCO29CQUNELFlBQVksRUFBRTt3QkFDWixhQUFhO3FCQUNkO29CQUNELE9BQU8sRUFBRTt3QkFDUCxhQUFhO3FCQUNkO29CQUNELFNBQVMsRUFBRTt3QkFDVCxnQkFBZ0I7cUJBQ2pCO2lCQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGRyQ29uZmlnU2VydmljZSB9IGZyb20gJy4vZGRyLWNvbmZpZy5zZXJ2aWNlJztcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBEZHJDb25maWdQaXBlIH0gZnJvbSAnLi9kZHItY29uZmlnLnBpcGUnO1xuaW1wb3J0IHsgSHR0cENsaWVudE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIEh0dHBDbGllbnRNb2R1bGVcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgRGRyQ29uZmlnUGlwZVxuICBdLFxuICBleHBvcnRzOiBbXG4gICAgRGRyQ29uZmlnUGlwZVxuICBdLFxuICBwcm92aWRlcnM6IFtcbiAgICBEZHJDb25maWdTZXJ2aWNlXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgRGRyQ29uZmlnTW9kdWxlIHsgfVxuIl19