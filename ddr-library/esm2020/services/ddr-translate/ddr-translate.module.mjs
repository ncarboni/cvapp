import { HttpClientModule } from '@angular/common/http';
import { DdrTranslatePipe } from './ddr-translate.pipe';
import { DdrTranslateService } from './ddr-translate.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as i0 from "@angular/core";
export class DdrTranslateModule {
}
DdrTranslateModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTranslateModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrTranslateModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTranslateModule, declarations: [DdrTranslatePipe], imports: [CommonModule,
        HttpClientModule], exports: [DdrTranslatePipe] });
DdrTranslateModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTranslateModule, providers: [
        DdrTranslateService
    ], imports: [[
            CommonModule,
            HttpClientModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTranslateModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule,
                        HttpClientModule
                    ],
                    declarations: [
                        DdrTranslatePipe
                    ],
                    exports: [
                        DdrTranslatePipe
                    ],
                    providers: [
                        DdrTranslateService
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLXRyYW5zbGF0ZS5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9kZHItbGlicmFyeS9zcmMvc2VydmljZXMvZGRyLXRyYW5zbGF0ZS9kZHItdHJhbnNsYXRlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQzs7QUFpQi9DLE1BQU0sT0FBTyxrQkFBa0I7OytHQUFsQixrQkFBa0I7Z0hBQWxCLGtCQUFrQixpQkFUM0IsZ0JBQWdCLGFBSmhCLFlBQVk7UUFDWixnQkFBZ0IsYUFNaEIsZ0JBQWdCO2dIQU1QLGtCQUFrQixhQUpsQjtRQUNULG1CQUFtQjtLQUNwQixZQVpRO1lBQ1AsWUFBWTtZQUNaLGdCQUFnQjtTQUNqQjsyRkFXVSxrQkFBa0I7a0JBZjlCLFFBQVE7bUJBQUM7b0JBQ1IsT0FBTyxFQUFFO3dCQUNQLFlBQVk7d0JBQ1osZ0JBQWdCO3FCQUNqQjtvQkFDRCxZQUFZLEVBQUU7d0JBQ1osZ0JBQWdCO3FCQUNqQjtvQkFDRCxPQUFPLEVBQUU7d0JBQ1AsZ0JBQWdCO3FCQUNqQjtvQkFDRCxTQUFTLEVBQUU7d0JBQ1QsbUJBQW1CO3FCQUNwQjtpQkFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBEZHJUcmFuc2xhdGVQaXBlIH0gZnJvbSAnLi9kZHItdHJhbnNsYXRlLnBpcGUnO1xuaW1wb3J0IHsgRGRyVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJy4vZGRyLXRyYW5zbGF0ZS5zZXJ2aWNlJztcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIEh0dHBDbGllbnRNb2R1bGVcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgRGRyVHJhbnNsYXRlUGlwZVxuICBdLFxuICBleHBvcnRzOiBbXG4gICAgRGRyVHJhbnNsYXRlUGlwZVxuICBdLFxuICBwcm92aWRlcnM6IFtcbiAgICBEZHJUcmFuc2xhdGVTZXJ2aWNlXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgRGRyVHJhbnNsYXRlTW9kdWxlIHsgfVxuIl19