import { Pipe } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "./ddr-translate.service";
export class DdrTranslatePipe {
    constructor(translateService) {
        this.translateService = translateService;
    }
    transform(value) {
        return this.translateService.getTranslate(value) ? this.translateService.getTranslate(value) : value;
    }
}
DdrTranslatePipe.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTranslatePipe, deps: [{ token: i1.DdrTranslateService }], target: i0.ɵɵFactoryTarget.Pipe });
DdrTranslatePipe.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTranslatePipe, name: "ddrTranslate" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrTranslatePipe, decorators: [{
            type: Pipe,
            args: [{
                    name: 'ddrTranslate'
                }]
        }], ctorParameters: function () { return [{ type: i1.DdrTranslateService }]; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLXRyYW5zbGF0ZS5waXBlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL3NlcnZpY2VzL2Rkci10cmFuc2xhdGUvZGRyLXRyYW5zbGF0ZS5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDOzs7QUFLcEQsTUFBTSxPQUFPLGdCQUFnQjtJQUUzQixZQUFvQixnQkFBcUM7UUFBckMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFxQjtJQUFHLENBQUM7SUFFN0QsU0FBUyxDQUFDLEtBQWE7UUFDckIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDdkcsQ0FBQzs7NkdBTlUsZ0JBQWdCOzJHQUFoQixnQkFBZ0I7MkZBQWhCLGdCQUFnQjtrQkFINUIsSUFBSTttQkFBQztvQkFDSixJQUFJLEVBQUUsY0FBYztpQkFDckIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEZHJUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSAnLi9kZHItdHJhbnNsYXRlLnNlcnZpY2UnO1xuaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AUGlwZSh7XG4gIG5hbWU6ICdkZHJUcmFuc2xhdGUnXG59KVxuZXhwb3J0IGNsYXNzIERkclRyYW5zbGF0ZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHRyYW5zbGF0ZVNlcnZpY2U6IERkclRyYW5zbGF0ZVNlcnZpY2UpIHt9XG5cbiAgdHJhbnNmb3JtKHZhbHVlOiBzdHJpbmcpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLnRyYW5zbGF0ZVNlcnZpY2UuZ2V0VHJhbnNsYXRlKHZhbHVlKSA/IHRoaXMudHJhbnNsYXRlU2VydmljZS5nZXRUcmFuc2xhdGUodmFsdWUpIDogdmFsdWU7XG4gIH1cblxufVxuIl19