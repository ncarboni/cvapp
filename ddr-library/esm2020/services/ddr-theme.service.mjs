import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "./ddr-constants.service";
export class DdrThemeService {
    constructor(constans) {
        this.constans = constans;
        this.themesAvailables = [
            this.constans.DdrThemeConstants.THEME_DEFAULT,
            this.constans.DdrThemeConstants.THEME_DARK
        ];
        this._theme = this.constans.DdrThemeConstants.THEME_DEFAULT;
    }
    get theme() {
        return this._theme;
    }
    changeTheme(theme) {
        if (this.themesAvailables.find(t => t === this._theme)) {
            this._theme = theme;
        }
        else {
            console.error("Theme " + theme + " not exists");
        }
    }
}
DdrThemeService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrThemeService, deps: [{ token: i1.DdrConstantsService }], target: i0.ɵɵFactoryTarget.Injectable });
DdrThemeService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrThemeService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrThemeService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return [{ type: i1.DdrConstantsService }]; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLXRoZW1lLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9wcm9qZWN0cy9kZHItbGlicmFyeS9zcmMvc2VydmljZXMvZGRyLXRoZW1lLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7O0FBSzNDLE1BQU0sT0FBTyxlQUFlO0lBYTFCLFlBQW9CLFFBQTZCO1FBQTdCLGFBQVEsR0FBUixRQUFRLENBQXFCO1FBTHpDLHFCQUFnQixHQUFhO1lBQ25DLElBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsYUFBYTtZQUM3QyxJQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLFVBQVU7U0FDM0MsQ0FBQztRQUdBLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUM7SUFDOUQsQ0FBQztJQVhELElBQVcsS0FBSztRQUNkLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUNyQixDQUFDO0lBV0QsV0FBVyxDQUFDLEtBQWE7UUFDdkIsSUFBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBQztZQUNwRCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUNyQjthQUFJO1lBQ0gsT0FBTyxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsS0FBSyxHQUFHLGFBQWEsQ0FBQyxDQUFDO1NBRWpEO0lBQ0gsQ0FBQzs7NEdBeEJVLGVBQWU7Z0hBQWYsZUFBZSxjQUZkLE1BQU07MkZBRVAsZUFBZTtrQkFIM0IsVUFBVTttQkFBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEZHJDb25zdGFudHNTZXJ2aWNlIH0gZnJvbSAnLi9kZHItY29uc3RhbnRzLnNlcnZpY2UnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBEZHJUaGVtZVNlcnZpY2Uge1xuXG4gIHByaXZhdGUgX3RoZW1lOiBzdHJpbmc7XG5cbiAgcHVibGljIGdldCB0aGVtZSgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLl90aGVtZTtcbiAgfVxuICBcbiAgcHJpdmF0ZSB0aGVtZXNBdmFpbGFibGVzOiBzdHJpbmdbXSA9IFtcbiAgICB0aGlzLmNvbnN0YW5zLkRkclRoZW1lQ29uc3RhbnRzLlRIRU1FX0RFRkFVTFQsXG4gICAgdGhpcy5jb25zdGFucy5EZHJUaGVtZUNvbnN0YW50cy5USEVNRV9EQVJLXG4gIF07ICBcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNvbnN0YW5zOiBEZHJDb25zdGFudHNTZXJ2aWNlKSB7IFxuICAgIHRoaXMuX3RoZW1lID0gdGhpcy5jb25zdGFucy5EZHJUaGVtZUNvbnN0YW50cy5USEVNRV9ERUZBVUxUO1xuICB9XG5cbiAgY2hhbmdlVGhlbWUodGhlbWU6IHN0cmluZyl7XG4gICAgaWYodGhpcy50aGVtZXNBdmFpbGFibGVzLmZpbmQodCA9PiB0ID09PSB0aGlzLl90aGVtZSkpe1xuICAgICAgdGhpcy5fdGhlbWUgPSB0aGVtZTtcbiAgICB9ZWxzZXtcbiAgICAgIGNvbnNvbGUuZXJyb3IoXCJUaGVtZSBcIiArIHRoZW1lICsgXCIgbm90IGV4aXN0c1wiKTtcbiAgICAgIFxuICAgIH1cbiAgfVxuXG59XG4iXX0=