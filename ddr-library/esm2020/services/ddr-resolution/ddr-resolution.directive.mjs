import { Directive, HostListener } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "./ddr-resolution.service";
export class DdrResolutionDirective {
    constructor(resolution) {
        this.resolution = resolution;
    }
    ngOnInit() {
        this.sendSize(window.innerWidth);
    }
    onResize(event) {
        this.sendSize(event.currentTarget.innerWidth);
    }
    sendSize(width) {
        this.resolution.setSize(width);
    }
}
DdrResolutionDirective.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionDirective, deps: [{ token: i1.DdrResolutionService }], target: i0.ɵɵFactoryTarget.Directive });
DdrResolutionDirective.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "13.0.2", type: DdrResolutionDirective, selector: "[ddrResolution]", host: { listeners: { "window:resize": "onResize($event)" } }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionDirective, decorators: [{
            type: Directive,
            args: [{
                    selector: '[ddrResolution]'
                }]
        }], ctorParameters: function () { return [{ type: i1.DdrResolutionService }]; }, propDecorators: { onResize: [{
                type: HostListener,
                args: ['window:resize', ['$event']]
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLXJlc29sdXRpb24uZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL3NlcnZpY2VzL2Rkci1yZXNvbHV0aW9uL2Rkci1yZXNvbHV0aW9uLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBVSxNQUFNLGVBQWUsQ0FBQzs7O0FBS2hFLE1BQU0sT0FBTyxzQkFBc0I7SUFFakMsWUFDVSxVQUFnQztRQUFoQyxlQUFVLEdBQVYsVUFBVSxDQUFzQjtJQUN0QyxDQUFDO0lBRUwsUUFBUTtRQUNOLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFHRCxRQUFRLENBQUMsS0FBSztRQUNaLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQsUUFBUSxDQUFDLEtBQWE7UUFDcEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDakMsQ0FBQzs7bUhBakJVLHNCQUFzQjt1R0FBdEIsc0JBQXNCOzJGQUF0QixzQkFBc0I7a0JBSGxDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLGlCQUFpQjtpQkFDNUI7MkdBWUMsUUFBUTtzQkFEUCxZQUFZO3VCQUFDLGVBQWUsRUFBRSxDQUFDLFFBQVEsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERkclJlc29sdXRpb25TZXJ2aWNlIH0gZnJvbSAnLi9kZHItcmVzb2x1dGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IERpcmVjdGl2ZSwgSG9zdExpc3RlbmVyLCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW2RkclJlc29sdXRpb25dJ1xufSlcbmV4cG9ydCBjbGFzcyBEZHJSZXNvbHV0aW9uRGlyZWN0aXZlIGltcGxlbWVudHMgT25Jbml0IHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIHJlc29sdXRpb246IERkclJlc29sdXRpb25TZXJ2aWNlXG4gICkgeyB9XG5cbiAgbmdPbkluaXQoKXtcbiAgICB0aGlzLnNlbmRTaXplKHdpbmRvdy5pbm5lcldpZHRoKTtcbiAgfVxuXG4gIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzpyZXNpemUnLCBbJyRldmVudCddKVxuICBvblJlc2l6ZShldmVudCl7XG4gICAgdGhpcy5zZW5kU2l6ZShldmVudC5jdXJyZW50VGFyZ2V0LmlubmVyV2lkdGgpO1xuICB9XG5cbiAgc2VuZFNpemUod2lkdGg6IG51bWJlcil7XG4gICAgdGhpcy5yZXNvbHV0aW9uLnNldFNpemUod2lkdGgpO1xuICB9XG5cbn1cbiJdfQ==