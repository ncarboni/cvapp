import { DdrResolutionService } from './ddr-resolution.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DdrResolutionDirective } from './ddr-resolution.directive';
import * as i0 from "@angular/core";
export class DdrResolutionModule {
}
DdrResolutionModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
DdrResolutionModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionModule, declarations: [DdrResolutionDirective], imports: [CommonModule], exports: [DdrResolutionDirective] });
DdrResolutionModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionModule, providers: [
        DdrResolutionService
    ], imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionModule, decorators: [{
            type: NgModule,
            args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [
                        DdrResolutionDirective
                    ],
                    exports: [
                        DdrResolutionDirective
                    ],
                    providers: [
                        DdrResolutionService
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLXJlc29sdXRpb24ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL3NlcnZpY2VzL2Rkci1yZXNvbHV0aW9uL2Rkci1yZXNvbHV0aW9uLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNoRSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQzs7QUFnQnBFLE1BQU0sT0FBTyxtQkFBbUI7O2dIQUFuQixtQkFBbUI7aUhBQW5CLG1CQUFtQixpQkFUNUIsc0JBQXNCLGFBSHRCLFlBQVksYUFNWixzQkFBc0I7aUhBTWIsbUJBQW1CLGFBSm5CO1FBQ1Qsb0JBQW9CO0tBQ3JCLFlBWFE7WUFDUCxZQUFZO1NBQ2I7MkZBV1UsbUJBQW1CO2tCQWQvQixRQUFRO21CQUFDO29CQUNSLE9BQU8sRUFBRTt3QkFDUCxZQUFZO3FCQUNiO29CQUNELFlBQVksRUFBRTt3QkFDWixzQkFBc0I7cUJBQ3ZCO29CQUNELE9BQU8sRUFBRTt3QkFDUCxzQkFBc0I7cUJBQ3ZCO29CQUNELFNBQVMsRUFBRTt3QkFDVCxvQkFBb0I7cUJBQ3JCO2lCQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGRyUmVzb2x1dGlvblNlcnZpY2UgfSBmcm9tICcuL2Rkci1yZXNvbHV0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBEZHJSZXNvbHV0aW9uRGlyZWN0aXZlIH0gZnJvbSAnLi9kZHItcmVzb2x1dGlvbi5kaXJlY3RpdmUnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1xuICAgIERkclJlc29sdXRpb25EaXJlY3RpdmVcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIERkclJlc29sdXRpb25EaXJlY3RpdmVcbiAgXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAgRGRyUmVzb2x1dGlvblNlcnZpY2VcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBEZHJSZXNvbHV0aW9uTW9kdWxlIHsgfVxuIl19