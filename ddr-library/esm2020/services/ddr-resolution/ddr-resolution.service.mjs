import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "./../ddr-constants.service";
export class DdrResolutionService {
    constructor(constants) {
        this.constants = constants;
    }
    get size() {
        return this._size;
    }
    set size(value) {
        this._size = value;
    }
    setSize(width) {
        if (width >= this.constants.DdrResolutionConstants.MOBILE_MIN &&
            width < this.constants.DdrResolutionConstants.MOBILE_MAX) {
            this._size = this.constants.DdrResolutionConstants.MOBILE;
        }
        else if (width >= this.constants.DdrResolutionConstants.TABLET_MIN &&
            width < this.constants.DdrResolutionConstants.TABLET_MAX) {
            this._size = this.constants.DdrResolutionConstants.TABLET;
        }
        else if (width >= this.constants.DdrResolutionConstants.WEB_MIN &&
            width < this.constants.DdrResolutionConstants.WEB_MAX) {
            this._size = this.constants.DdrResolutionConstants.WEB;
        }
    }
}
DdrResolutionService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionService, deps: [{ token: i1.DdrConstantsService }], target: i0.ɵɵFactoryTarget.Injectable });
DdrResolutionService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrResolutionService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return [{ type: i1.DdrConstantsService }]; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLXJlc29sdXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2Rkci1saWJyYXJ5L3NyYy9zZXJ2aWNlcy9kZHItcmVzb2x1dGlvbi9kZHItcmVzb2x1dGlvbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7OztBQUszQyxNQUFNLE9BQU8sb0JBQW9CO0lBVy9CLFlBQ1UsU0FBOEI7UUFBOUIsY0FBUyxHQUFULFNBQVMsQ0FBcUI7SUFDcEMsQ0FBQztJQVRMLElBQVcsSUFBSTtRQUNiLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNwQixDQUFDO0lBQ0QsSUFBVyxJQUFJLENBQUMsS0FBYTtRQUMzQixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUNyQixDQUFDO0lBTUQsT0FBTyxDQUFDLEtBQWE7UUFFbkIsSUFBSSxLQUFLLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVO1lBQzNELEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLHNCQUFzQixDQUFDLFVBQVUsRUFBRTtZQUMxRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDO1NBQzNEO2FBQU0sSUFBSSxLQUFLLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVO1lBQ2xFLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLHNCQUFzQixDQUFDLFVBQVUsRUFBRTtZQUMxRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDO1NBQzNEO2FBQU0sSUFBSSxLQUFLLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPO1lBQy9ELEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLHNCQUFzQixDQUFDLE9BQU8sRUFBRTtZQUN2RCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsc0JBQXNCLENBQUMsR0FBRyxDQUFDO1NBQ3hEO0lBR0gsQ0FBQzs7aUhBN0JVLG9CQUFvQjtxSEFBcEIsb0JBQW9CLGNBRm5CLE1BQU07MkZBRVAsb0JBQW9CO2tCQUhoQyxVQUFVO21CQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERkckNvbnN0YW50c1NlcnZpY2UgfSBmcm9tICcuLy4uL2Rkci1jb25zdGFudHMuc2VydmljZSc7XG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIERkclJlc29sdXRpb25TZXJ2aWNlIHtcblxuICBwcml2YXRlIF9zaXplOiBzdHJpbmc7XG5cbiAgcHVibGljIGdldCBzaXplKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuX3NpemU7XG4gIH1cbiAgcHVibGljIHNldCBzaXplKHZhbHVlOiBzdHJpbmcpIHtcbiAgICB0aGlzLl9zaXplID0gdmFsdWU7XG4gIH1cblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGNvbnN0YW50czogRGRyQ29uc3RhbnRzU2VydmljZVxuICApIHsgfVxuXG4gIHNldFNpemUod2lkdGg6IG51bWJlcikge1xuXG4gICAgaWYgKHdpZHRoID49IHRoaXMuY29uc3RhbnRzLkRkclJlc29sdXRpb25Db25zdGFudHMuTU9CSUxFX01JTiAmJlxuICAgICAgd2lkdGggPCB0aGlzLmNvbnN0YW50cy5EZHJSZXNvbHV0aW9uQ29uc3RhbnRzLk1PQklMRV9NQVgpIHtcbiAgICAgIHRoaXMuX3NpemUgPSB0aGlzLmNvbnN0YW50cy5EZHJSZXNvbHV0aW9uQ29uc3RhbnRzLk1PQklMRTtcbiAgICB9IGVsc2UgaWYgKHdpZHRoID49IHRoaXMuY29uc3RhbnRzLkRkclJlc29sdXRpb25Db25zdGFudHMuVEFCTEVUX01JTiAmJlxuICAgICAgd2lkdGggPCB0aGlzLmNvbnN0YW50cy5EZHJSZXNvbHV0aW9uQ29uc3RhbnRzLlRBQkxFVF9NQVgpIHtcbiAgICAgIHRoaXMuX3NpemUgPSB0aGlzLmNvbnN0YW50cy5EZHJSZXNvbHV0aW9uQ29uc3RhbnRzLlRBQkxFVDtcbiAgICB9IGVsc2UgaWYgKHdpZHRoID49IHRoaXMuY29uc3RhbnRzLkRkclJlc29sdXRpb25Db25zdGFudHMuV0VCX01JTiAmJlxuICAgICAgd2lkdGggPCB0aGlzLmNvbnN0YW50cy5EZHJSZXNvbHV0aW9uQ29uc3RhbnRzLldFQl9NQVgpIHtcbiAgICAgIHRoaXMuX3NpemUgPSB0aGlzLmNvbnN0YW50cy5EZHJSZXNvbHV0aW9uQ29uc3RhbnRzLldFQjtcbiAgICB9XG5cblxuICB9XG5cblxufVxuIl19