import { DdrToastConstants, DdrBlockListConstants, DdrResolutionConstants, DdrAccordionConstants, DdrModalTypeConstants, DdrThemeConstants } from './../constans/ddr-constans';
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class DdrConstantsService {
    constructor() {
        this.DdrToastConstants = DdrToastConstants;
        this.DdrBlockListConstants = DdrBlockListConstants;
        this.DdrResolutionConstants = DdrResolutionConstants;
        this.DdrAccordionConstants = DdrAccordionConstants;
        this.DdrModalTypeConstants = DdrModalTypeConstants;
        this.DdrThemeConstants = DdrThemeConstants;
    }
}
DdrConstantsService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConstantsService, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
DdrConstantsService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConstantsService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.0.2", ngImport: i0, type: DdrConstantsService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWNvbnN0YW50cy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL3NlcnZpY2VzL2Rkci1jb25zdGFudHMuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUscUJBQXFCLEVBQUUsc0JBQXNCLEVBQUUscUJBQXFCLEVBQUUscUJBQXFCLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUMvSyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQUszQyxNQUFNLE9BQU8sbUJBQW1CO0lBUzlCO1FBUEEsc0JBQWlCLEdBQUcsaUJBQWlCLENBQUM7UUFDdEMsMEJBQXFCLEdBQUcscUJBQXFCLENBQUM7UUFDOUMsMkJBQXNCLEdBQUcsc0JBQXNCLENBQUM7UUFDaEQsMEJBQXFCLEdBQUcscUJBQXFCLENBQUM7UUFDOUMsMEJBQXFCLEdBQUcscUJBQXFCLENBQUM7UUFDOUMsc0JBQWlCLEdBQUcsaUJBQWlCLENBQUM7SUFHdEMsQ0FBQzs7Z0hBVlUsbUJBQW1CO29IQUFuQixtQkFBbUIsY0FGbEIsTUFBTTsyRkFFUCxtQkFBbUI7a0JBSC9CLFVBQVU7bUJBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGRyVG9hc3RDb25zdGFudHMsIERkckJsb2NrTGlzdENvbnN0YW50cywgRGRyUmVzb2x1dGlvbkNvbnN0YW50cywgRGRyQWNjb3JkaW9uQ29uc3RhbnRzLCBEZHJNb2RhbFR5cGVDb25zdGFudHMsIERkclRoZW1lQ29uc3RhbnRzIH0gZnJvbSAnLi8uLi9jb25zdGFucy9kZHItY29uc3RhbnMnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBEZHJDb25zdGFudHNTZXJ2aWNlIHtcblxuICBEZHJUb2FzdENvbnN0YW50cyA9IERkclRvYXN0Q29uc3RhbnRzO1xuICBEZHJCbG9ja0xpc3RDb25zdGFudHMgPSBEZHJCbG9ja0xpc3RDb25zdGFudHM7XG4gIERkclJlc29sdXRpb25Db25zdGFudHMgPSBEZHJSZXNvbHV0aW9uQ29uc3RhbnRzO1xuICBEZHJBY2NvcmRpb25Db25zdGFudHMgPSBEZHJBY2NvcmRpb25Db25zdGFudHM7XG4gIERkck1vZGFsVHlwZUNvbnN0YW50cyA9IERkck1vZGFsVHlwZUNvbnN0YW50cztcbiAgRGRyVGhlbWVDb25zdGFudHMgPSBEZHJUaGVtZUNvbnN0YW50cztcblxuICBjb25zdHJ1Y3RvcigpIHsgXG4gIH1cblxufVxuIl19