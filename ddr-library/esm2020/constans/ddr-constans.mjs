export class DdrToastConstants {
}
DdrToastConstants.TYPE_INFO = 'info';
DdrToastConstants.TYPE_WARNING = 'warning';
DdrToastConstants.TYPE_ERROR = 'danger';
DdrToastConstants.TYPE_SUCCESS = 'success';
DdrToastConstants.TIMEOUT = 5000;
export class DdrBlockListConstants {
}
DdrBlockListConstants.PAGINATION_DEFAULT = 10;
export class DdrResolutionConstants {
}
DdrResolutionConstants.MOBILE = 'mobile';
DdrResolutionConstants.TABLET = 'tablet';
DdrResolutionConstants.WEB = 'web';
DdrResolutionConstants.MOBILE_MIN = 0;
DdrResolutionConstants.MOBILE_MAX = 568;
DdrResolutionConstants.TABLET_MIN = 568;
DdrResolutionConstants.TABLET_MAX = 992;
DdrResolutionConstants.WEB_MIN = 992;
DdrResolutionConstants.WEB_MAX = 9999;
export class DdrAccordionConstants {
}
DdrAccordionConstants.OPEN = 'open';
DdrAccordionConstants.CLOSE = 'close';
export class DdrModalTypeConstants {
}
DdrModalTypeConstants.CONFIRM = 'confirm';
DdrModalTypeConstants.INFO = 'info';
DdrModalTypeConstants.NO_BUTTONS = 'no-buttons';
export class DdrThemeConstants {
}
DdrThemeConstants.THEME_DEFAULT = 'theme-default';
DdrThemeConstants.THEME_DARK = 'theme-dark';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGRyLWNvbnN0YW5zLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vcHJvamVjdHMvZGRyLWxpYnJhcnkvc3JjL2NvbnN0YW5zL2Rkci1jb25zdGFucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxNQUFNLE9BQU8saUJBQWlCOztBQUNuQiwyQkFBUyxHQUFXLE1BQU0sQ0FBQztBQUMzQiw4QkFBWSxHQUFXLFNBQVMsQ0FBQztBQUNqQyw0QkFBVSxHQUFXLFFBQVEsQ0FBQztBQUM5Qiw4QkFBWSxHQUFXLFNBQVMsQ0FBQztBQUVqQyx5QkFBTyxHQUFXLElBQUksQ0FBQztBQUdsQyxNQUFNLE9BQU8scUJBQXFCOztBQUN2Qix3Q0FBa0IsR0FBVyxFQUFFLENBQUM7QUFHM0MsTUFBTSxPQUFPLHNCQUFzQjs7QUFDeEIsNkJBQU0sR0FBVyxRQUFRLENBQUM7QUFDMUIsNkJBQU0sR0FBVyxRQUFRLENBQUM7QUFDMUIsMEJBQUcsR0FBVyxLQUFLLENBQUM7QUFDcEIsaUNBQVUsR0FBVyxDQUFDLENBQUM7QUFDdkIsaUNBQVUsR0FBVyxHQUFHLENBQUM7QUFDekIsaUNBQVUsR0FBVyxHQUFHLENBQUM7QUFDekIsaUNBQVUsR0FBVyxHQUFHLENBQUM7QUFDekIsOEJBQU8sR0FBVyxHQUFHLENBQUM7QUFDdEIsOEJBQU8sR0FBVyxJQUFJLENBQUM7QUFHbEMsTUFBTSxPQUFPLHFCQUFxQjs7QUFDdkIsMEJBQUksR0FBVyxNQUFNLENBQUM7QUFDdEIsMkJBQUssR0FBVyxPQUFPLENBQUM7QUFHbkMsTUFBTSxPQUFPLHFCQUFxQjs7QUFDdkIsNkJBQU8sR0FBVyxTQUFTLENBQUM7QUFDNUIsMEJBQUksR0FBVyxNQUFNLENBQUM7QUFDdEIsZ0NBQVUsR0FBVyxZQUFZLENBQUM7QUFHN0MsTUFBTSxPQUFPLGlCQUFpQjs7QUFDbkIsK0JBQWEsR0FBVyxlQUFlLENBQUM7QUFDeEMsNEJBQVUsR0FBVyxZQUFZLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgRGRyVG9hc3RDb25zdGFudHMge1xuICAgIHN0YXRpYyBUWVBFX0lORk86IHN0cmluZyA9ICdpbmZvJztcbiAgICBzdGF0aWMgVFlQRV9XQVJOSU5HOiBzdHJpbmcgPSAnd2FybmluZyc7XG4gICAgc3RhdGljIFRZUEVfRVJST1I6IHN0cmluZyA9ICdkYW5nZXInO1xuICAgIHN0YXRpYyBUWVBFX1NVQ0NFU1M6IHN0cmluZyA9ICdzdWNjZXNzJztcblxuICAgIHN0YXRpYyBUSU1FT1VUOiBudW1iZXIgPSA1MDAwO1xufVxuXG5leHBvcnQgY2xhc3MgRGRyQmxvY2tMaXN0Q29uc3RhbnRzIHtcbiAgICBzdGF0aWMgUEFHSU5BVElPTl9ERUZBVUxUOiBudW1iZXIgPSAxMDtcbn1cblxuZXhwb3J0IGNsYXNzIERkclJlc29sdXRpb25Db25zdGFudHMge1xuICAgIHN0YXRpYyBNT0JJTEU6IHN0cmluZyA9ICdtb2JpbGUnO1xuICAgIHN0YXRpYyBUQUJMRVQ6IHN0cmluZyA9ICd0YWJsZXQnO1xuICAgIHN0YXRpYyBXRUI6IHN0cmluZyA9ICd3ZWInO1xuICAgIHN0YXRpYyBNT0JJTEVfTUlOOiBudW1iZXIgPSAwO1xuICAgIHN0YXRpYyBNT0JJTEVfTUFYOiBudW1iZXIgPSA1Njg7XG4gICAgc3RhdGljIFRBQkxFVF9NSU46IG51bWJlciA9IDU2ODtcbiAgICBzdGF0aWMgVEFCTEVUX01BWDogbnVtYmVyID0gOTkyO1xuICAgIHN0YXRpYyBXRUJfTUlOOiBudW1iZXIgPSA5OTI7XG4gICAgc3RhdGljIFdFQl9NQVg6IG51bWJlciA9IDk5OTk7XG59XG5cbmV4cG9ydCBjbGFzcyBEZHJBY2NvcmRpb25Db25zdGFudHMge1xuICAgIHN0YXRpYyBPUEVOOiBzdHJpbmcgPSAnb3Blbic7XG4gICAgc3RhdGljIENMT1NFOiBzdHJpbmcgPSAnY2xvc2UnO1xufVxuXG5leHBvcnQgY2xhc3MgRGRyTW9kYWxUeXBlQ29uc3RhbnRzIHtcbiAgICBzdGF0aWMgQ09ORklSTTogc3RyaW5nID0gJ2NvbmZpcm0nO1xuICAgIHN0YXRpYyBJTkZPOiBzdHJpbmcgPSAnaW5mbyc7XG4gICAgc3RhdGljIE5PX0JVVFRPTlM6IHN0cmluZyA9ICduby1idXR0b25zJztcbn1cblxuZXhwb3J0IGNsYXNzIERkclRoZW1lQ29uc3RhbnRzIHtcbiAgICBzdGF0aWMgVEhFTUVfREVGQVVMVDogc3RyaW5nID0gJ3RoZW1lLWRlZmF1bHQnO1xuICAgIHN0YXRpYyBUSEVNRV9EQVJLOiBzdHJpbmcgPSAndGhlbWUtZGFyayc7XG59Il19