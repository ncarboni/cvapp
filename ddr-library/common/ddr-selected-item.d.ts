export declare class DdrSelectedItem<T> {
    item: T;
    index: number;
}
