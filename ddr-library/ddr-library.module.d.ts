import * as i0 from "@angular/core";
import * as i1 from "./pipes/ddr-join-pipe/ddr-join-pipe.module";
import * as i2 from "./components/ddr-spinner/ddr-spinner.module";
import * as i3 from "./components/ddr-toast/ddr-toast.module";
import * as i4 from "./directives/ddr-click-outside/ddr-click-outside.module";
import * as i5 from "./components/ddr-dropdown/ddr-dropdown.module";
import * as i6 from "./directives/ddr-load-iframe/ddr-load-iframe.module";
import * as i7 from "./services/ddr-config/ddr-config.module";
import * as i8 from "./components/ddr-detail/ddr-detail.module";
import * as i9 from "./components/ddr-block-list/ddr-block-list.module";
import * as i10 from "./components/ddr-tabs/ddr-tabs.module";
import * as i11 from "./services/ddr-resolution/ddr-resolution.module";
import * as i12 from "./components/ddr-accordion/ddr-accordion.module";
import * as i13 from "./services/ddr-translate/ddr-translate.module";
import * as i14 from "./components/ddr-ng-model-base/ddr-ng-model-base.module";
import * as i15 from "./components/ddr-toggle/ddr-toggle.module";
import * as i16 from "./components/ddr-modal/ddr-modal.module";
export declare class DdrLibraryModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrLibraryModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DdrLibraryModule, never, [typeof i1.DdrJoinPipeModule, typeof i2.DdrSpinnerModule, typeof i3.DdrToastModule, typeof i4.DdrClickOutsideModule, typeof i5.DdrDropdownModule, typeof i6.DdrLoadIframeModule, typeof i7.DdrConfigModule, typeof i8.DdrDetailModule, typeof i9.DdrBlockListModule, typeof i10.DdrTabsModule, typeof i11.DdrResolutionModule, typeof i12.DdrAccordionModule, typeof i13.DdrTranslateModule, typeof i14.DdrNgModelBaseModule, typeof i15.DdrToggleModule, typeof i16.DdrModalModule], [typeof i1.DdrJoinPipeModule, typeof i2.DdrSpinnerModule, typeof i3.DdrToastModule, typeof i4.DdrClickOutsideModule, typeof i5.DdrDropdownModule, typeof i6.DdrLoadIframeModule, typeof i7.DdrConfigModule, typeof i8.DdrDetailModule, typeof i9.DdrBlockListModule, typeof i10.DdrTabsModule, typeof i11.DdrResolutionModule, typeof i12.DdrAccordionModule, typeof i13.DdrTranslateModule, typeof i14.DdrNgModelBaseModule, typeof i15.DdrToggleModule, typeof i16.DdrModalModule]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DdrLibraryModule>;
}
