import * as i0 from "@angular/core";
import * as i1 from "./ddr-join.pipe";
import * as i2 from "@angular/common";
export declare class DdrJoinPipeModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrJoinPipeModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DdrJoinPipeModule, [typeof i1.DdrJoinPipe], [typeof i2.CommonModule], [typeof i1.DdrJoinPipe]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DdrJoinPipeModule>;
}
