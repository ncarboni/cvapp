import { PipeTransform } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DdrJoinPipe implements PipeTransform {
    transform(value: string[], separator?: string): any;
    static ɵfac: i0.ɵɵFactoryDeclaration<DdrJoinPipe, never>;
    static ɵpipe: i0.ɵɵPipeDeclaration<DdrJoinPipe, "join">;
}
