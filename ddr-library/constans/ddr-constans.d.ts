export declare class DdrToastConstants {
    static TYPE_INFO: string;
    static TYPE_WARNING: string;
    static TYPE_ERROR: string;
    static TYPE_SUCCESS: string;
    static TIMEOUT: number;
}
export declare class DdrBlockListConstants {
    static PAGINATION_DEFAULT: number;
}
export declare class DdrResolutionConstants {
    static MOBILE: string;
    static TABLET: string;
    static WEB: string;
    static MOBILE_MIN: number;
    static MOBILE_MAX: number;
    static TABLET_MIN: number;
    static TABLET_MAX: number;
    static WEB_MIN: number;
    static WEB_MAX: number;
}
export declare class DdrAccordionConstants {
    static OPEN: string;
    static CLOSE: string;
}
export declare class DdrModalTypeConstants {
    static CONFIRM: string;
    static INFO: string;
    static NO_BUTTONS: string;
}
export declare class DdrThemeConstants {
    static THEME_DEFAULT: string;
    static THEME_DARK: string;
}
